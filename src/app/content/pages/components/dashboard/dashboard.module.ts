import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {RouterModule} from '@angular/router';
import {LayoutModule} from '../../../layout/layout.module';
import {PartialsModule} from '../../../partials/partials.module';
import {ListTimelineModule} from '../../../partials/layout/quick-sidebar/list-timeline/list-timeline.module';
import {WidgetChartsModule} from '../../../partials/content/widgets/charts/widget-charts.module';
import {InqStatComponent} from './inq-stat/inq-stat.component';
import {
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatSelectModule
} from '@angular/material';
import {ChartsModule} from 'ng2-charts';
import {NgxMatDrpModule} from 'ngx-mat-daterange-picker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SalesOutstandingChartComponent} from './sales-outstanding-chart/sales-outstanding-chart.component';
import {TargetSalesChartComponent} from './target-sales-chart/target-sales-chart.component';
import {SumPipe} from '../../../../shared/sum.pipe';
import {Daterangepicker} from 'ng2-daterangepicker';
import {AccountTasksComponent} from './account-tasks/account-tasks.component';
import {NgxPermissionsGuard} from 'ngx-permissions';
import { FinanceDashboardComponent } from './finance-dashboard/finance-dashboard.component';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        LayoutModule,
        PartialsModule,
        ListTimelineModule,
        WidgetChartsModule,
        RouterModule.forChild([
            {
                path: '',
                component: DashboardComponent
            },
            {
                path: 'account-tasks',
                component: AccountTasksComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['account-tasks'],
                        redirectTo: '/'
                    }
                },
            },
            {
                path: 'dashboards/:slug',
                component: FinanceDashboardComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['finance-dashboard'],
                        redirectTo: '/'
                    }
                },
            }
        ]),
        MatProgressSpinnerModule,
        ChartsModule,
        MatListModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        NgxMatDrpModule,
        MatSelectModule,
        MatIconModule,
        Daterangepicker,
        AngularMultiSelectModule
    ],
    providers: [],
    declarations: [
        DashboardComponent,
        InqStatComponent,
        SalesOutstandingChartComponent,
        TargetSalesChartComponent,
        SumPipe,
        AccountTasksComponent,
        FinanceDashboardComponent
    ]
})
export class DashboardModule {
}
