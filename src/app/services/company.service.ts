import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class CompanyService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient) {
        this.API_URL = environment.API_URL;
    }

    getSettingList(): Observable<any> {
        return this.http.get(this.API_URL + '/companies/setting-list');
    }

    saveCompany(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/companies', data);
    }

    validateCompany(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/companies/validate', data);
    }

    getCompanies(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/companies', {params: params});
    }

    getCompanyTypes() {
        return [
            {
                id: 0,
                name: 'Company'
            },
            {
                id: 1,
                name: 'Travel Agent'
            },
            {
                id: 2,
                name: 'OTA'
            }
        ];
    }

    getCompanyDetail(id): Observable<any> {
        return this.http.get(this.API_URL + '/companies/find/' + id);
    }

    searchCompanies(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/companies/search', {params: params});
    }

    getHistory(id): Observable<any> {
        return this.http.get(this.API_URL + '/companies/history/' + id);
    }
}

