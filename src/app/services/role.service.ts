import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class RoleService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient) {
        this.API_URL = environment.API_URL;
    }


    getRoles(): Observable<any> {
        return this.http.get(this.API_URL + '/role-manage');
    }

    findRole(id): Observable<any> {
        return this.http.get(this.API_URL + '/role-manage/' + id + '/find');
    }

    saveRole(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/role-manage', data);
    }

    defaultPermissions() {
        let defaultPermissions: any;
        defaultPermissions = {
            'inquiries': {
                view: 'inquiries-view-responsible',
                add: 'inquiries-add',
                update: 'inquiries-update',
                delete: 'inquiries-delete-responsible'
            },
            'leads': {
                view: '',
                add: '',
                update: '',
                delete: ''
            },
            'payment': {
                view: '',
                add: '',
                update: '',
                delete: ''
            },
            'dsr': {
                view: '',
                add: '',
                update: '',
                delete: ''
            },
            'companies': {
                view: '',
                add: '',
                update: '',
                delete: ''
            },
            'sales target': {
                view: '',
                add: '',
                update: '',
                delete: ''
            },

        };

        let otherPermissions: any;
        otherPermissions = {
            settings: '',
            'dashboard': '',
            'finance_dashboard': '',
            'user_management': '',
            'role_management': '',
            'company_manage': '',
            'hotel_manage': '',
            'rooms_manage': '',
            'rate_plans': '',
            'rate_inventory': '',
            'email_templates': 'email-templates',
            'action_to_confirm_payment': '',
            'account_tasks': '',
            'booking_outstanding_report': '',
            'booking_discount_report': '',
            'booking_status_report': '',
            'booking_report': '',
            'cm_settings': '',
            'cm_logs': '',
            'email_logs': '',
            'revert_sale': '',
			'release_booking': '',
            'company_credit_approve': ''
        };

        return {
            module_permissions: defaultPermissions,
            other_permissions: otherPermissions
        };
    }

    preparePermissions(permissions: any = []) {

        const defaultPerms = this.defaultPermissions();

        if (!permissions || !permissions.length) {
            return defaultPerms;
        }

        let modulePerms: any;
        let otherPerms: any;
        modulePerms = defaultPerms.module_permissions;
        otherPerms = defaultPerms.other_permissions;

        Object.keys(otherPerms).map((otherPerm) => {
            let permGranted;

            const otherParamName = otherPerm.replace(/_/g, '-');
            permGranted = permissions.indexOf(otherParamName);
            if (permGranted < 0) {
                otherPerms[otherPerm] = '';
            } else {
                otherPerms[otherPerm] = otherParamName;
            }
        });


        Object.keys(modulePerms).map((module_name) => {
            console.log(module_name);

            let peramName = '';
            let permGranted;
            peramName = module_name.toLowerCase() + '-view';
            permGranted = permissions.indexOf(peramName);
            if (permGranted < 0) {
                peramName = module_name.toLowerCase() + '-view-responsible';
                permGranted = permissions.indexOf(peramName);
                if (permGranted < 0) {
                    peramName = '';
                }
            }

            modulePerms[module_name].view = peramName;

            peramName = module_name.toLowerCase() + '-add';
            permGranted = permissions.indexOf(peramName);
            if (permGranted < 0) {
                peramName = '';
            }
            modulePerms[module_name].add = peramName;

            peramName = module_name.toLowerCase() + '-update';
            permGranted = permissions.indexOf(peramName);
            if (permGranted < 0) {
                peramName = module_name.toLowerCase() + '-update-responsible';
                permGranted = permissions.indexOf(peramName);
                if (permGranted < 0) {
                    peramName = '';
                }
            }
            modulePerms[module_name].update = peramName;

            peramName = module_name.toLowerCase() + '-delete';
            permGranted = permissions.indexOf(peramName);
            if (permGranted < 0) {
                peramName = module_name.toLowerCase() + '-delete-responsible';
                permGranted = permissions.indexOf(peramName);
                if (permGranted < 0) {
                    peramName = '';
                }
            }
            modulePerms[module_name].delete = peramName;
        });

        return {
            module_permissions: modulePerms,
            other_permissions: otherPerms
        };
    }

}

