import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {LayoutUtilsService} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {CommonService} from '../../services/common.service';
import {ToastrService} from 'ngx-toastr';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {fromEvent, merge} from 'rxjs';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {SettingsService} from '../../services/settings.service';
import {UserService} from '../../services/user.service';
import {SettingDataSource} from '../../Models/data-sources/masters/setting.dataSource';
import {RatePlanDetailComponent} from './rate-plan-detail/rate-plan-detail.component';
import {HotelService} from '../../services/hotel.service';
import {CompanyService} from '../../services/company.service';

@Component({
    selector: 'm-rate-plans',
    templateUrl: './rate-plans.component.html',
    styleUrls: ['./rate-plans.component.scss'],
    providers: [SettingsService, UserService, CommonService, HotelService, CompanyService]
})
export class RatePlansComponent implements OnInit {
    isCollapsedFilter: boolean = true;
    planFilters: any = {};

    isDeleting: boolean = false;
    isSaving: boolean = false;
    dataSource: any = SettingDataSource;
    users: any[] = [];
    hotels: any[] = [];
    hotelRoomTypes: any[] = [];
    departments: any[] = [];

    roomTypes: any[] = [];
    ratePlanTypes: any[] = [];
    travelAgents: any[] = [];
    mealPlans: any[] = [];

    bookingTypes: any[] = [];

    selection = new SelectionModel<any>(true, []);
    itemsPerPage: number = 10;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('searchInput') searchInput: ElementRef;

    constructor(public dialog: MatDialog,
                private layoutUtilsService: LayoutUtilsService,
                private commonService: CommonService,
                private hotelService: HotelService,
                private toastService: ToastrService,
                private userService: UserService,
                private companyService: CompanyService,
                private settingService: SettingsService) {

    }

    displayedColumns: string[] = [
        'select',
        'name',
        'hotel_name',
        'room_type_name',
        'rate_type_name',
        'min_nights',
        'rack_rate',
        'actions'
    ];

    ngOnInit() {
        this.paginator.page
            .pipe(
                tap(() => this.loadRatePlanRequest())
            )
            .subscribe();


        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(350),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadRatePlanRequest();
                })
            )
            .subscribe();


        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadRatePlanRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingService);
        this.dataSource.loadRatePlans(queryParams);
        this.dataSource.entitySubject.subscribe(res => {
            this.users = res;
        });


        //
        this.getRatePlanTypes();
        this.getBookingTypes();
        this.getRoomTypes();
        this.getHotels();
        this.getTravelAgents();
        this.loadMealPlans();
    }

    loadRatePlanRequest() {
        const q = this.searchInput.nativeElement.value;
        let queryParams: any;
        queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);

        if (Array.isArray(this.planFilters.hotel_id)) {
            queryParams.hotel_id = this.planFilters.hotel_id.join(',');
        }
        if (Array.isArray(this.planFilters.room_type_id)) {
            queryParams.room_type_id = this.planFilters.room_type_id.join(',');
        }
        // queryParams = Object.assign(queryParams, this.planFilters);

        this.dataSource.loadRatePlans(queryParams);
        this.selection.clear();
    }

    getHotelRoomTypes() {
        let reqParams: any;
        reqParams = {};
        if (!this.planFilters.hotel_id) {
            return;
        }
        reqParams.hotel_id = this.planFilters.hotel_id.join(',');
        if (Array.isArray(this.planFilters.hotel_id)) {
            if (this.planFilters.hotel_id.length < 1) {
                return;
            }
            reqParams.hotel_id = this.planFilters.hotel_id.join(',');
        }
        this.settingService.getRoomTypeList(reqParams)
            .subscribe(response => {
                    this.hotelRoomTypes = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    openPlanDetail(detail: any = {}) {
        const dialogRef = this.dialog.open(RatePlanDetailComponent, {
            data: {
                currentRecord: detail,
                roomTypes: this.roomTypes,
                ratePlanTypes: this.ratePlanTypes,
                bookingTypes: this.bookingTypes,
                hotels: this.hotels,
                travelAgents: this.travelAgents,
                mealPlans: this.mealPlans
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadRatePlanRequest();
            }
        });
    }

    /** SELECTION */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.users.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.selection.selected.length === this.users.length) {
            this.selection.clear();
        } else {
            this.users.forEach(row => this.selection.select(row));
        }
    }

    deleteRecord(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.isDeleting = true;
            this.settingService.deleteRatePlans(record.id)
                .subscribe(response => {
                        this.isDeleting = false;
                        this.refreshCurrentPage();
                        this.toastService.success('Deleted Successfully', 'Done');
                    },
                    error => {
                        this.isDeleting = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    });
        });
    }

    getHotels() {
        this.hotelService.getHotelList()
            .subscribe(response => {
                    this.hotels = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    getRoomTypes() {
        this.settingService.getRoomTypes()
            .subscribe(response => {
                    this.roomTypes = response.items;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    getRatePlanTypes() {
        this.settingService.getRateTypes()
            .subscribe(response => {
                    this.ratePlanTypes = response.items;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    getBookingTypes() {
        this.settingService.getInquiryTypes()
            .subscribe(response => {
                    this.bookingTypes = response.items;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    getTravelAgents() {
        this.companyService.searchCompanies({type: 1})
            .subscribe(response => {
                    this.travelAgents = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    refreshCurrentPage() {
        this.paginator.pageIndex -= 1;
        this.loadRatePlanRequest();
    }

    loadMealPlans() {
        this.settingService.getMealPlans()
            .subscribe(response => {
                    this.mealPlans = response.items;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    copyRatePlan(ratePlan) {
        ratePlan = Object.assign({}, ratePlan);
        ratePlan.name = ratePlan.name + '- Copy';
        delete ratePlan.id;
        delete ratePlan.key;

        this.isSaving = true;
        this.settingService.saveRatePlans(ratePlan)
            .subscribe(response => {
                    console.log(response);
                    this.isSaving = false;
                    this.toastService.success('Copied Successfully', 'Success');
                    this.loadRatePlanRequest();
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    applyFilter() {
        this.loadRatePlanRequest();
    }

    resetFilter() {
        this.planFilters = {};
        this.isCollapsedFilter = true;
        this.loadRatePlanRequest();
    }
}
