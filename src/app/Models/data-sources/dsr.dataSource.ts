import {finalize, tap} from 'rxjs/operators';
import {BaseDataSource} from '../../content/pages/components/apps/e-commerce/_core/models/data-sources/_base.datasource';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {DsrService} from '../../services/dsr.service';

export class DSRDataSource extends BaseDataSource {

    constructor(private dsrService: DsrService) {
        super();
    }

    loadDSRs(queryParams: QueryParamsModel) {
        this.loadingSubject.next(true);
        this.dsrService.get(queryParams).pipe(
            tap(res => {
                const result: any = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }
}
