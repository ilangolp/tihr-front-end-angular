import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'THActiveFilter'
})
export class THActiveFilterPipe implements PipeTransform {
    transform(items: any, filterField: any, filterValue: any, includeField: any = '', includeValue: any = ''): any {
        if (Array.isArray(items)) {
            let result: any = [];
            if (filterField && filterValue) {
                if (!includeField || !includeValue) {
                    result = items.filter(itm => {
                        return itm[filterField] === filterValue;
                    });
                } else if (includeField && includeValue) {
                    result = items.filter(itm => {
                        return (itm[filterField] === filterValue) || itm[includeField] === includeValue;
                    });
                } // console.log(result);
                return result;
            }
        }

        return items;
    }
}
