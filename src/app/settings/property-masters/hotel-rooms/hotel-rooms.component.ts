import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {SettingDataSource} from '../../../Models/data-sources/masters/setting.dataSource';
import {MatDialog, MatPaginator, MatSlideToggleChange, MatSort} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {SettingsService} from '../../../services/settings.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {fromEvent, merge} from 'rxjs';
import {QueryParamsModel} from '../../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {RoomDetailComponent} from './room-detail/room-detail.component';
import {SelectionModel} from '@angular/cdk/collections';
import {CopyRoomComponent} from './copy-room/copy-room.component';

@Component({
    selector: 'm-hotel-rooms',
    templateUrl: './hotel-rooms.component.html',
    styleUrls: ['./hotel-rooms.component.scss'],
    providers: [SettingsService]
})
export class HotelRoomsComponent implements OnInit {
    isSaving: boolean = false;
    isLoading: boolean = false;
    isDeleting: boolean = false;
    itemsPerPage: number = 10;
    roomSettings: any = {};
    hotelRoomFilters: any = {};
    hotelRooms: any[] = [];
    isCollapsedFilter: boolean = true;
    dataSource: any = SettingDataSource;
    selection = new SelectionModel<any>(true, []);
    displayedColumns: string[] = [
        'select',
        'name',
        'code',
        'hotel_code',
        'room_type_name',
        'bed_type_name',
        // 'is_enabled',
        'actions'
    ];

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('searchInput') searchInput: ElementRef;

    constructor(private dialog: MatDialog,
                private toastService: ToastrService,
                private layoutUtilsService: LayoutUtilsService,
                public  settingsService: SettingsService,
                private modalService: NgbModal) {
    }

    ngOnInit() {
        this.getSettings();

        fromEvent(this.searchInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(350),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;
                    this.loadHotelRoomsRequest();
                })
            )
            .subscribe();


        this.paginator.page
            .pipe(
                tap(() => this.loadHotelRoomsRequest())
            )
            .subscribe();

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                    this.loadHotelRoomsRequest();
                })
            )
            .subscribe();

        const queryParams = new QueryParamsModel(
            '',
            'asc',
            '',
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
        );
        this.dataSource = new SettingDataSource(this.settingsService);
        this.dataSource.loadHotelRooms(queryParams);
        this.dataSource.entitySubject.subscribe(res => (this.hotelRooms = res));
    }


    openDetailForm(record: any = {}) {
        const dialogRef = this.dialog.open(RoomDetailComponent, {
            width: '500px',
            data: {
                record: record,
                settings: this.roomSettings
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.paginator.pageIndex -= 1;
                this.loadHotelRoomsRequest();
            }
        });
    }

    copyRoom(record: any = {}) {
        const dialogRef = this.dialog.open(CopyRoomComponent, {
            width: '400px',
            data: {
                record: record,
                settings: this.roomSettings
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.paginator.pageIndex -= 1;
                this.loadHotelRoomsRequest();
            }
        });
    }

    loadHotelRoomsRequest() {
        const q = this.searchInput.nativeElement.value;
        let queryParams = new QueryParamsModel(
            q,
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);

        queryParams = Object.assign(queryParams, this.hotelRoomFilters);

        this.dataSource.loadHotelRooms(queryParams);
        this.selection.clear();
    }

    deleteRecord(record) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            this.isDeleting = true;
            this.settingsService.deleteRooms(record.id)
                .subscribe(response => {
                        this.isDeleting = false;
                        this.paginator.pageIndex -= 1;
                        this.loadHotelRoomsRequest();
                        this.toastService.success('Deleted Successfully', 'Done');
                    },
                    error => {
                        this.isDeleting = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    });
        });
    }

    getSettings() {
        this.settingsService.getHotelRoomSettings()
            .subscribe(response => {
                    this.roomSettings = response;
                    console.log(this.roomSettings.hotels);
                },
                error => {
                    this.isDeleting = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    applyFilter() {
        this.paginator.pageIndex = 0;
        this.loadHotelRoomsRequest();
    }

    resetFilter() {
        this.paginator.pageIndex = 0;
        this.searchInput.nativeElement.value = '';
        this.hotelRoomFilters = {};
        this.isCollapsedFilter = true;
        this.loadHotelRoomsRequest();
    }

    /** SELECTION */
    isAllSelected(): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.hotelRooms.length;
        return numSelected === numRows;
    }

    masterToggle() {
        if (this.selection.selected.length === this.hotelRooms.length) {
            this.selection.clear();
        } else {
            this.hotelRooms.forEach(row => this.selection.select(row));
        }
    }

    onStatusChange(currentRecord, event: MatSlideToggleChange) {
        const dialogRef = this.layoutUtilsService.deleteElement(
            'Confirm Operation',
            'Are you sure to perform action?',
            'saving...',
            'Yes',
            'No'
        );
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                currentRecord.is_enabled = !event.checked;
                return;
            }
            this.save(currentRecord, event.checked);
        });
    }

    save(currentRecord, new_is_enabled) {
        currentRecord.isSaving = true;
        currentRecord.is_enabled = new_is_enabled;
        this.settingsService.saveHotelRooms(currentRecord)
            .subscribe(response => {
                    currentRecord.isSaving = false;
                    currentRecord.is_enabled = response.is_enabled;
                    this.toastService.success('Saved Successfully', 'Success');
                },
                error => {
                    currentRecord.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }
}
