import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class CommonService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient) {
        this.API_URL = environment.API_URL;
    }

    getCountries(): Observable<any> {
        return this.http.get(this.API_URL + '/countries');
    }

    getStates(countryId): Observable<any> {
        return this.http.get(this.API_URL + '/country-states/' + countryId);
    }

    getCities(stateId): Observable<any> {
        return this.http.get(this.API_URL + '/state-cities/' + stateId);
    }

    searchCities(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/search-cities', {params: params});
    }

    getPaymentModes(): Observable<any> {
        return this.http.get(this.API_URL + '/payment-modes');
    }

    getDepartments(): Observable<any> {
        return this.http.get(this.API_URL + '/departments');
    }

    getDesignation(): Observable<any> {
        return this.http.get(this.API_URL + '/designations');
    }

    getNotifications(): Observable<any> {
        return this.http.get(this.API_URL + '/users/notifications');
    }

    archiveNotification(id): Observable<any> {
        return this.http.get(this.API_URL + '/users/notifications/' + id + '/archive');
    }

    fetchHistory(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/fetch-history', {params: params});
    }

}

