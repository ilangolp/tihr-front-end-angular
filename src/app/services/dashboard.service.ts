import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class DashboardService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient) {
        this.API_URL = environment.API_URL;
    }

    inquiryStat(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/dashboard/inquiry-statistic', {params: params});
    }

    hotelOutstanding(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/dashboard/hotel-outstanding', {params: params});
    }

    salesTarget(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/dashboard/sales-target', {params: params});
    }

    tasks(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/dashboard/tasks', params);
    }

    pendingApproval(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/dashboard/pending-approval', params);
    }

    getInquirySettingLists(): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/setting-list');
    }
}

