import {Component, OnInit} from '@angular/core';
import {TokenStorage} from '../core/auth/token-storage.service';
import {NgxPermissionsService} from 'ngx-permissions';
import {RoleService} from '../services/role.service';

@Component({
    selector: 'm-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss'],
    providers: [RoleService]
})
export class SettingsComponent implements OnInit {

    currentMenu: string = 'settings';
    loggedInUser: any = {};

    constructor(private tokenStorage: TokenStorage,
                private permissions: NgxPermissionsService,
                private roleService: RoleService) {
        this.loggedInUser = this.tokenStorage.getUserDetail();
        if (localStorage.getItem('lastOpenSetting')) {
            this.currentMenu = localStorage.getItem('lastOpenSetting');
        }

        const isGranted = this.loggedInUser.permissions.indexOf(this.currentMenu);
        if (isGranted < 0) {
            this.currentMenu = '';
            const otherPermissions: any = this.roleService.defaultPermissions();
            Object.keys(otherPermissions.other_permissions).forEach(oprm => {
                const otherParamName = oprm.replace(/_/g, '-');
                const isoPerm = this.loggedInUser.permissions.indexOf(otherParamName);
                if (isoPerm >= 0 && this.currentMenu !== '') {
                    this.currentMenu = otherParamName;
                }
            });
        }

        const perms = this.permissions.getPermissions();
        // console.log(perms);
        // console.log(perms['inquiries-view-responsible']);
        // console.log(perms['inquiries-view2010']);
    }

    ngOnInit() {
    }

    setCurrentMenu(menu) {
        this.currentMenu = menu;
        localStorage.setItem('lastOpenSetting', this.currentMenu);
    }

}
