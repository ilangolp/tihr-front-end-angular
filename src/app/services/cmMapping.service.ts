import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class CMMappingService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient) {
        this.API_URL = environment.API_URL;
    }

    getHotelMappings(hotelId): Observable<any> {
        return this.http.get(this.API_URL + '/settings/cm-settings/' + hotelId);
    }

    saveHotelMappings(hotelId, params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/cm-settings/' + hotelId, params);
    }
}
