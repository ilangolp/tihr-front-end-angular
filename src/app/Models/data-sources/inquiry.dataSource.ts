import {finalize, tap} from 'rxjs/operators';
import {BaseDataSource} from '../../content/pages/components/apps/e-commerce/_core/models/data-sources/_base.datasource';
import {InquiryService} from '../../inquiry/inquiry.service';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';

export class InquiryDataSource extends BaseDataSource {
    constructor(private inquiryService: InquiryService) {
        super();
    }

    loadInquiries(queryParams: any) {
        this.loadingSubject.next(true);
        this.inquiryService.getInquiriesList(queryParams).pipe(
            tap(res => {
                // Comment this when you start work with real server
                // This code imitates server calls
                // START
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
                // END

                // Uncomment this when you start work with real server
                // START
                // this.entitySubject.next(res.items);
                // this.paginatorTotalSubject.next(res.totalCount);
                // END
            }),
            // catchError(err => of(new QueryResultsModel([], err))),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }

    loadLeads(queryParams: any) {
        this.loadingSubject.next(true);
        this.inquiryService.getLeadList(queryParams).pipe(
            tap(res => {
                // Comment this when you start work with real server
                // This code imitates server calls
                // START
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
                // END

                // Uncomment this when you start work with real server
                // START
                // this.entitySubject.next(res.items);
                // this.paginatorTotalSubject.next(res.totalCount);
                // END
            }),
            // catchError(err => of(new QueryResultsModel([], err))),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }
}
