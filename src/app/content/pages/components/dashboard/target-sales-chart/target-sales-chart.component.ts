import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {DashboardService} from '../../../../../services/dashboard.service';
import * as moment from 'moment';

@Component({
    selector: 'm-target-sales-chart',
    templateUrl: './target-sales-chart.component.html',
    styleUrls: ['./target-sales-chart.component.scss'],
    providers: [DashboardService]
})
export class TargetSalesChartComponent implements OnChanges {

    chartSettings: any = {fromDate: '', toDate: ''};
    @Input() fromDate: any;
    @Input() toDate: any;

    isLoadingSalesTarget: boolean = false;
    public salesTargetChartData: number[] = [];
    salesTargetChartBy: string = 'Revenue';
    public salesTargetLabels: string[] = ['Target', 'Sales'];
    public doughnutChartType: string = 'doughnut';

    public colors: any = [{
        borderColor: 'red',
        backgroundColor: 'green',
        pointBackgroundColor: 'orange',
        pointBorderColor: 'blue,'
    }, {
        borderColor: 'red',
        backgroundColor: 'green',
        pointBackgroundColor: 'orange',
        pointBorderColor: 'blue,'
    }];

    public chartOptions: any = {
        animation: {
            animateRotate: true,
            render: false
        }
    };

    currentMonth: any;
    filterMonths: any[] = [];

    constructor(private toastService: ToastrService, private dashboardService: DashboardService) {

        for (let i = 0; i <= 11; i++) {
            this.filterMonths.push(
                {
                    name: moment().subtract(i, 'months').format('MMM - YYYY'),
                    value: moment().subtract(i, 'months').format('YYYY-MM-DD')
                }
            );
        }

        this.currentMonth = this.filterMonths[0];
        this.changeMonthEvent();
    }

    ngOnChanges(changes: SimpleChanges) {
        /*console.log(changes);
        let dateChanged = false;
        if (changes.fromDate) {
            this.chartSettings.fromDate = changes.fromDate.currentValue;
            dateChanged = true;
        }

        if (changes.toDate) {
            this.chartSettings.toDate = changes.toDate.currentValue;
            dateChanged = true;
        }

        if (dateChanged) {
            this.salesAndTarget();
        }*/
    }

    salesAndTarget() {
        this.isLoadingSalesTarget = true;
        this.dashboardService.salesTarget(Object.assign({chartBy: this.salesTargetChartBy}, this.chartSettings))
            .subscribe(response => {
                    if (response.sales || response.target) {
                        this.salesTargetChartData = [response.target, response.sales];
                    } else {
                        this.salesTargetChartData = [];
                    }
                    this.salesTargetLabels = ['Target ' + this.salesTargetChartBy, 'Sales ' + this.salesTargetChartBy];
                    this.isLoadingSalesTarget = false;
                },
                error => {
                    this.isLoadingSalesTarget = false;
                    this.salesTargetChartData = [];
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    changeSalesRevenueBy(type) {
        this.salesTargetChartBy = type;
        this.salesAndTarget();
    }

    changeMonthEvent() {
        this.chartSettings.fromDate = this.currentMonth.value;
        this.chartSettings.toDate = this.currentMonth.value;
        this.salesAndTarget();
    }

}
