import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {RoleService} from '../../../services/role.service';

@Component({
    selector: 'm-role-detail',
    templateUrl: './role-detail.component.html',
    styleUrls: ['./role-detail.component.scss'],
    providers: [RoleService]
})
export class RoleDetailComponent implements OnInit {
    isSaving: boolean = false;
    currentPermissionCategory = 'Module';
    isLoading: boolean = false;
    roleDetail: any = {
        name: '',
        permissions: {}
    };

    constructor(public dialogRef: MatDialogRef<RoleDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private toastService: ToastrService,
                private  roleService: RoleService) {
        this.roleDetail.permissions = this.roleService.defaultPermissions();
        if (this.data) {
            if (this.data.currentRecord.id) {
                this.fetchRole(this.data.currentRecord.id);
            }
            console.log(this.roleDetail);
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    permissionCheck(permission, oldPerm) {
        this.roleDetail.permissions[permission] = true;
        this.roleDetail.permissions[oldPerm] = false;
        console.log(this.roleDetail);
    }

    removeCheck(permYes, permNo) {
        this.roleDetail.permissions[permYes] = false;
        this.roleDetail.permissions[permNo] = false;
        console.log(this.roleDetail);
    }

    save() {
        this.isSaving = true;
        this.roleService.saveRole(this.roleDetail)
            .subscribe(response => {
                    this.isSaving = false;
                    this.closeDialog();
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    fetchRole(id) {
        this.isLoading = true;
        this.roleService.findRole(id)
            .subscribe(response => {
                    this.roleDetail = response;
                    this.roleDetail.permissions = this.roleService.preparePermissions(response.permissions);
                    this.isLoading = false;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    disabledRelevantPermissions(permission) {
        permission.view = '';
        permission.add = '';
        permission.update = '';
        permission.delete = '';
        console.log(this.roleDetail);
    }

    changeViewPermissionCategory(val) {
        this.currentPermissionCategory = val;
    }
}
