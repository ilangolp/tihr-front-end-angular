import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import * as moment from 'moment';
import {RateInventoryService} from '../../../services/rateInventory.service';
import {ToastrService} from 'ngx-toastr';
import {SettingsService} from '../../../services/settings.service';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {DatePipe} from '@angular/common';

@Component({
    selector: 'm-rate-bulk-update',
    templateUrl: './rate-bulk-update.component.html',
    styleUrls: ['./rate-bulk-update.component.scss'],
    providers: [RateInventoryService, SettingsService]
})
export class RateBulkUpdateComponent implements OnInit {
    isLoading: boolean = false;
    isSaving: boolean = false;
    inventorySources: any[] = [];
    ratePlans: any[] = [];
    hotels: any[] = [];
    roomTypes: any[] = [];
    minFromDate: any = moment();
    minToDate: any = moment();
    bulkUpdate: any = {
        rate_type: 'rate',
        rate_source_id: null,
        hotel_id: null,
        room_type_id: '',
        fromDate: null,
        toDate: null,
        rate_plan_id: [],
        rate_plans: []
    };
    selectedDays: any = [
        {
            name: 'Mon',
            is_selected: true
        },
        {
            name: 'Tue',
            is_selected: true
        }, {
            name: 'Wed',
            is_selected: true
        }, {
            name: 'Thu',
            is_selected: true
        }, {
            name: 'Fri',
            is_selected: true
        }, {
            name: 'Sat',
            is_selected: true
        }, {
            name: 'Sun',
            is_selected: true
        }
    ];

    constructor(public dialogRef: MatDialogRef<RateBulkUpdateComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private toastService: ToastrService,
                private datePipe: DatePipe,
                private settingsService: SettingsService,
                private layoutUtilsService: LayoutUtilsService,
                private rateInventoryService: RateInventoryService) {
        if (this.data) {
            this.hotels = this.data.hotels;
            this.inventorySources = this.data.inventorySources;
            this.roomTypes = this.data.roomTypes;
            if (this.data.filters) {
                this.bulkUpdate = Object.assign(this.bulkUpdate, this.data.filters);
                if (this.bulkUpdate.hotel_id) {
                    this.loadRatePlans();
                }
            }
        }
    }

    ngOnInit() {
        //
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    dateChangeEvent(type: string) {
        switch (type) {
            case 'fromDate':
                this.minToDate = this.bulkUpdate[type];
                break;
        }
        this.bulkUpdate[type] = this.bulkUpdate[type].format('YYYY-MM-DD');
    }

    save() {
        let reqData: any = {};
        reqData = Object.assign({}, this.bulkUpdate);
        reqData.days = [];

        this.selectedDays.forEach(itm => {
            if (itm.is_selected === true) {
                reqData.days.push(itm.name);
            }
        });

        const selHotel = this.hotels.find(itm => {
            return itm.id === this.bulkUpdate.hotel_id;
        });

        const selRoomType = this.roomTypes.find(itm => {
            return itm.id === this.bulkUpdate.room_type_id;
        });

        const selRatePlans = this.ratePlans.filter(itm => {
            const isExist: boolean = this.bulkUpdate.rate_plan_id.indexOf(itm);
            return isExist;
        });
        let planStr = '';
        if (selRatePlans) {
            let planNames: any[];
            planNames = [];
            this.bulkUpdate.rate_plans.forEach(itm => {
                planNames.push(itm.name);
            });
            planStr = planNames.join(', ');
        }

        let updateFields: any;
        updateFields = [];
        if (this.bulkUpdate.is_rate) {
            updateFields.push('Rates');
        }
        if (this.bulkUpdate.is_min_nights) {
            updateFields.push('Min Nights');
        }
        if (this.bulkUpdate.is_max_nights) {
            updateFields.push('Max Nights');
        }
        if (this.bulkUpdate.stop_Sell) {
            updateFields.push('Stop Sell');
        }
        if (this.bulkUpdate.is_coa) {
            updateFields.push('COA');
        }
        if (this.bulkUpdate.is_cod) {
            updateFields.push('COD');
        }

        let desc: string = '<table class="table table-bordered">' +
            '<tr>' +
            '<td>Hotel: </td><td>' + selHotel.code + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Room Type: </td><td>' + selRoomType.name + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Plans:</td><td> ' + planStr + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Date:</td><td> ' + this.datePipe.transform(this.bulkUpdate.fromDate, 'dd-MM-yyyy') + ' - ' + this.datePipe.transform(this.bulkUpdate.toDate, 'dd-MM-yyyy') + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Days:</td><td> ' + reqData.days.join(', ') + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td>Update:</td><td> ' + updateFields.join(', ') + '</td>' +
            '</tr>';
        desc += '</table>';


        const dialogRef = this.layoutUtilsService.deleteElement(
            'Are you sure to update following ?',
            desc,
            'Updating...',
            'Yes',
            'No'
        );
        dialogRef.afterClosed().subscribe(res => {

            if (!res) {
                return;
            }

            this.isSaving = true;
            this.rateInventoryService.bulkUpdate(reqData)
                .subscribe(response => {
                        this.isSaving = false;
                        this.toastService.success('Saved Success', 'Success');
                        this.closeDialog(response);
                    },
                    error => {
                        this.isSaving = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                    });
        });
    }

    loadRatePlans() {
        let params: any = {};
        params = {};
        params.hotel_id = this.bulkUpdate.hotel_id;
        params.room_type_id = this.bulkUpdate.room_type_id;

        this.settingsService.getRatePlanList(params)
            .subscribe(response => {
                    this.isLoading = false;
                    this.ratePlans = response;
                    console.log(this.ratePlans);
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    canApplyBulkUpdate() {
        let result: boolean;
        result = false;
        let isError: boolean = false;

        if (this.bulkUpdate.is_rate ||
            this.bulkUpdate.is_min_nights ||
            this.bulkUpdate.is_max_nights ||
            this.bulkUpdate.stop_Sell ||
            this.bulkUpdate.is_coa ||
            this.bulkUpdate.is_cod) {
            result = true;
        } else {
            isError = true;
        }

        const selDays = this.selectedDays.filter(item => item.is_selected === true);
        if (!isError) {
            if (selDays.length < 1) {
                result = false;
            }
        }

        if (isError) {
            result = false;
        }

        return result;
    }

    ratePlansChange() {
        console.log(this.bulkUpdate.rate_plans);
    }
}
