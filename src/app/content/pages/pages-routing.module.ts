import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from './pages.component';
import {ActionComponent} from './header/action/action.component';
import {NgxPermissionsGuard} from 'ngx-permissions';
import {ProfileComponent} from './header/profile/profile.component';
import {ErrorPageComponent} from './snippets/error-page/error-page.component';
import {InquiryComponent} from '../../inquiry/inquiry.component';
import {SettingsComponent} from '../../settings/settings.component';
import {CompaniesComponent} from '../../companies/companies.component';
import {DsrComponent} from '../../dsr/dsr.component';
import {SalesTargetComponent} from '../../sales-target/sales-target.component';
import {BookingOutstandingReportComponent} from '../../reports/booking-outstanding-report/booking-outstanding-report.component';
import {BookingComponent} from '../../inquiry/booking/booking.component';
import {BookingReportComponent} from '../../reports/booking-report/booking-report.component';
import {LeadComponent} from '../../inquiry/lead/lead.component';
import {WelcomeComponent} from '../../welcome/welcome.component';
import {InventoryReportComponent} from '../../reports/inventory-report/inventory-report.component';
import {BookingStatusReportComponent} from '../../reports/booking-status-report/booking-status-report.component';
import {BookingDiscountReportComponent} from '../../reports/booking-discount-report/booking-discount-report.component';

const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
            permissions: {
                // only: ['ADMIN', 'USER'],
                // except: ['GUEST'],
                // redirectTo: '/login'
            }
        },
        children: [
            {
                path: 'welcome',
                component: WelcomeComponent,
                canActivate: [NgxPermissionsGuard],
            },
            {
                path: '',
                loadChildren: './components/dashboard/dashboard.module#DashboardModule',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['dashboard'],
                        redirectTo: 'welcome'
                    }
                }
            },
            {
                path: 'finance-dashboard',
                loadChildren: './components/dashboard/dashboard.module#DashboardModule',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['finance-dashboard'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'account-tasks',
                loadChildren: './components/dashboard/dashboard.module#DashboardModule',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['account-tasks'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'mail',
                loadChildren: './components/apps/mail/mail.module#MailModule'
            },
            {
                path: 'ecommerce',
                loadChildren: './components/apps/e-commerce/e-commerce.module#ECommerceModule'
            },
            {
                path: 'ngbootstrap',
                loadChildren: './components/ngbootstrap/ngbootstrap.module#NgbootstrapModule'
            },
            {
                path: 'material',
                loadChildren: './components/material/material.module#MaterialModule'
            },
            {
                path: 'metronic',
                loadChildren: './components/metronic/metronic.module#MetronicModule'
            },
            {
                path: 'user-management',
                loadChildren: './components/user-management/user-management.module#UserManagementModule'
            },
            {
                path: 'audit-log',
                loadChildren: './components/apps/audit-log/audit-log.module#AuditLogModule'
            },
            {
                path: 'builder',
                loadChildren: './builder/builder.module#BuilderModule'
            },
            {
                path: 'header/actions',
                component: ActionComponent
            },
            {
                path: 'profile',
                component: ProfileComponent
            },
            {
                path: 'inquiries',
                component: InquiryComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['inquiries-view', 'inquiries-view-responsible'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'leads',
                component: LeadComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['leads-view', 'leads-view-responsible'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'settings',
                component: SettingsComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['settings', 'user-management', 'role-management',
                            'company-manage', 'hotel-manage', 'rooms-manage', 'rate-plans',
                            'rate-inventory', 'email-templates', 'action-to-confirm-payment'],
                        redirectTo: '/'
                    }
                },
            },
            {
                path: 'companies',
                component: CompaniesComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['companies-view', 'companies-view-responsible'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'dsr',
                component: DsrComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['dsr-view', 'dsr-view-responsible'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'sales-target',
                component: SalesTargetComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['sales target-view', 'sales target-view-responsible'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'reports/booking-outstanding',
                component: BookingOutstandingReportComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['booking-outstanding-report'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'reports/booking-discount',
                component: BookingDiscountReportComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['booking-discount-report'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'reports/booking-report',
                component: BookingReportComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['booking-report'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'reports/inventory-report',
                component: InventoryReportComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['booking-report'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'booking',
                component: BookingComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['inquiries-view', 'inquiries-view-responsible'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'booking/:id',
                component: BookingComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['inquiries-view', 'inquiries-view-responsible'],
                        redirectTo: '/error/7'
                    }
                }
            },
            {
                path: 'reports/booking-status-report',
                component: BookingStatusReportComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: ['booking-status-report'],
                        redirectTo: '/error/7'
                    }
                }
            },
        ]
    },
    {
        path: 'login',
        canActivate: [NgxPermissionsGuard],
        loadChildren: './auth/auth.module#AuthModule',
        data: {
            permissions: {
                except: 'ADMIN'
            }
        },
    },
    {
        path: '404',
        component: ErrorPageComponent
    },
    {
        path: 'error/:type',
        component: ErrorPageComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule {
}
