import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {InquiryService} from '../../inquiry/inquiry.service';
import {UserService} from '../../services/user.service';
import {HotelService} from '../../services/hotel.service';
import {ReportService} from '../../services/report.service';
import {BookingOutStandingDataSource} from '../../Models/data-sources/reports/booking-outstanding.dataSource';
import {environment} from '../../../environments/environment';
import {SpinnerButtonOptions} from '../../content/partials/content/general/spinner-button/button-options.interface';
import {SelectionModel} from '@angular/cdk/collections';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {DaterangePickerComponent} from 'ng2-daterangepicker';
import {FormControl} from '@angular/forms';
import {DataSource} from '@oasisdigital/angular-material-search-select';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import * as moment from 'moment';
import {LayoutUtilsService} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {UtilsService} from '../../core/services/utils.service';
import {$} from 'protractor';

@Component({
    selector: 'm-booking-report',
    templateUrl: './booking-report.component.html',
    styleUrls: ['./booking-report.component.scss'],
    providers: [InquiryService, UserService, HotelService, ReportService, UtilsService]
})
export class BookingReportComponent implements OnInit {
    isCollapsedFilter: boolean = false;
    customFilters: any = {
        rooms_filter: {
            operator: '',
            range_from: 0,
            range_to: 10
        },
    };
    dataSource: any = BookingOutStandingDataSource;
    inquiryStatusList: any = [];
    inquirySubStatusList: any = [];
    inquiryTypesList: any = [];
    hotelList: any = [];
    sortField = 'check_in_date';
    sortOrderBy = 'asc';
    reportRecordLists: any = [];
    isLoading: boolean = false;
    itemsPerPage: number = 10;
    assignToUserList: any = [];
    travelAgents: any = [];
    companies: any = [];
    bookingReportData: any = [];
    baseUrl: any = environment.Base_Url;
    spinner: SpinnerButtonOptions = {
        active: false,
        spinnerSize: 18,
        raised: true,
        buttonColor: 'primary',
        spinnerColor: 'accent',
        fullWidth: false
    };
    hotelListSettings: any = {};
    selectedHotelLists = [];
    statusSettings: any = {};
    selectedStatuses = [];
    inquiryTypesSettings: any = {};
    selectedInquiryTypes = [];
    displayedColumns: string[] = [
        'inquiry_id',
        'booking_id',
        'hotel',
        'status',
        'inquiry_sub_status',
        'check_in_date',
        'check_out_date',
        'name',
        'phone',
        'room_type_name',
        'room_count',
        'rate_plan_name',
        'meal_plan_name',
        'source',
        'offer_rate',
        'paid_amount',
        'outstanding_amount',
        'assign_to'
    ];

    selection = new SelectionModel<any>(true, []);

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('checkInDateInput') checkInDateInput: DaterangePickerComponent;


    isVisibleCompanies: any = false;
    isVisibleAgents: any = false;

    travel_agent = new FormControl(null, []);
    travelAgentDataSource: DataSource;

    dateOptions: any = {};

    inquirySourceList: any = [];
    selectedSourcesLists = [];
    sourceSettings: any = {};

    companyList = new FormControl(null, []);
    companyDataSource: DataSource;
    companyLists: any = [];
    selectedCompanies: any = [];
    companySettings: any = {};
    searchCompanyTypes: any = [];

    dropdownList = [];
    selectedItems = [];
    dropdownSettings1 = {};

    constructor(private modalService: NgbModal,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private http: HttpClient,
                private inquiryService: InquiryService,
                private reportService: ReportService,
                private hotelService: HotelService,
                private toastService: ToastrService,
                private utilsService: UtilsService,
                private layoutUtilsService: LayoutUtilsService,
                private  userService: UserService) {

        this.dateOptions = this.utilsService.getCalenderConfig({
            startDate: moment(),
            endDate: moment().add(2, 'days').endOf('day'),
        });

        const apiURL = environment.API_URL + '/companies/';

        this.hotelListSettings = this.utilsService.getMultiSelctConfig({text: 'Select Hotels', groupBy: 'category', labelKey: 'code'});
        this.statusSettings = this.utilsService.getMultiSelctConfig({text: 'Select Status', badgeShowLimit: 2});
        this.inquiryTypesSettings = this.utilsService.getMultiSelctConfig({text: 'Select Type', badgeShowLimit: 1});
        this.sourceSettings = this.utilsService.getMultiSelctConfig({text: 'Select Source'});
        this.companySettings = this.utilsService.getMultiSelctConfig({text: 'Select Company / OTA / TA'});
    }

    ngOnInit() {
        this.initFilters();
        this.getSettingLists();
    }

    initFilters() {
        this.customFilters = {
            check_in_date_fromDate: moment().format('YYYY-MM-DD'),
            check_in_date_toDate: moment().add(2, 'days').endOf('day').format('YYYY-MM-DD'),
            check_in_date_filter: true,
            rooms_filter: {
                operator: '',
                range_from: 0,
                range_to: 10
            },
        };
    }

    leadReportDataRequest() {
        let queryParams: any = new QueryParamsModel(
            '',
            this.sort.direction,
            this.sort.active,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize);

        queryParams = Object.assign(queryParams, this.customFilters);

        // delete queryParams.check_in_date_filter;
        delete queryParams.check_out_date_filter;
        delete queryParams.inquiry_date_filter;

        if (!this.customFilters.inquiry_date_filter) {
            delete queryParams.inquiry_fromDate;
            delete queryParams.inquiry_toDate;
        }

        // if (!this.customFilters.check_in_date_filter) {
        //     delete queryParams.check_in_date_fromDate;
        //     delete queryParams.check_in_date_toDate;
        // }

        if (!this.customFilters.check_out_date_filter) {
            delete queryParams.check_out_date_fromDate;
            delete queryParams.check_out_date_toDate;
        }

        this.dataSource.loadBookingReportData(queryParams);
    }

    applyFilter() {
        // this.paginator.pageIndex = 0;
        // this.onHotelSelect();
        if (this.isVisibleAgents) {
            this.customFilters.travel_agent_id = this.travel_agent.value;
        }
        if (this.isVisibleCompanies) {
            this.customFilters.company_id = this.companyList.value;
        }
        this.loadReportData();
        // this.leadReportDataRequest();
    }

    applySearchFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    resetFilter() {
        this.selectedHotelLists = [];
        this.selectedStatuses = [];
        this.selectedInquiryTypes = [];
        this.selectedSourcesLists = [];
        this.companyList = new FormControl(null, []);
        this.travel_agent = new FormControl(null, []);
        /*this.isCollapsedFilter = true;*/
        this.initFilters();
        this.paginator.pageIndex = 0;
        this.leadReportDataRequest();
    }

    getSettingLists() {
        this.inquiryService.getInquirySettingLists()
            .subscribe(response => {
                    this.assignToUserList = response.users;
                    this.inquirySourceList = response.sources;
                    this.inquiryStatusList = response.statuses;
                    this.inquirySubStatusList = response.subStatuses;
                    // this.hotelList = response.hotels;
                    this.inquiryTypesList = response.types;
                    this.travelAgents = response.travel_agents;
                    this.companies = response.companies;

                    this.inquiryStatusList.forEach(itm => {
                        if (itm.is_confirmed === true || itm.is_sold === true) {
                            this.selectedStatuses.push(itm);
                        }
                    });
                    this.onStatusSelect();

                    this.hotelList = [];
                    response.hotels.forEach(itm => {
                        if (itm.is_enabled) {
                            itm.category = 'Active';
                            this.selectedHotelLists.push(itm);
                        } else {
                            itm.category = 'Disabled';
                        }
                        this.hotelList.push(itm);
                    });
                    this.hotelList = this.hotelList.sort((a, b) => a.category.localeCompare(b.category));
                    this.onHotelSelect();
                    /*this.customFilters.hotel_id = [];
                    this.hotelList.forEach(itm => {
                        this.selectedHotelLists.push(itm);
                    });
                    this.onHotelSelect();
                    this.inquiryStatusList.forEach(itm => {
                        this.selectedStatuses.push(itm);
                    });
                    this.onStatusSelect();
                    this.loadReportData();*/
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }


    loadReportData() {
        let queryParams: any = new QueryParamsModel(
            '',
            this.sortOrderBy,
            this.sortField,
            1,
            10);

        queryParams = Object.assign(queryParams, this.customFilters);

        this.isLoading = true;
        this.reportService.bookingReport(queryParams)
            .subscribe(response => {
                    this.bookingReportData = response.items;
                    this.isLoading = false;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                    this.isLoading = false;
                });
    }

    exportReportData(type: string = 'pdf') {
        let queryParams: any = new QueryParamsModel(
            '',
            this.sortOrderBy,
            this.sortField,
            1,
            10);

        queryParams = Object.assign(queryParams, this.customFilters);
        queryParams.type = type;

        this.isLoading = true;
        this.reportService.bookingReportExport(queryParams)
            .subscribe(response => {
                    this.isLoading = false;
                    window.open(response.message, '_blank');
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    sourceChange() {
        const source = this.inquirySourceList.find(item => item.id === this.customFilters.source_id);
        this.isVisibleCompanies = false;
        this.isVisibleAgents = false;
        if (source) {
            if (source.name === 'Travel Agent') {
                this.isVisibleAgents = true;
                this.customFilters.company_id = '';
            } else if (source.name === 'Company') {
                this.isVisibleCompanies = true;
                this.customFilters.travel_agent_id = '';
            } else {
                this.customFilters.travel_agent_id = '';
                this.customFilters.company_id = '';
            }
        } else {
            this.customFilters.travel_agent_id = '';
            this.customFilters.company_id = '';
        }
    }

    selectedIQDate(value: any) {
        this.customFilters.inquiry_fromDate = value.start.format('YYYY-MM-DD');
        this.customFilters.inquiry_toDate = value.end.format('YYYY-MM-DD');
    }

    clearIQDate() {
        this.customFilters.inquiry_date_filter = false;
    }

    selectedCheckInDate(value: any) {
        this.customFilters.check_in_date_fromDate = value.start.format('YYYY-MM-DD');
        this.customFilters.check_in_date_toDate = value.end.format('YYYY-MM-DD');
    }

    clearCheckInDate() {
        this.customFilters.check_in_date_filter = false;
    }

    selectedCheckOutDate(value: any) {
        this.customFilters.check_out_date_fromDate = value.start.format('YYYY-MM-DD');
        this.customFilters.check_out_date_toDate = value.end.format('YYYY-MM-DD');
    }

    clearCheckOutDate() {
        this.customFilters.check_out_date_filter = false;
    }

    getNextRoomDetails(record) {
        let plans;
        plans = [];
        record.booking_rooms.forEach((rm, index) => {
            if (index > 0) {
                plans.push(Object.assign({}, rm));
            }
        });

        return plans;
    }

    applySorting(field) {
        if (this.sortField === field) {
            if (this.sortOrderBy === 'asc') {
                this.sortOrderBy = 'desc';
            } else {
                this.sortOrderBy = 'asc';
            }
        } else {
            this.sortField = field;
            this.sortOrderBy = 'asc';
        }
        this.loadReportData();
    }

    onHotelSelect(item: any = {}) {
        this.customFilters.hotel_id = [];
        if (this.selectedHotelLists.length > 0) {
            this.selectedHotelLists.forEach(itm => {
                this.customFilters.hotel_id.push(itm.id);
            });
        }
    }

    onStatusSelect(item: any = {}) {
        this.customFilters.status_id = [];
        if (this.selectedStatuses.length > 0) {
            this.selectedStatuses.forEach(itm => {
                this.customFilters.status_id.push(itm.id);
            });
        }
    }

    onSourceSelectionEvent(item: any = {}) {
        this.customFilters.source_id = [];
        if (this.selectedSourcesLists.length > 0) {
            this.selectedSourcesLists.forEach(itm => {
                this.customFilters.source_id.push(itm.id);
            });
        }
        this.checkCompanySourceSelected();
    }

    onCompanySelectEvent(item: any = {}) {
        this.customFilters.company_id = [];
        if (this.selectedCompanies.length > 0) {
            this.selectedCompanies.forEach(itm => {
                this.customFilters.company_id.push(itm.id);
            });
        }
    }

    checkCompanySourceSelected() {
        const company = this.inquirySourceList.find(item => item.name === 'Company');
        const ota = this.inquirySourceList.find(item => item.name === 'OTA');
        const travelAgent = this.inquirySourceList.find(item => item.name === 'Travel Agent');

        const compInd = this.customFilters.source_id.indexOf(company.id);
        const otaInd = this.customFilters.source_id.indexOf(ota.id);
        const taInd = this.customFilters.source_id.indexOf(travelAgent.id);

        this.searchCompanyTypes = [];
        if (compInd >= 0) {
            this.searchCompanyTypes.push(0);
        }

        if (taInd >= 0) {
            this.searchCompanyTypes.push(1);
        }

        if (otaInd >= 0) {
            this.searchCompanyTypes.push(2);
        }
    }

    onSearchCompany(evt: any) {
        console.log(evt.target.value);
        this.companyLists = [];
        const companySearchParams = {
            pageSize: '10',
            q: evt.target.value,
            type: this.searchCompanyTypes.join(','),
            _sort: 'name'
        };

        this.http.get(environment.API_URL + '/companies/search', {params: companySearchParams})
            .subscribe(response => {
                console.log(response);
                this.companyLists = response;
            }, error => {
            });
    }

    onInquiryTypeSelect(item: any = {}) {
        this.customFilters.type_id = [];
        if (this.selectedInquiryTypes.length > 0) {
            this.selectedInquiryTypes.forEach(itm => {
                this.customFilters.type_id.push(itm.id);
            });
        }
    }
}

