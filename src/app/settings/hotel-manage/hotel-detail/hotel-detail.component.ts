import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {SettingsService} from '../../../services/settings.service';
import {ToastrService} from 'ngx-toastr';
import {UtilsService} from '../../../core/services/utils.service';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {HotelService} from '../../../services/hotel.service';
import {LayoutUtilsService} from '../../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';

@Component({
    selector: 'm-hotel-detail',
    templateUrl: './hotel-detail.component.html',
    styleUrls: ['./hotel-detail.component.scss'],
    providers: [SettingsService, UtilsService, HotelService]
})
export class HotelDetailComponent implements OnInit, AfterViewInit {
    selectedTabIndex: number = 0;
    isSaving: boolean = false;
    currentRecord: any = {};
    currentBank: any = {};
    isLoadingBanks: boolean = false;
    isSavingBank: boolean = false;
    hotelBanks: any[] = [];
    organizations: any[] = [];
    config: any = {
        selector: 'textarea',
        height: 350,
        theme: 'modern',
        plugins: 'print preview fullpage powerpaste searchreplace' +
            ' autolink directionality advcode visualblocks visualchars fullscreen ' +
            'image link media codesample table charmap hr pagebreak nonbreaking anchor toc ' +
            'insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker ' +
            'imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor |' +
            ' link | alignleft aligncenter alignright alignjustify ' +
            ' | numlist bullist outdent indent  | removeformat',
        image_advtab: true,
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    };
    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '15rem',
        minHeight: '5rem',
        placeholder: 'Enter text here...',
        translate: 'no',
        customClasses: [
            {
                name: 'quote',
                class: 'quote',
            },
            {
                name: 'redText',
                class: 'redText'
            },
            {
                name: 'titleText',
                class: 'titleText',
                tag: 'h1',
            },
        ]
    };

    @ViewChild('policyTab') public test;

    constructor(private dialogRef: MatDialogRef<HotelDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private toastService: ToastrService,
                private utilsService: UtilsService,
                private hotelService: HotelService,
                private layoutUtilsService: LayoutUtilsService,
                private  settingsService: SettingsService) {
        // this.config = this.utilsService.getTinyConfig({}, 'basic');
        if (this.data) {
            if (this.data.record.id) {
                this.currentRecord = this.data.record;
                this.getBanks();
            }
        }


    }

    ngOnInit() {
        this.loadOrganizations();


    }

    ngAfterViewInit() {
        /*this.test.select('tab-selectbyid1');
        this.test.select('tab-selectbyid3');
        this.test.select('tab-selectbyid2');
        this.test.select('tab-selectbyid1');*/
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveHotels(this.currentRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    loadOrganizations() {
        this.settingsService.getOrganizations()
            .subscribe(response => {
                    this.organizations = response.items;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    tabClick(event, tabId) {
        this.test.select('tab-selectbyid3');
        this.test.select('tab-selectbyid2');
        this.test.select('tab-selectbyid1');
        console.log(tabId);
    }

    resetBank() {
        this.currentBank = {};
    }

    getBanks() {
        this.isLoadingBanks = true;
        this.hotelService.getHotelBanks(this.currentRecord.id)
            .subscribe(response => {
                    this.hotelBanks = response;
                    this.isLoadingBanks = false;
                },
                error => {
                    this.isLoadingBanks = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    saveBank(bank) {
        this.isSavingBank = true;
        this.hotelService.saveHotelBank(this.currentRecord.id, bank)
            .subscribe(response => {
                    this.isSavingBank = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.resetBank();
                    this.getBanks();
                },
                error => {
                    this.isSavingBank = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    setDefaultBank(bank) {
        bank.is_default = true;
        this.saveBank(bank);
    }

    editBank(bank) {
        this.currentBank = Object.assign({}, bank);
    }

    deleteBank(bank: any) {
        const dialogRef = this.layoutUtilsService.deleteElement();
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                return;
            }
            bank.isDeleting = true;
            this.hotelService.deleteHotelBanks(bank.id)
                .subscribe(response => {
                        bank.isDeleting = false;
                        this.toastService.success('Deleted Successfully', 'Done');
                        this.getBanks();
                    },
                    error => {
                        bank.isDeleting = false;
                        this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    });
        });
    }

}
