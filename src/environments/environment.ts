// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    tinyMCEApiKey: 'k2d7iux7x982gboxoa998c0xmcpo88itvx6s1vo8r86yzpm4',
	Server_URL: 'https://development.mytihr.in/tihr-backend-laravel/server.php/',
    // API_URL: 'http://tihr.com/api/v1',
    API_URL: 'https://development.mytihr.in/tihr-backend-laravel/server.php/api/v1',
    Base_Url: 'https://development.mytihr.in/dist'
    // API_URL: 'https://mytihr.in/tihr-backend-laravel/server.php/api/v1',
    // Base_Url: 'https://mytihr.in/dist'
};
