import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class SalesTargetService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient) {
        this.API_URL = environment.API_URL;
    }

    save(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/sales-target', data);
    }

    get(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/sales-target', {params: params});
    }

    getMonthTarget(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/sales-target/month', {params: params});
    }
}

