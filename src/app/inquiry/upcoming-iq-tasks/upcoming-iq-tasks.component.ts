import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialogRef, MatOption} from '@angular/material';
import {InquiryService} from '../inquiry.service';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {TokenStorage} from '../../core/auth/token-storage.service';
import * as moment from 'moment';

@Component({
    selector: 'm-upcoming-iq-tasks',
    templateUrl: './upcoming-iq-tasks.component.html',
    styleUrls: ['./upcoming-iq-tasks.component.scss'],
    providers: [InquiryService, UserService, InquiryService]
})
export class UpcomingIqTasksComponent implements OnInit {
    data: any = {
        result: [],
        dates: []
    };
    users: any[] = [];
    inquiryStatuses: any[] = [];
    filters: any = {
        assign_to_id: [],
        status_id: [],
        from_date: moment().format('YYYY-MM-DD')
    };
    isLoading: boolean = false;
    baseUrl: any = environment.Base_Url;
    loggedInUser: any = {};
    isChanging: boolean = false;
    minInqDate: any = moment();
    @ViewChild('allSelected') private allSelected: MatOption;

    constructor(private dialogRef: MatDialogRef<UpcomingIqTasksComponent>,
                private toastService: ToastrService,
                private userService: UserService,
                private  route: ActivatedRoute,
                private tokenStorage: TokenStorage,
                private router: Router,
                private  inquiryService: InquiryService) {
        this.loggedInUser = this.tokenStorage.getUserDetail();

        if (this.loggedInUser.department_name !== 'Sales') {
            this.getUsers();
        } else {
            this.getInquiryStatus();
            // this.loadTasks();
        }
    }

    ngOnInit() {

    }


    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    loadTasks() {
        this.isLoading = true;
        this.inquiryService.getUpcomingWIPInquiries(this.filters)
            .subscribe(response => {
                    this.data = response;
                    this.isLoading = false;
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    getUsers() {
        this.userService.getUserList({status: 1})
            .subscribe(response => {
                    this.users = response;
                    this.users.forEach(usr => {
                        // if (this.loggedInUser.id === usr.id) {
                        this.filters.assign_to_id.push(usr.id);
                        // }
                    });

                    this.getInquiryStatus();
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    getInquiryStatus() {
        this.inquiryService.getInquiryStatusList()
            .subscribe(response => {
                    response.forEach(stat => {
                        if (stat.name === 'WIP' || stat.name === 'Hold' || stat.name === 'Waitlisted') {
                            this.inquiryStatuses.push(stat);
                            this.filters.status_id.push(stat.id);
                        }
                    });
                    this.changeFilter();
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    openDetail(inq) {
        this.router.navigate(
            ['/inquiries'],
            {
                queryParams: {iqId: inq.inquiry_id, r: Math.floor(Math.random() * ((9 - 1) + 1))},
                queryParamsHandling: 'merge',
            });
        this.closeDialog();
    }

    changeFilter() {
        this.loadTasks();
    }

    dateChangeEvent(type: string) {
        this.filters[type] = this.filters[type].format('YYYY-MM-DD');
    }
}
