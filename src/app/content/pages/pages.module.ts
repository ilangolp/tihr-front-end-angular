import {LayoutModule} from '../layout/layout.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PagesRoutingModule} from './pages-routing.module';
import {PagesComponent} from './pages.component';
import {PartialsModule} from '../partials/partials.module';
import {ActionComponent} from './header/action/action.component';
import {ProfileComponent} from './header/profile/profile.component';
import {MailModule} from './components/apps/mail/mail.module';
import {ECommerceModule} from './components/apps/e-commerce/e-commerce.module';
import {CoreModule} from '../../core/core.module';
import {AngularEditorModule} from '@kolkov/angular-editor';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ErrorPageComponent} from './snippets/error-page/error-page.component';
import {InquiryComponent} from '../../inquiry/inquiry.component';
import {
	MatAutocompleteModule,
	MatButtonModule,
	MatButtonToggleModule,
	MatCardModule,
	MatCheckboxModule,
	MatDatepickerModule,
	MatDividerModule,
	MatFormFieldModule,
	MatIconModule,
	MatInputModule,
	MatListModule, MatMenuModule,
	MatOptionModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatRadioModule,
	MatSelectModule,
	MatSliderModule,
	MatSlideToggleModule,
	MatSortModule,
	MatStepperModule,
	MatTableModule,
	MatTabsModule,
	MatTooltipModule
} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {SpinnerButtonModule} from '../partials/content/general/spinner-button/spinner-button.module';
import {AddInquiryComponent} from '../../inquiry/add-inquiry/add-inquiry.component';
import {JwtInterceptor} from '../../_helpers/jwt.interceptor';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {ToastrModule} from 'ngx-toastr';
import {NgbAlertModule, NgbCollapseModule, NgbPaginationModule, NgbPopoverModule, NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import {SettingsComponent} from '../../settings/settings.component';
import {BasicSettingsComponent} from '../../settings/basic-settings/basic-settings.component';
import {CompaniesComponent} from '../../companies/companies.component';
import {CompanyDetailComponent} from '../../companies/company-detail/company-detail.component';
import {SearchSelectModule} from '@oasisdigital/angular-material-search-select';
import {DsrComponent} from '../../dsr/dsr.component';
import {DsrDetailComponent} from '../../dsr/dsr-detail/dsr-detail.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {UsersManagementComponent} from '../../settings/users-management/users-management.component';
import {UserDetailComponent} from '../../settings/users-management/user-detail/user-detail.component';
import {SalesTargetComponent} from '../../sales-target/sales-target.component';
import {SalesTargetDetailComponent} from '../../sales-target/sales-target-detail/sales-target-detail.component';
import {InqPaymentConfirmComponent} from '../../inquiry/inq-payment-confirm/inq-payment-confirm.component';
import {AngularFileUploaderModule} from 'angular-file-uploader';
import {SearchPipe} from '../../shared/search.pipe';
import {BookingOutstandingReportComponent} from '../../reports/booking-outstanding-report/booking-outstanding-report.component';
import {Daterangepicker} from 'ng2-daterangepicker';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {RoomTypesComponent} from '../../settings/basic-settings/room-types/room-types.component';
import {RoomTypeDetailComponent} from '../../settings/basic-settings/room-types/room-type-detail/room-type-detail.component';
import {MccColorPickerModule} from 'material-community-components';
import {MealPlansComponent} from '../../settings/basic-settings/meal-plans/meal-plans.component';
import {AmenitiesComponent} from '../../settings/basic-settings/amenities/amenities.component';
import {AmenityTypesComponent} from '../../settings/basic-settings/amenity-types/amenity-types.component';
import {BedTypesComponent} from '../../settings/basic-settings/bed-types/bed-types.component';
import {AmenityDetailComponent} from '../../settings/basic-settings/amenities/amenity-detail/amenity-detail.component';
import {HolidaysComponent} from '../../settings/basic-settings/holidays/holidays.component';
import {DiscountsComponent} from '../../settings/basic-settings/discounts/discounts.component';
import {HolidayDetailComponent} from '../../settings/basic-settings/holidays/holiday-detail/holiday-detail.component';
import {TaxesComponent} from '../../settings/basic-settings/taxes/taxes.component';
import {TaxDetailComponent} from '../../settings/basic-settings/taxes/tax-detail/tax-detail.component';
import {ExtrasComponent} from '../../settings/basic-settings/extras/extras.component';
import {ExtraDetailComponent} from '../../settings/basic-settings/extras/extra-detail/extra-detail.component';
import {HotelManageComponent} from '../../settings/hotel-manage/hotel-manage.component';
import {HotelDetailComponent} from '../../settings/hotel-manage/hotel-detail/hotel-detail.component';
import {PropertyMastersComponent} from '../../settings/property-masters/property-masters.component';
import {HotelRoomsComponent} from '../../settings/property-masters/hotel-rooms/hotel-rooms.component';
import {RoomDetailComponent} from '../../settings/property-masters/hotel-rooms/room-detail/room-detail.component';
import {CopyRoomComponent} from '../../settings/property-masters/hotel-rooms/copy-room/copy-room.component';
import {RatePlanTypesComponent} from '../../settings/basic-settings/rate-plans/rate-plan-types.component';
import {RatePlansComponent} from '../../settings/rate-plans/rate-plans.component';
import {RatePlanDetailComponent} from '../../settings/rate-plans/rate-plan-detail/rate-plan-detail.component';
import {EmailTemplatesComponent} from '../../settings/email-templates/email-templates.component';
import {TemplateDetailComponent} from '../../settings/email-templates/template-detail/template-detail.component';
import {OrganizationDetailComponent} from '../../settings/organizations/organization-detail/organization-detail.component';
import {OrganizationsComponent} from '../../settings/organizations/organizations.component';
import {ApplicationSettingsComponent} from '../../settings/application-settings/application-settings.component';
import {CKEditorModule} from 'ngx-ckeditor';
import {EditorModule} from '@tinymce/tinymce-angular';
import {RateInventoryComponent} from '../../settings/rate-inventory/rate-inventory.component';
import {RateBulkUpdateComponent} from '../../settings/rate-inventory/rate-bulk-update/rate-bulk-update.component';
import {RateInventorySourcesComponent} from '../../settings/basic-settings/rate-inventory-sources/rate-inventory-sources.component';
import {BookingComponent} from '../../inquiry/booking/booking.component';
import {NgxSummernoteModule} from 'ngx-summernote';
import {RoleManagementComponent} from '../../settings/role-management/role-management.component';
import {RoleDetailComponent} from '../../settings/role-management/role-detail/role-detail.component';
import {HistoryComponent} from '../../utility/history/history.component';
import {NgxPermissionsModule} from 'ngx-permissions';
import {BookingReportComponent} from '../../reports/booking-report/booking-report.component';
import {LeadComponent} from '../../inquiry/lead/lead.component';
import {LeadDetailComponent} from '../../inquiry/lead/lead-detail/lead-detail.component';
import {CMSettingsComponent} from '../../settings/cm-settings/cm-settings.component';
import {UpdateWizardComponent} from '../../settings/rate-inventory/update-wizard/update-wizard.component';
import {RateInventoryViewComponent} from '../../settings/rate-inventory/rate-inventory-view/rate-inventory-view.component';
import {CMLogsComponent} from '../../settings/cm-logs/cm-logs.component';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {WelcomeComponent} from '../../welcome/welcome.component';
import {TemplateSettingsComponent} from '../../settings/email-templates/template-settings/template-settings.component';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {InventoryReportComponent} from '../../reports/inventory-report/inventory-report.component';
import {SourcesComponent} from '../../settings/basic-settings/sources/sources.component';
import {IndustriesComponent} from '../../settings/basic-settings/industries/industries.component';
import {CategoriesComponent} from '../../settings/basic-settings/categories/categories.component';
import {InquiryTypesComponent} from '../../settings/basic-settings/inquiry-types/inquiry-types.component';
import {InquiryStatusesComponent} from '../../settings/basic-settings/inquiry-statuses/inquiry-statuses.component';
import {InquiryStatusDetailComponent} from '../../settings/basic-settings/inquiry-statuses/inquiry-status-detail/inquiry-status-detail.component';
import {EmailLogComponent} from '../../settings/email-logs/email-log.component';
import {LogDetailComponent} from '../../settings/email-logs/log-detail/log-detail.component';
import {WaitlistedInquiriesComponent} from '../../inquiry/waitlisted-inquiries/waitlisted-inquiries.component';
import {BookingStatusReportComponent} from '../../reports/booking-status-report/booking-status-report.component';
import {PreviewEmailComponent} from '../../inquiry/booking/preview-email/preview-email.component';
import {DynamicDashboardsComponent} from '../../settings/basic-settings/dynamic-dashboards/dynamic-dashboards.component';
import {BookingDiscountReportComponent} from '../../reports/booking-discount-report/booking-discount-report.component';
import {InquiryPaymentStatusesComponent} from '../../settings/basic-settings/inquiry-payment-statuses/inquiry-payment-statuses.component';
import {InquiryPaymentStatusDetailComponent} from '../../settings/basic-settings/inquiry-payment-statuses/inquiry-payment-status-detail/inquiry-payment-status-detail.component';
import {InqDiscountApproveComponent} from '../../inquiry/inq-discount-approve/inq-discount-approve.component';
import {PaymentModesComponent} from '../../settings/basic-settings/payment-modes/payment-modes.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {UnitExpensesComponent} from '../../settings/property-masters/unit-expenses/unit-expenses.component';
import {TimeagoModule} from 'ngx-timeago';


export const MY_FORMATS = {
    parse: {
        dateInput: 'DD-MM-YYYY',
    },
    display: {
        dateInput: 'DD-MM-YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'YYYY-MM-DD',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

@NgModule({
    declarations: [
        PagesComponent,
        ActionComponent,
        ProfileComponent,
        ErrorPageComponent,
        InquiryComponent,
        AddInquiryComponent,
        SettingsComponent,
        BasicSettingsComponent,
        CompaniesComponent,
        DsrComponent,
        DsrDetailComponent,
        CompanyDetailComponent,
        UsersManagementComponent,
        UserDetailComponent,
        SalesTargetComponent,
        SalesTargetDetailComponent,
        InqPaymentConfirmComponent,
        InqDiscountApproveComponent,
        SearchPipe,
        BookingDiscountReportComponent,
        DynamicDashboardsComponent,
        IndustriesComponent,
        CategoriesComponent,
        SourcesComponent,
        InquiryTypesComponent,
        InquiryStatusesComponent,
        InquiryStatusDetailComponent,
        RoomTypesComponent,
        RoomTypeDetailComponent,
        BedTypesComponent,
        MealPlansComponent,
        AmenitiesComponent,
        AmenityDetailComponent,
        AmenityTypesComponent,
        HolidaysComponent,
        DiscountsComponent,
        HolidayDetailComponent,
        TaxesComponent,
        TaxDetailComponent,
        ExtrasComponent,
        ExtraDetailComponent,
        HotelManageComponent,
        HotelDetailComponent,
        PropertyMastersComponent,
        HotelRoomsComponent,
        RoomDetailComponent,
        CopyRoomComponent,
        RatePlanTypesComponent,
        RatePlansComponent,
        RatePlanDetailComponent,
        EmailTemplatesComponent,
        TemplateDetailComponent,
        EmailLogComponent,
        LogDetailComponent,
        OrganizationsComponent,
        OrganizationDetailComponent,
        ApplicationSettingsComponent,
        RateInventoryComponent,
        RateBulkUpdateComponent,
        RateInventorySourcesComponent,
        BookingComponent,
        RoleManagementComponent,
        RoleDetailComponent,
        HistoryComponent,
        BookingReportComponent,
        LeadComponent,
        LeadDetailComponent,
        CMSettingsComponent,
        UpdateWizardComponent,
        RateInventoryViewComponent,
        CMLogsComponent,
        WelcomeComponent,
        TemplateSettingsComponent,
        InventoryReportComponent,
        WaitlistedInquiriesComponent,
        BookingStatusReportComponent,
        PreviewEmailComponent,
        BookingOutstandingReportComponent,
        InquiryPaymentStatusesComponent,
        InquiryPaymentStatusDetailComponent,
        PaymentModesComponent,
		UnitExpensesComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        SearchSelectModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        ReactiveFormsModule,
        MatSlideToggleModule,
        MatAutocompleteModule,
        MatTabsModule,
        MatCardModule,
        MatDividerModule,
        MatPaginatorModule,
        MatTooltipModule,
        MatIconModule,
        MatTableModule,
        MatSortModule,
        MatSelectModule,
        MatButtonModule,
        MatListModule,
        MatInputModule,
        MatOptionModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatCheckboxModule,
        MatDatepickerModule,
        TranslateModule.forChild(),
        SpinnerButtonModule,
        HttpClientModule,
        PagesRoutingModule,
        CoreModule,
        LayoutModule,
        PartialsModule,
        MailModule,
        ECommerceModule,
        NgbPopoverModule,
        NgbCollapseModule,
        AngularEditorModule,
        MatFormFieldModule,
        MatSliderModule,
        MatButtonToggleModule,
        NgxMaterialTimepickerModule.forRoot(),
        ToastrModule.forRoot({
            timeOut: 10000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
        }),
        AngularFileUploaderModule,
        Daterangepicker,
        PerfectScrollbarModule,
        MccColorPickerModule.forRoot({
            empty_color: 'transparent',
        }),
        NgbAlertModule,
        NgbPaginationModule,
        CKEditorModule,
		MatExpansionModule,
        EditorModule,
        NgbTabsetModule,
        NgxSummernoteModule,
        NgxPermissionsModule.forChild(),
        MatStepperModule,
        AngularMultiSelectModule,
        NgMultiSelectDropDownModule.forRoot(),
        MccColorPickerModule.forRoot({
            empty_color: 'transparent',
            used_colors: ['#000000', '#FFF555']
        }),
		MatMenuModule,
		TimeagoModule.forRoot()
    ],
    entryComponents: [
        AddInquiryComponent,
        CompanyDetailComponent,
        DsrDetailComponent,
        UserDetailComponent,
        SalesTargetDetailComponent,
        InqPaymentConfirmComponent,
        InqDiscountApproveComponent,
        RateBulkUpdateComponent,
        LeadDetailComponent,
        UpdateWizardComponent,


        // Global Masters
        DynamicDashboardsComponent,
        IndustriesComponent,
        CategoriesComponent,
        SourcesComponent,
        InquiryTypesComponent,
        InquiryStatusesComponent,
        InquiryStatusDetailComponent,
        RoomTypesComponent,
        RoomTypeDetailComponent,
        BedTypesComponent,
        MealPlansComponent,
        AmenitiesComponent,
        AmenityTypesComponent,
        AmenityDetailComponent,
        HolidaysComponent,
        HolidayDetailComponent,
        DiscountsComponent,
        TaxesComponent,
        TaxDetailComponent,
        ExtrasComponent,
        ExtraDetailComponent,
        HotelDetailComponent,
        RatePlanTypesComponent,
        TemplateDetailComponent,
        TemplateSettingsComponent,
        LogDetailComponent,
        OrganizationDetailComponent,
        ApplicationSettingsComponent,
        RateInventorySourcesComponent,
        WaitlistedInquiriesComponent,
        PreviewEmailComponent,
        InquiryPaymentStatusesComponent,
        InquiryPaymentStatusDetailComponent,
        PaymentModesComponent,
		UnitExpensesComponent,

        // Property Masters
        RoomDetailComponent,
        CopyRoomComponent,
        RatePlanDetailComponent,
        RoleDetailComponent
    ],
    providers: [{
        provide: HTTP_INTERCEPTORS,
        useClass: JwtInterceptor,
        multi: true
    },
        {provide: DateAdapter, useClass: MomentDateAdapter},
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
    ]
})
export class PagesModule {
    //
}
