import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class SettingsService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient) {
        this.API_URL = environment.API_URL;
    }

    dynamicDashboardsList(): Observable<any> {
        return this.http.get(this.API_URL + '/dashboard/dynamic-dashboards-list');
    }

    dynamicDashboardsDetails(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/dashboard/dynamic-dashboards-details', {params: data});
    }

    getDynamicDashboards(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/dashboard/dynamic-dashboards', {params: data});
    }

    saveDynamicDashboards(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/dashboard/dynamic-dashboards', data);
    }

    deleteDynamicDashboards(id): Observable<any> {
        return this.http.delete(this.API_URL + '/dashboard/dynamic-dashboards/' + id);
    }

    getIndustries(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/industries', {params: data});
    }

    saveIndustries(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/industries', data);
    }

    deleteIndustries(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/industries/' + id);
    }

    getCategories(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/categories', {params: data});
    }

    saveCategories(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/categories', data);
    }

    deleteCategories(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/categories/' + id);
    }

	getExpenseList(data: any = {}): Observable<any> {
		return this.http.get(this.API_URL + '/settings/expenses', {params: data});
	}

	saveExpense(data: any = {}): Observable<any> {
		return this.http.post(this.API_URL + '/settings/expenses', data);
	}

	deleteExpense(id): Observable<any> {
		return this.http.delete(this.API_URL + '/settings/expenses/' + id);
	}

    getSources(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/sources', {params: data});
    }

    saveSource(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/sources', data);
    }

    deleteSource(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/sources/' + id);
    }

    getInquiryTypes(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/inquiry-types', {params: data});
    }

    saveInquiryTypes(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/inquiry-types', data);
    }

    deleteInquiryTypes(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/inquiry-types/' + id);
    }

    getInquiryStatuses(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/inquiry-statuses', {params: data});
    }

    /* saveInquiryStatuses(data): Observable<any> {
        return this.http.post(this.API_URL + '/settings/inquiry-statuses', data);
    } */

    saveInquiryStatuses(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/inquiry-statuses', data);
    }

    deleteInquiryStatuses(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/inquiry-statuses/' + id);
    }

    getInquiryPaymentStatuses(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/inquiry-payment-statuses', {params: data});
    }

    saveInquiryPaymentStatus(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/inquiry-payment-statuses', data);
    }

    deleteInquiryPaymentStatus(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/inquiry-payment-statuses/' + id);
    }

    /*saveHotelBank(inquiryStatusesId, params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/hotels/' + inquiryStatusesId + '/banks', params);
    }

    getHotelBanks(inquiryStatusesId): Observable<any> {
        return this.http.get(this.API_URL + '/hotels/' + inquiryStatusesId + '/banks');
    }

    deleteHotelBanks(bankId): Observable<any> {
        return this.http.delete(this.API_URL + '/hotels/' + bankId + '/banks');
    }*/

    getRoomTypeList(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/room-types/list', {params: data});
    }

    getRoomTypes(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/room-types', {params: data});
    }

    saveRoomType(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/room-types', data);
    }

    deleteRoomType(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/room-types/' + id);
    }


    getBedTypes(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/bed-types', {params: data});
    }

    saveBedType(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/bed-types', data);
    }

    deleteBedType(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/bed-types/' + id);
    }


    getAmenityTypes(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/amenity-types', {params: data});
    }

    saveAmenityType(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/amenity-types', data);
    }

    deleteAmenityType(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/amenity-types/' + id);
    }


    getAmenities(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/amenities', {params: data});
    }

    saveAmenities(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/amenities', data);
    }

    deleteAmenities(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/amenities/' + id);
    }


    getMealPlans(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/meal-plans', {params: data});
    }

    saveMealPlans(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/meal-plans', data);
    }

    deleteMealPlans(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/meal-plans/' + id);
    }

    getHolidays(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/holidays', {params: data});
    }

    saveHolidays(data): Observable<any> {
        return this.http.post(this.API_URL + '/settings/holidays', data);
    }

    deleteHolidays(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/holidays/' + id);
    }

    getDiscounts(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/discounts', {params: data});
    }

    saveDiscounts(data): Observable<any> {
        return this.http.post(this.API_URL + '/settings/discounts', data);
    }

    deleteDiscounts(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/discounts/' + id);
    }

    getTaxes(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/taxes', {params: data});
    }

    saveTaxes(data): Observable<any> {
        return this.http.post(this.API_URL + '/settings/taxes', data);
    }

    deleteTaxes(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/taxes/' + id);
    }

    getExtras(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/extras', {params: data});
    }

    saveExtras(data): Observable<any> {
        return this.http.post(this.API_URL + '/settings/extras', data);
    }

    deleteExtras(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/extras/' + id);
    }

    getPaymentModes(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/payment-modes', {params: data});
    }

    savePaymentModes(data): Observable<any> {
        return this.http.post(this.API_URL + '/settings/payment-modes', data);
    }

    deletePaymentModes(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/payment-modes/' + id);
    }

    getHotels(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/hotels', {params: params});
    }

    saveHotels(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/hotels', params);
    }

    getHotelRoomSettings(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/hotels/rooms-settings', {params: params});
    }

    getHotelRooms(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/hotels/rooms-list', params);
    }

    saveHotelRooms(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/hotels/rooms', params);
    }

    deleteRooms(id): Observable<any> {
        return this.http.delete(this.API_URL + '/hotels/rooms/' + id);
    }

    copyHotelRooms(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/hotels/copy-rooms', params);
    }

    getRateTypes(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/rate-plan-types', {params: data});
    }

    saveRateTypes(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/rate-plan-types', data);
    }

    deleteRateTypes(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/rate-plan-types/' + id);
    }

    getRatePlans(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/rate-plans', {params: data});
    }

    getRatePlanList(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/rate-plan-lists', {params: data});
    }

    saveRatePlans(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/rate-plans', data);
    }

    deleteRatePlans(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/rate-plans/' + id);
    }

    findRatePlan(id): Observable<any> {
        return this.http.get(this.API_URL + '/settings/rate-plans/' + id);
    }

    getOrganizations(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/organizations', {params: params});
    }

    saveOrganization(data: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/organizations', data);
    }

    getEmailTemplates(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/email-templates', {params: params});
    }

    saveEmailTemplates(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/email-templates', params);
    }

    getEmailAssignments(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/email-templates-setting', {params: params});
    }

    saveEmailAssignments(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/email-templates-setting', params);
    }

    deleteEmailTemplates(id): Observable<any> {
        return this.http.delete(this.API_URL + '/settings/email-templates/' + id);
    }

    getEmailLogs(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/email-log', {params: params});
    }

    getModuleFields(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/module-fields', {params: params});
    }

    getApplicationSettings(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/settings/util-settings', {params: data});
    }

    saveApplicationSettings(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/util-settings', params);
    }

    getAppConfigs(data: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/app-settings', {params: data});
    }

    getCMLogs(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/settings/cm-logs', params);
    }

    getCMLogDetails(id): Observable<any> {
        return this.http.get(this.API_URL + '/settings/cm-logs/' + id);
    }

}

