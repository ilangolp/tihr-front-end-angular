import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CommonService} from '../../services/common.service';
import {ToastrService} from 'ngx-toastr';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {RoleDetailComponent} from './role-detail/role-detail.component';
import {RoleService} from '../../services/role.service';

@Component({
    selector: 'm-role-management',
    templateUrl: './role-management.component.html',
    styleUrls: ['./role-management.component.scss'],
    providers: [CommonService,RoleService]
})
export class RoleManagementComponent implements OnInit {
    roles: any[] = [];
    isLoading: boolean = false;
    numberOfTicks: number = 0;
    dataSource = new MatTableDataSource(this.roles);
    displayedColumns: any = [
        'name',
        'users_count'
    ];

    constructor(private toastService: ToastrService,
                public dialog: MatDialog,
                private ref: ChangeDetectorRef,
                private  roleService: RoleService,
                private commonService: CommonService) {
        setInterval(() => {
            this.numberOfTicks++;
            this.ref.markForCheck();
        }, 1000);
    }

    ngOnInit() {
        this.getRoles();
    }

    getRoles() {
        this.isLoading = true;
        this.roleService.getRoles()
            .subscribe(response => {
                    this.isLoading = false;
                    this.roles = response;
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    openRole(currentRecord: any = {}, params: any = {}) {
        const dialogRef = this.dialog.open(RoleDetailComponent
            , {
                data: {currentRecord: Object.assign({}, currentRecord)}
            });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                //
            }
        });
    }
}
