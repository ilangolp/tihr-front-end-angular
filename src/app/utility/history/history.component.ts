import {Component, Inject, Input, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CommonService} from '../../services/common.service';
import {ToastrService} from 'ngx-toastr';
import {TokenStorage} from '../../core/auth/token-storage.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'm-history',
    templateUrl: './history.component.html',
    styleUrls: ['./history.component.scss'],
    providers: [CommonService]
})
export class HistoryComponent implements OnInit {
    @Input() reference: any;
    @Input() id: any;
    histories: any[] = [];
    loggedInUser: any = {};
    isLoading: boolean = false;

    constructor(private dialogRef: MatDialogRef<HistoryComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private modalService: NgbModal,
                private toastService: ToastrService,
                private tokenStorage: TokenStorage,
                private commonService: CommonService) {
        this.loggedInUser = this.tokenStorage.getUserDetail();

        /*if (this.data) {
            if (this.data.referenceId) {
                this.referenceId = this.data.referenceId;
            }
        }*/
    }

    ngOnInit() {
        console.log(this.reference);
        console.log(this.id);
    }

    openHistory(content) {
        this.loadHistory();
        this.modalService.open(content, {centered: true});
    }

    loadHistory() {
        this.isLoading = true;
        this.commonService.fetchHistory({reference: this.reference, id: this.id})
            .subscribe(response => {
                    this.histories = response;
                    this.isLoading = false;
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }
}
