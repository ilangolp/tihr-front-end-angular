import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

@Injectable()
export class TokenStorage {
    /**
     * Get access token
     * @returns {Observable<string>}
     */
    public getAccessToken(): Observable<string> {
        const token: string = <string>localStorage.getItem('accessToken');
        return of(token);
    }

    /**
     * Get refresh token
     * @returns {Observable<string>}
     */
    public getRefreshToken(): Observable<string> {
        const token: string = <string>localStorage.getItem('refreshToken');
        return of(token);
    }

    /**
     * Get user roles in JSON string
     * @returns {Observable<any>}
     */
    public getUserRoles(): Observable<any> {
        const roles: any = localStorage.getItem('userRoles');
        try {
            return of(JSON.parse(roles));
        } catch (e) {
        }
    }

    /**
     * Get role Permissions in JSON string
     * @returns {Observable<any>}
     */
    public getRolePermissions(): Observable<any> {
        const perms: any = localStorage.getItem('rolePermissions');
        try {
            const permissions: any = of(JSON.parse(perms));
            return (permissions.value) ? permissions.value : [];
        } catch (e) {
        }
    }

    /**
     * Set access token
     * @returns {TokenStorage}
     */
    public setAccessToken(token: string): TokenStorage {
        localStorage.setItem('accessToken', token);

        return this;
    }

    /**
     * Set refresh token
     * @returns {TokenStorage}
     */
    public setRefreshToken(token: string): TokenStorage {
        localStorage.setItem('refreshToken', token);

        return this;
    }

    /**
     * Set user roles
     * @param roles
     * @param permissions
     * @returns {TokenStorage}
     */
    public setUserRoles(roles: any, permissions: any = []): any {
        if (roles != null) {
            // roles.push('USER');
            localStorage.setItem('userRoles', JSON.stringify(roles));
            localStorage.setItem('rolePermissions', JSON.stringify(permissions));
        }

        return this;
    }

    /**
     * Get user detail
     * @returns {Observable<any>}
     */
    public getUserDetail(): Observable<any> {
        const user: any = localStorage.getItem('user');
        try {
            return (JSON.parse(user));
        } catch (e) {
        }
    }


    /**
     * Set user Detail
     * @param user
     * @returns {TokenStorage}
     */
    public setUserDetail(user: any): any {
        if (user != null) {
            localStorage.setItem('user', JSON.stringify(user));
        }

        return this;
    }


    /**
     * Get user detail
     * @returns {Observable<any>}
     */
    public getAppConfig(): Observable<any> {
        const appConfig: any = localStorage.getItem('appConfig');
        try {
            return (JSON.parse(appConfig));
        } catch (e) {
        }
    }

    /**
     * Get user detail
     * @returns {Observable<any>}
     */
    public getAppConfigByKey(key): Observable<any> {
        const appConfig: any = localStorage.getItem('appConfig');
        try {
            const config = (JSON.parse(appConfig));
            return (config[key]) ? config[key] : '';
        } catch (e) {
        }
    }


    /**
     * Set user Detail
     * @param user
     * @returns {TokenStorage}
     */
    public setAppConfig(config: any): any {
        if (config != null) {
            localStorage.setItem('appConfig', JSON.stringify(config));
        }

        return this;
    }

    /**
     * Remove tokens
     */
    public clear() {
        localStorage.removeItem('accessToken');
        localStorage.removeItem('refreshToken');
        localStorage.removeItem('userRoles');
        localStorage.removeItem('rolePermissions');
        localStorage.removeItem('user');
        localStorage.removeItem('lastOpenSetting');
    }

    public setUserAndPermissions(user) {
        if (user['id']) {
            this.setUserDetail(user);
            const role = user.department_name;
            let permsArray: any = [];
            permsArray = user.permissions;
            permsArray.push('Authenticated');
            let permissions: any;
            permissions = {};
            permissions[role] = permsArray;
            this.setUserRoles([role], permissions);
        }
    }
}
