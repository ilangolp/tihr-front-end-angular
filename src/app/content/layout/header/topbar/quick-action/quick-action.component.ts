import {ChangeDetectionStrategy, Component, HostBinding, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {UpcomingIqTasksComponent} from '../../../../../inquiry/upcoming-iq-tasks/upcoming-iq-tasks.component';

@Component({
    selector: 'm-quick-action',
    templateUrl: './quick-action.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuickActionComponent implements OnInit {
    @HostBinding('class')
        // tslint:disable-next-line:max-line-length
    classes = 'm-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light';

    @HostBinding('attr.m-dropdown-toggle') attrDropdownToggle = 'click';

    constructor(public dialog: MatDialog) {
    }

    ngOnInit(): void {
    }

    openTasks() {
        const dialogRef = this.dialog.open(UpcomingIqTasksComponent, {
            width: '1000px',
            data: {}
        });
    }

    setCurrentMenu(menu) {
        localStorage.setItem('lastOpenSetting', menu);
    }
}
