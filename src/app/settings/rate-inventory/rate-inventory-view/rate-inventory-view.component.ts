import {Component, OnInit} from '@angular/core';
import * as moment from 'moment';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MatDialog} from '@angular/material';
import {RateInventoryService} from '../../../services/rateInventory.service';
import {SettingsService} from '../../../services/settings.service';
import {UpdateWizardComponent} from '../update-wizard/update-wizard.component';
import {RateBulkUpdateComponent} from '../rate-bulk-update/rate-bulk-update.component';

@Component({
    selector: 'm-rate-inventory-view',
    templateUrl: './rate-inventory-view.component.html',
    styleUrls: ['./rate-inventory-view.component.scss'],
    providers: [RateInventoryService, SettingsService]
})
export class RateInventoryViewComponent implements OnInit {
    days: any = [];
    showDays = 10;
    inventoryData: any[] = [];
    inventorySources: any[] = [];
    hotels: any[] = [];
    isLoading: boolean = false;
    isLoadingSettings: boolean = false;
    isSaving: boolean = false;
    totalRoomsByDate: any = {};
    rateTypes: any[] = [
        {
            id: 'rate',
            name: 'Rates'
        },
        {
            id: 'child_rate',
            name: 'Child Rate'
        },
        {
            id: 'adult_rate',
            name: 'Adult Rate',
        },
        {
            id: 'min_nights',
            name: 'Min Nights'
        },
        {
            id: 'max_nights',
            name: 'MAx Nights'
        },
        {
            id: 'is_stop_Sell',
            name: 'Stop Sell'
        },
        {
            id: 'coa',
            name: 'COA'
        },
        {
            id: 'cod',
            name: 'COD'
        }
    ];
    roomTypes: any[] = [];
    filters: any = {
        rate_type: 'rate',
        source_id: null,
        hotel_id: null,
        room_type_id: '',
        fromDate: null,
        toDate: null
    };

    constructor(private toastService: ToastrService,
                private modalService: NgbModal,
                public dialog: MatDialog,
                public  settingsService: SettingsService,
                private rateInventoryService: RateInventoryService) {
    }

    ngOnInit() {
        this.loadSettings();
    }

    resetDates() {
        this.days = [];
        for (let i = 0; i < this.showDays; i++) {
            this.days.push(moment().add(i, 'days').format('YYYY-MM-DD'));
        }

        console.log(this.days);
        this.loadRateInventoryData();
    }


    setNextWeekDates() {
        const lastDate = this.days[this.days.length - 1];
        this.days = [];
        for (let i = 1; i <= this.showDays; i++) {
            this.days.push(moment(lastDate, 'YYYY-MM-DD').add(i, 'days').format('YYYY-MM-DD'));
        }
        this.loadRateInventoryData();
    }

    setPreviousWeekDates() {
        const lastDate = this.days[0];
        this.days = [];
        for (let i = this.showDays; i >= 1; i--) {
            this.days.push(moment(lastDate, 'YYYY-MM-DD').add(0 - i, 'days').format('YYYY-MM-DD'));
        }

        this.loadRateInventoryData();
    }

    isBellowToday() {
        let result: boolean = false;
        if (this.days.length) {
            if (moment(this.days[0], 'YYYY-MM-DD') <= moment()) {
                result = false;
            } else {
                result = true;
            }
        }
        return result;
    }

    loadSettings() {
        this.isLoadingSettings = true;
        this.rateInventoryService.getSettingList()
            .subscribe(response => {
                    this.roomTypes = response.roomTypes;
                    this.inventorySources = response.sources;
                    this.hotels = response.hotels;
                    // if (this.roomTypes) {
                    //     this.filters.room_type_id = this.roomTypes[0]['id'];
                    // }
                    if (this.inventorySources) {
                        this.filters.source_id = this.inventorySources[0]['id'];
                    }
                    if (this.hotels) {
                        this.filters.hotel_id = this.hotels[0]['id'];
                    }
                    this.isLoadingSettings = false;
                    this.loadRoomTypes();
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                    this.isLoadingSettings = false;
                });
    }

    loadRoomTypes() {
        this.isLoadingSettings = true;
        this.settingsService.getRoomTypeList({hotel_id: this.filters.hotel_id})
            .subscribe(response => {
                    this.roomTypes = response;
                    this.isLoadingSettings = false;

                    if (this.filters.room_type_id) {
                        const selRoomType = this.roomTypes.find(itm => {
                            return itm.id === this.filters.room_type_id;
                        });

                        if (!selRoomType) {
                            this.filters.room_type_id = '';
                        }
                    }

                    this.resetDates();
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                    this.isLoadingSettings = false;
                });
    }

    loadRateInventoryData() {
        this.isLoading = true;
        let params: any = {};
        params = Object.assign({}, this.filters);
        if (this.days.length > 0) {
            params.fromDate = this.days[0];
            params.toDate = this.days[this.days.length - 1];
        }
        this.inventoryData = [];
        this.rateInventoryService.getRateInventoryData(params)
            .subscribe(response => {
                    this.isLoading = false;
                    this.inventoryData = response;
                    this.totalRoomsByDate = {};
                    this.days.forEach(dt => {
                        this.totalRoomsByDate[dt] = 0;
                        this.inventoryData.forEach(roomData => {
                            this.totalRoomsByDate[dt] += roomData['qty'][dt];
                        });
                    });


                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    saveRateInventory() {
        let reqData: any;
        reqData = [];
        this.inventoryData.forEach(roomType => {
            roomType.plans.forEach(plan => {
                plan.mealplans.forEach(mealPlan => {
                    mealPlan.values.forEach(value => {
                        value.rate_plan_id = plan.id;
                        value.meal_plan_id = mealPlan.id;
                        reqData.push(value);
                    });
                });
            });
        });

        console.log(reqData);
        this.isSaving = true;
        this.rateInventoryService.saveRateInventoryData(reqData)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Success', 'Success');
                    this.loadRateInventoryData();
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });

    }


    openBulkUpdate() {
        const dialogRef = this.dialog.open(RateBulkUpdateComponent, {
            data: {
                inventorySources: this.inventorySources,
                roomTypes: this.roomTypes,
                hotels: this.hotels,
                filters: {
                    hotel_id: this.filters.hotel_id,
                    room_type_id: this.filters.room_type_id,
                    rate_source_id: this.filters.source_id,
                }
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadRateInventoryData();
            }
        });
    }

    openBulkUpdateWizard() {
        const dialogRef = this.dialog.open(UpdateWizardComponent, {
            data: {
                inventorySources: this.inventorySources,
                roomTypes: this.roomTypes,
                hotels: this.hotels,
                filters: {
                    hotel_id: this.filters.hotel_id,
                    room_type_id: this.filters.room_type_id,
                    rate_source_id: this.filters.source_id,
                }
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.loadRateInventoryData();
            }
        });
    }

    expandRatePlanRates(plan) {
        if (!plan.isExpandable) {
            plan.isExpandable = true;
        } else {
            plan.isExpandable = false;
        }
    }
}
