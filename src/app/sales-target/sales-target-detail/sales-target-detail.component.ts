import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '../../services/user.service';
import {TokenStorage} from '../../core/auth/token-storage.service';
import {HttpClient} from '@angular/common/http';
import {CommonService} from '../../services/common.service';
import {DsrService} from '../../services/dsr.service';
import {FormControl} from '@angular/forms';
import moment from 'moment';
import {SalesTargetService} from '../../services/sales-target.service';
import {NgxPermissionsService} from 'ngx-permissions';


@Component({
    selector: 'm-sales-target-detail',
    templateUrl: './sales-target-detail.component.html',
    styleUrls: ['./sales-target-detail.component.scss'],
    providers: [DsrService, UserService, CommonService, SalesTargetService]
})
export class SalesTargetDetailComponent implements OnInit {

    isSaving: boolean = false;
    isLoading: boolean = false;
    assignToList: any[] = [];
    editRecord: any = {};
    targetEntry: any = {
        users: [],
        target_month: moment().format('YYYY-MM-DD')
    };
    date = new FormControl(moment());
    loggedInUser: any = {};
    userPermissions: any = {};

    constructor(private dialogRef: MatDialogRef<SalesTargetDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private cdr: ChangeDetectorRef,
                private toastService: ToastrService,
                private modalService: NgbModal,
                private userService: UserService,
                private tokenStorage: TokenStorage,
                private salesTargetService: SalesTargetService,
                private http: HttpClient,
                private permissions: NgxPermissionsService,
                private commonService: CommonService,
                private dsrService: DsrService) {


        this.loggedInUser = this.tokenStorage.getUserDetail();
        this.userPermissions = this.permissions.getPermissions();

        if (this.data) {
            this.editRecord = this.data.currentRecord;

            if (this.userPermissions['sales target-update-responsible']) {
                if (this.editRecord.assign_to_id !== this.loggedInUser.id) {
                    this.editRecord.isReadOnly = true;
                }
            } else if (!this.userPermissions['sales target-update']) {
                this.editRecord.isReadOnly = true;
            }
        }
    }

    ngOnInit() {
        this.loadAssignTioUsers();
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    loadTargetOfMonth(month: string) {
        this.isLoading = true;
        this.salesTargetService.getMonthTarget({month: month})
            .subscribe(response => {
                    this.targetEntry.target_month = response.target_month;
                    for (let i = 0; i < this.targetEntry.users.length; i++) {

                        if (this.userPermissions['sales target-update-responsible'] || this.userPermissions['sales target-view-responsible']) {
                            if (this.editRecord.assign_to_id !== this.loggedInUser.id) {
                                this.editRecord.isReadOnly = true;
                            }
                        }
                        const executive = this.targetEntry.users[i];
                        const exTarget = response.users.find(function (node) {
                            return parseInt(node.assign_to_id) === parseInt(executive.assign_to_id);
                        });

                        if (exTarget) {
                            executive.room_nights = exTarget.room_nights;
                            executive.revenue = exTarget.revenue;
                        }
                    }

                    this.isLoading = false;
                    this.cdr.detectChanges();
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    this.cdr.detectChanges();
                });
    }

    loadAssignTioUsers() {
        this.userService.getUserList()
            .subscribe(response => {
                    this.assignToList = response;

                    // Sale
                    const sellers = this.assignToList.filter(itm => {
                        return (itm.role === 'Sales' && itm.status !== 0);
                    });
                    for (let saller of sellers) {
                        this.targetEntry.users.push({
                            name: saller.name,
                            room_nights: null,
                            revenue: null,
                            assign_to_id: saller.id
                        });
                    }

                    if (this.editRecord.id) {
                        this.loadTargetOfMonth(this.editRecord.target_month);
                    }

                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    getUsers() {
        if (this.userPermissions['sales target-update-responsible'] || this.userPermissions['sales target-view-responsible']) {
            const sellers = this.targetEntry.users.filter(itm => {
                return (itm.assign_to_id === this.loggedInUser.id);
            });

            return sellers;
        }
        return this.targetEntry.users;
    }

    dateChangeEvent(type: string) {
        this.targetEntry[type] = this.targetEntry[type].format('YYYY-MM-DD HH:mm:ss');
    }

    saveSalesTarget() {
        this.isSaving = true;
        this.salesTargetService.save(this.targetEntry)
            .subscribe(response => {
                    this.isSaving = false;
                    this.cdr.detectChanges();
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    this.cdr.detectChanges();
                });
    }
}
