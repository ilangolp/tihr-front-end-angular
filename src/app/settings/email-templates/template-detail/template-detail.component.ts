import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {SettingsService} from '../../../services/settings.service';
import {UtilsService} from '../../../core/services/utils.service';
import {CKEditorComponent} from 'ngx-ckeditor';
import {environment} from '../../../../environments/environment';

declare var $: any;

@Component({
    selector: 'm-template-detail',
    templateUrl: './template-detail.component.html',
    styleUrls: ['./template-detail.component.scss'],
    providers: [SettingsService, UtilsService]
})
export class TemplateDetailComponent implements OnInit {
    public editorValue: string = '';
    selectedTabIndex: number = 0;
    isSaving: boolean = false;
    currentRecord: any = {'module': '', content: ''};
    moduleFields: any[] = [];
    selectedAssociatedModule: any = '';
    selectedAssociatedModuleField: any = '';
    selectedAssociatedModuleFieldValue: any = '';
    associatedModules: any[] = [];
    selectedAssociatedModuleFields: any = [];
    tinyApiKey = environment.tinyMCEApiKey;
    config: any = {};
    ngxConfig = {
        placeholder: '',
        tabsize: 2,
        height: 300,
        focus: true,
        uploadImagePath: '',
        dialogsInBody: true,
        toolbar: [
            ['misc', ['fullscreen', 'codeview', 'undo', 'redo']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            ['fontsize', ['fontname', 'fontsize', 'color']],
            ['para', ['style0', 'ul', 'ol', 'paragraph', 'height']],
            ['insert', ['table', 'picture', 'link', 'video', 'hr']]
        ],
    };

    modules: any[] = [];
    @ViewChild('ckEditor') ckEditor: CKEditorComponent;

    constructor(private dialogRef: MatDialogRef<TemplateDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private toastService: ToastrService,
                private utilsService: UtilsService,
                private  settingsService: SettingsService) {

        if (this.data) {
            if (this.data.record.id) {
                this.currentRecord = this.data.record;
                // $('#summernote').summernote('editor.insertText', this.currentRecord.content);
                this.changeModule();
            }
        }

        // this.config = this.utilsService.getTinyConfig();
        this.config = this.utilsService.getCKEditorConfig();

    }

    ngOnInit() {
        this.loadModuleFields();
        const htmlString = this.currentRecord.content;
        // $('#summernote').summernote('editor.insertText', htmlString);

    }

    changeTemplateContent(event) {
        console.log('change event');
        if (event) {
            this.currentRecord.content = event;
        }
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.isSaving = true;
        this.settingsService.saveEmailTemplates(this.currentRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    loadModuleFields() {
        this.settingsService.getModuleFields()
            .subscribe(response => {
                    this.moduleFields = response;
                    this.modules = this.moduleFields.filter(item => {
                        return item.is_root_module === true;
                    });
                    this.changeModule();
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    changeModule() {
        if (this.moduleFields.length < 1) {
            return;
        }

        this.associatedModules = [];
        const selModule: any = this.moduleFields.find(item => {
            return item.name === this.currentRecord.module;
        });

        if (selModule) {
            this.associatedModules = [selModule];
            if (selModule.associatedModules) {
                selModule.associatedModules.forEach(itm => {
                    const assMod = this.moduleFields.find(item => {
                        return item.name === itm;
                    });
                    if (assMod) {
                        this.associatedModules.push(assMod);
                    }
                });
            }
        }
    }

    changeAssociateModuleField() {
        this.selectedAssociatedModuleFieldValue = this.selectedAssociatedModuleField.field_value;
        /*this.associatedModules = this.moduleFields.filter(item => {
            return item.name = this.currentRecord.module;
        });
        console.log(this.associatedModules);*/
    }

    appendText() {
        // let content = this.currentRecord.content;
        // content = this.currentRecord.content + this.selectedAssociatedModuleFieldValue;
        // this.ckEditor.writeValue(content);
        $('#summernote').summernote('editor.insertText', this.selectedAssociatedModuleFieldValue);
    }
}
