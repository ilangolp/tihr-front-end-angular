import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {AuthenticationService} from '../../../../core/auth/authentication.service';
import {Router} from '@angular/router';
import {Subject} from 'rxjs';
import {AuthNoticeService} from '../../../../core/auth/auth-notice.service';
import {NgForm} from '@angular/forms';
import * as objectPath from 'object-path';
import {TranslateService} from '@ngx-translate/core';
import {SpinnerButtonOptions} from '../../../partials/content/general/spinner-button/button-options.interface';
import {TokenStorage} from '../../../../core/auth/token-storage.service';
import {environment} from '../../../../../environments/environment';
import {AclService} from '../../../../core/services/acl.service';

@Component({
    selector: 'm-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit, OnDestroy {
    geoLocationPosition: any;
    currentLat: any;
    currentLong: any;
    public model: any = {/*email: '',*/ user_name: '', password: '', lat: '', long: ''};
    @Output() actionChange = new Subject<string>();
    public loading = false;

    @Input() action: string;

    @ViewChild('f') f: NgForm;
    errors: any = [];

    spinner: SpinnerButtonOptions = {
        active: false,
        spinnerSize: 18,
        raised: true,
        buttonColor: 'primary',
        spinnerColor: 'accent',
        fullWidth: false
    };

    constructor(
        private authService: AuthenticationService,
        private router: Router,
        public authNoticeService: AuthNoticeService,
        private translate: TranslateService,
        private cdr: ChangeDetectorRef,
        private aclService: AclService,
        private tokenStorage: TokenStorage
    ) {
    }

    submit() {
        this.spinner.active = true;
        if (this.validate(this.f)) {
            this.model.lat = this.currentLat;
            this.model.long = this.currentLong;
            this.authService.login(this.model).subscribe(response => {
                    if (typeof response !== 'undefined') {
                        // response.user.permissions.push('USER');
                        response.user.permissions.push('Authenticated');
                        this.aclService.aclModel.permissions = {};
                        this.aclService.aclModel.permissions[response.user.department_name] = response.user.permissions;
                        // this.aclService.aclModel.permissions['Authenticated'] = ['Authenticated'];
                        this.authService.saveAccessData({
                            accessToken: response.token,
                            refreshToken: response.token,
                            user: response.user,
                            roles: [response.user.department_name, 'Authenticated'],
                            permissions: this.aclService.aclModel.permissions
                        });
                        this.authService.initializePermissions();
                        if (response.user.department_name === 'Accounts') {
                            this.router.navigate(['/account-tasks']);
                        } else {
                            this.router.navigate(['/']);
                        }
                    } else {
                        this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'error');
                    }
                    this.spinner.active = false;
                    this.cdr.detectChanges();
                },
                error => {
                    this.authNoticeService.setNotice(error.error.message, 'error');
                    this.spinner.active = false;
                    this.cdr.detectChanges();
                });
        }
    }

    ngOnInit(): void {
        this.trackCurrentLocation();
        // demo message to show
        /*if (!this.authNoticeService.onNoticeChanged$.getValue()) {
            const initialNotice = `Use account
            <strong>admin@demo.com</strong> and password
            <strong>demo</strong> to continue.`;
            this.authNoticeService.setNotice(initialNotice, 'success');
        }*/
    }

    trackCurrentLocation() {
        if (window.navigator && window.navigator.geolocation) {
            window.navigator.geolocation.getCurrentPosition(
                position => {
                    this.geoLocationPosition = position,
                    this.currentLat = position.coords.latitude;
                    this.currentLong = position.coords.longitude;
                },
                error => {
                    switch (error.code) {
                        case 1:
                            console.log('Permission Denied');
                            break;
                        case 2:
                            console.log('Position Unavailable');
                            break;
                        case 3:
                            console.log('Timeout');
                            break;
                    }
                }
            );
        }
    }

    ngOnDestroy(): void {
        this.authNoticeService.setNotice(null);
    }

    validate(f: NgForm) {
        this.errors = [];

        if (!this.currentLat || !this.currentLong) {
            this.trackCurrentLocation();
            if (!this.currentLat || !this.currentLong) {
                this.errors.push('Only secure origins are allowed, please enable device\'s location '
                    + '<a href="' + environment.Base_Url + '">click</a>' + ' here to redirect secure url');
                if (this.errors.length > 0) {
                    this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
                    this.spinner.active = false;
                }
                return false;
            }
        }

        if (f.form.status === 'VALID') {
            return true;
        }

        if (objectPath.get(f, 'form.controls.user_name.errors.required')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.REQUIRED', {name: this.translate.instant('AUTH.INPUT.USERNAME')}));
        }
        if (objectPath.get(f, 'form.controls.user_name.errors.minlength')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.MIN_LENGTH', {name: this.translate.instant('AUTH.INPUT.USERNAME')}));
        }

        if (objectPath.get(f, 'form.controls.password.errors.required')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.INVALID', {name: this.translate.instant('AUTH.INPUT.PASSWORD')}));
        }
        if (objectPath.get(f, 'form.controls.password.errors.minlength')) {
            this.errors.push(this.translate.instant('AUTH.VALIDATION.MIN_LENGTH', {name: this.translate.instant('AUTH.INPUT.PASSWORD')}));
        }

        if (this.errors.length > 0) {
            this.authNoticeService.setNotice(this.errors.join('<br/>'), 'error');
            this.spinner.active = false;
        }

        return false;
    }

    forgotPasswordPage(event: Event) {
        this.action = 'forgot-password';
        this.actionChange.next(this.action);
    }

    register(event: Event) {
        this.action = 'register';
        this.actionChange.next(this.action);
    }
}
