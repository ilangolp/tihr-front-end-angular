import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {InquiryService} from './inquiry.service';
import {SpinnerButtonOptions} from '../content/partials/content/general/spinner-button/button-options.interface';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {AddInquiryComponent} from './add-inquiry/add-inquiry.component';
import {UserService} from '../services/user.service';
import {HotelService} from '../services/hotel.service';
import {ToastrService} from 'ngx-toastr';
import {SelectionModel} from '../../../node_modules/@angular/cdk/collections';
import {LayoutUtilsService, MessageType} from '../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {QueryParamsModel} from '../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {InquiryDataSource} from '../Models/data-sources/inquiry.dataSource';
import {debounceTime, distinctUntilChanged, map, tap} from 'rxjs/operators';
import {fromEvent, merge, Observable, of} from 'rxjs';
import {FormControl} from '@angular/forms';
import {DataSource} from '@oasisdigital/angular-material-search-select';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import * as moment from 'moment';
import {ActivatedRoute} from '@angular/router';
import {TokenStorage} from '../core/auth/token-storage.service';
import {UtilsService} from '../core/services/utils.service';

@Component({
	selector: 'm-inquiry',
	templateUrl: './inquiry.component.html',
	styleUrls: ['./inquiry.component.scss'],
	providers: [InquiryService, UserService, HotelService, UtilsService]
})
export class InquiryComponent implements OnInit {
	serverUrl = environment.Server_URL;
	loggedInUser: any = {};
	isCollapsedFilter: boolean = true;
	inquiryFilters: any = {};
	dataSource: any = InquiryDataSource;
	inquiries: any[] = [];
	isLoading: boolean = false;
	isSettingsLoading: boolean = false;
	itemsPerPage: number = 10;
	travelAgents: any[] = [];
	companies: any[] = [];
	spinner: SpinnerButtonOptions = {
		active: false,
		spinnerSize: 18,
		raised: true,
		buttonColor: 'primary',
		spinnerColor: 'accent',
		fullWidth: false
	};
	histories: any = [];

	displayedColumns: string[] = [
		'select',
		'inquiry_id',
		'inquiry_date',
		'hotel',
		'name',
		'check_in_date',
		'check_out_date',
		'rooms',
		// 'room_nights',
		'offer_rate',
		'status',
		'assign_to',
		'source',
		'type',
		'actions'
	];

	// displayedColumns = ['id', 'name','inquiry_date'];
	// dataSource = new MatTableDataSource(this.inquiries);
	selection = new SelectionModel<any>(true, []);

	@ViewChild(MatSort) sort: MatSort;
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild('searchInput') searchInput: ElementRef;

	dateOptions: any = {};

	hotelList: any = [];
	hotelListSettings: any = {};
	selectedHotelLists = [];

	assignToUserList: any[] = [];
	userListSettings: any = {};
	selectedUserLists = [];

	inquiryStatusList: any = [];
	selectedStatuses = [];
	statusSettings: any = {};

	inquirySubStatusList: any = [];
	selectedSubStatusesLists = [];
	subStatusListSettings: any = {};
	filteredInquirySubStatusList: any = [];

	inquirySourceList: any = [];
	selectedSourcesLists = [];
	sourceSettings: any = {};

	isVisibleCompanies: any = false;
	isVisibleAgents: any = false;

	travel_agent = new FormControl(null, []);
	travelAgentDataSource: DataSource;

	companyList = new FormControl(null, []);
	companyDataSource: DataSource;
	companyLists: any = [];
	selectedCompanies: any = [];
	companySettings: any = {};
	searchCompanyTypes: any = [];

	inquiryTypesList: any = [];
	selectedInquiryTypesLists = [];
	inquiryTypesListsSettings: any = {};

	inquiryDiscountStatusList: any = [];
	selectedDiscountStatuses = [];
	inquiryDiscountStatusSettings: any = {};

	inquiryPaymentStatusList: any = [];
	selectedPaymentStatuses = [];
	inquiryPaymentStatusSettings: any = {};

	inquiryPaymentSubStatusList: any = [];
	selectedPaymentSubStatuses = [];
	inquiryPaymentSubStatusSettings: any = {};
	filteredInquiryPaymentSubStatusList: any = [];

	discountApprovedByList: any[] = [];
	discountApprovedByListSettings: any = {};
	selectedDiscountApprovedBy = [];

	constructor(private modalService: NgbModal,
				public dialog: MatDialog,
				private cdr: ChangeDetectorRef,
				private http: HttpClient,
				private _route: ActivatedRoute,
				private inquiryService: InquiryService,
				private hotelService: HotelService,
				private toastService: ToastrService,
				private activatedRoute: ActivatedRoute,
				private layoutUtilsService: LayoutUtilsService,
				private tokenStorage: TokenStorage,
				private utilsService: UtilsService) {

		const apiURL = environment.API_URL + '/companies/';
		this.loggedInUser = this.tokenStorage.getUserDetail();

		this.dateOptions = this.utilsService.getCalenderConfig({
			startDate: moment(),
			endDate: moment()
		});

		this.statusSettings = this.utilsService.getMultiSelctConfig({text: 'Select Status', badgeShowLimit: 2});
		this.subStatusListSettings = this.utilsService.getMultiSelctConfig({text: 'Select Sub Status', badgeShowLimit: 1});
		this.inquiryTypesListsSettings = this.utilsService.getMultiSelctConfig({text: 'Select Inquiry Type', badgeShowLimit: 1});
		this.hotelListSettings = this.utilsService.getMultiSelctConfig({text: 'Select Hotels', labelKey: 'code', groupBy: 'status'});
		this.userListSettings = this.utilsService.getMultiSelctConfig({
			text: 'Select Assign To',
			badgeShowLimit: 1,
			groupBy: 'status_name'
		});
		this.sourceSettings = this.utilsService.getMultiSelctConfig({text: 'Select Source'});
		this.companySettings = this.utilsService.getMultiSelctConfig({text: 'Select Company / OTA / TA'});

		this.inquiryPaymentStatusSettings = this.utilsService.getMultiSelctConfig({text: 'Select Payment Status', badgeShowLimit: 1});
		this.inquiryPaymentSubStatusSettings = this.utilsService.getMultiSelctConfig({
			text: 'Select Payment Sub Status',
			badgeShowLimit: 1
		});
		this.inquiryDiscountStatusSettings = this.utilsService.getMultiSelctConfig({text: 'Select Discount Status', badgeShowLimit: 2});

		this.discountApprovedByListSettings = this.utilsService.getMultiSelctConfig({
			text: 'Select Approved By',
			badgeShowLimit: 1,
			groupBy: 'status_name'
		});

		this.travelAgentDataSource = {
			displayValue(value: any): Observable<any | null> {
				console.log('finding display value for', value);
				if (typeof value === 'string') {
					value = parseInt(value, 10);
				}
				if (typeof value !== 'number') {
					return of(null);
				}

				return http.get<any>(apiURL + 'find/' + value).pipe(
					map(e => ({
						value: e.id,
						display: `${e.name}`,
						details: {}
					}))
				);
			},
			search(term: string): Observable<any[]> {
				return http.get<any[]>(apiURL + 'search', {
					params: {
						pageSize: '5',
						q: term || '',
						type: '1',
						_sort: 'name'
					}
				}).pipe(
					map(list => list.map(e => ({
						value: e.id,
						display: `${e.name}`,
						details: {}
					}))));
			}
		};


		this.companyDataSource = {
			displayValue(value: any): Observable<any | null> {
				console.log('finding display value for', value);
				if (typeof value === 'string') {
					value = parseInt(value, 10);
				}
				if (typeof value !== 'number') {
					return of(null);
				}

				return http.get<any>(apiURL + 'find/' + value).pipe(
					map(e => ({
						value: e.id,
						display: `${e.name}`,
						details: {}
					}))
				);
			},
			search(term: string): Observable<any[]> {
				return http.get<any[]>(apiURL + 'search', {
					params: {
						pageSize: '5',
						q: term || '',
						type: '0',
						_sort: 'name'
					}
				}).pipe(
					map(list => list.map(e => ({
						value: e.id,
						display: `${e.name}`,
						details: {}
					}))));
			}
		};
	}

	ngOnInit() {
		this.initFilters();
		this.getInquirySettingLists();

		this.paginator.page
			.pipe(
				tap(() => this.loadInquiriesList())
			)
			.subscribe();


		fromEvent(this.searchInput.nativeElement, 'keyup')
			.pipe(
				debounceTime(350),
				distinctUntilChanged(),
				tap(() => {
					this.paginator.pageIndex = 0;
					this.loadInquiriesList();
				})
			)
			.subscribe();


		// this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

		/* Data load will be triggered in two cases:
		- when a pagination event occurs => this.paginator.page
		- when a sort event occurs => this.sort.sortChange
		**/
		merge(this.sort.sortChange, this.paginator.page)
			.pipe(
				tap(() => {
					this.loadInquiriesList();
				})
			)
			.subscribe();

		const queryParams = new QueryParamsModel(
			'',
			'desc',
			'',
			this.paginator.pageIndex + 1,
			this.paginator.pageSize
		);
		this.dataSource = new InquiryDataSource(this.inquiryService);
		// First load
		this.dataSource.loadInquiries(queryParams);
		this.dataSource.entitySubject.subscribe(res => (this.inquiries = res));
		// this.dataSource.sort = this.sort;

		this.activatedRoute
			.queryParams
			.subscribe(queryParams => {
				if (!this.isSettingsLoading) {
					this.getInquirySettingLists();
				}
			});
	}

	getInqTotalAmount() {
		let amt = 0;
		if (this.inquiries) {
			this.inquiries.forEach(rec => {
				amt += parseFloat(rec.offer_rate);
			});
		}

		return amt;
	}

	getRoomTotal() {
		let roomTotal = 0;
		if (this.inquiries) {
			this.inquiries.forEach(rec => {
				roomTotal += parseInt(rec.rooms);
			});
		}

		return roomTotal;
	}

	initFilters() {
		this.inquiryFilters = {
			check_in_date_fromDate: moment().format('YYYY-MM-DD'),
			check_in_date_toDate: moment().format('YYYY-MM-DD'),
			check_out_date_fromDate: moment().format('YYYY-MM-DD'),
			check_out_date_toDate: moment().format('YYYY-MM-DD'),
			inquiry_fromDate: moment().format('YYYY-MM-DD'),
			inquiry_toDate: moment().format('YYYY-MM-DD'),
			check_in_date_filter: false,
			check_out_date_filter: false,
			inquiry_date_filter: false,
			source_id: [],
		};
	}

	applyQueryFilters() {
		this._route.params.subscribe(paramsData => {
			let params: any = [];
			params = this._route.snapshot.queryParams;
			console.log(params);
			if (params.iqId) {
				this.inquiryService.findInquiry(params.iqId)
					.subscribe(response => {
							this.openInquiryForm(response, params);
						},
						error => {
							this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
						});
			}
		});
	}

	loadInquiriesList() {
		const q = this.searchInput.nativeElement.value;
		let queryParams: any = new QueryParamsModel(
			q,
			this.sort.direction,
			this.sort.active,
			this.paginator.pageIndex + 1,
			this.paginator.pageSize);

		queryParams = Object.assign(queryParams, this.inquiryFilters);

		delete queryParams.check_in_date_filter;
		delete queryParams.check_out_date_filter;
		delete queryParams.inquiry_date_filter;

		if (!this.inquiryFilters.inquiry_date_filter) {
			delete queryParams.inquiry_fromDate;
			delete queryParams.inquiry_toDate;
		}

		if (!this.inquiryFilters.check_in_date_filter) {
			delete queryParams.check_in_date_fromDate;
			delete queryParams.check_in_date_toDate;
		}

		if (!this.inquiryFilters.check_out_date_filter) {
			delete queryParams.check_out_date_fromDate;
			delete queryParams.check_out_date_toDate;
		}


		this.dataSource.loadInquiries(queryParams);
		this.selection.clear();
	}

	applyInquiriesFilter() {
		this.paginator.pageIndex = 0;
		if (this.isVisibleAgents) {
			this.inquiryFilters.travel_agent_id = this.travel_agent.value;
		}
		if (this.isVisibleCompanies) {
			this.inquiryFilters.company_id = this.companyList.value;
		}
		if (!this.inquiryFilters.company_id) {
			this.inquiryFilters.company_id = this.travel_agent.value;
		}
		this.loadInquiriesList();
	}

	resetInquiriesFilter() {
		this.selectedUserLists = [];
		this.selectedHotelLists = [];
		this.selectedStatuses = [];
		this.selectedSubStatusesLists = [];
		this.selectedInquiryTypesLists = [];
		this.selectedSourcesLists = [];
		this.selectedPaymentStatuses = [];
		this.selectedPaymentSubStatuses = [];
		this.selectedDiscountStatuses = [];
		this.selectedDiscountApprovedBy = [];
		this.companyList = new FormControl(null, []);
		this.travel_agent = new FormControl(null, []);
		this.isCollapsedFilter = true;
		this.initFilters();
		this.paginator.pageIndex = 0;
		this.loadInquiriesList();
	}

	getInquirySettingLists() {
		if (this.assignToUserList.length <= 0 ||
			this.hotelList.length <= 0 ||
			this.inquirySourceList.length <= 0 ||
			this.inquiryStatusList.length < 1 ||
			this.inquirySubStatusList.length <= 0 ||
			this.inquiryDiscountStatusList.length < 1 ||
			this.inquiryPaymentStatusList.length < 1 ||
			this.inquiryPaymentSubStatusList.length <= 0 ||
			this.discountApprovedByList.length <= 0 ||
			this.inquiryTypesList.length <= 0 ||
			this.travelAgents.length <= 0 ||
			this.companies.length <= 0
		) {
			this.isSettingsLoading = true;
			this.inquiryService.getInquirySettingLists()
				.subscribe(response => {
						this.assignToUserList = response.users;
						this.hotelList = response.hotels;
						this.inquirySourceList = response.sources;
						this.inquiryStatusList = response.statuses;
						this.inquirySubStatusList = response.subStatuses;
						this.inquiryDiscountStatusList = [
							{'id': 1, 'name': 'Pending'},
							{'id': 2, 'name': 'Approved'},
							{'id': 3, 'name': 'Rejected'},
						];
						this.inquiryPaymentStatusList = response.inquiryPaymentStatuses;
						this.inquiryPaymentSubStatusList = response.inquiryPaymentSubStatuses;
						this.discountApprovedByList = response.users;
						this.inquiryTypesList = response.types;
						this.travelAgents = response.travel_agents;
						this.companies = response.companies;
						this.isSettingsLoading = false;
						this.applyQueryFilters();
					},
					error => {
						this.isSettingsLoading = false;
						this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
					});
		} else {
			this.applyQueryFilters();
		}

	}

	/*fetchInquiries() {
		this.spinner.active = true;
		this.isLoading = true;
		this.inquiryService.getInquiries({page: 1, pageSize: 1})
			.subscribe(
				response => {
					this.inquiries = response;
					// this.dataSource = new MatTableDataSource(this.inquiries);
					this.dataSource.data = this.inquiries;
					this.spinner.active = false;
					this.isLoading = false;
					this.cdr.detectChanges();
				},
				error => {
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
					this.spinner.active = false;
					this.isLoading = false;
					this.cdr.detectChanges();
				});
	}*/

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	resetFilter() {
		this.paginator.pageIndex = 0;
		this.loadInquiriesList();
	}

	deleteInquiry(inquiry) {
		const dialogRef = this.layoutUtilsService.deleteElement();
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			this.layoutUtilsService.showActionNotification('Not Implemented yet', MessageType.Delete);
		});
	}

	moreActionPerform(record: any = {}, actionName: string) {
		const params = {selectTab: actionName};
		switch (actionName) {
			case 'notes':
				this.openInquiryForm(record, {selectTab: 0});
				break;
			case 'payments':
				this.openInquiryForm(record, {selectTab: 1});
				break;
			case 'invoices':
				this.openInquiryForm(record, {selectTab: 2});
				break;
		}
	}

	openInquiryForm(currentRecord: any = {}, params: any = {}) {
		const dialogRef = this.dialog.open(AddInquiryComponent, {
			data: {
				statuses: this.inquiryStatusList,
				sources: this.inquirySourceList,
				assignToList: this.assignToUserList,
				subStatuses: this.inquirySubStatusList,
				paymentStatuses: this.inquiryPaymentStatusList,
				paymentSubStatuses: this.inquiryPaymentSubStatusList,
				types: this.inquiryTypesList,
				hotels: this.hotelList,
				travel_agents: this.travelAgents,
				companies: this.companies,
				currentRecord: currentRecord,
				originalBooking: Object.assign({}, currentRecord),
				params: params,
				detailViewType: 1
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				// const existingIndex = this.inquiries.findIndex(item => item.id === result.id);
				// if (existingIndex !== -1) {
				//     this.inquiries.splice(existingIndex, 1, result);
				//     this.dataSource.data = this.inquiries;
				// } else {
				//     this.inquiries.push(result);
				//     this.dataSource.data = this.inquiries;
				// }
				this.loadInquiriesList();
			}
		});
	}

	/*loadInquiriesPage() {
		console.log(this.paginator.pageIndex);
		console.log(this.paginator.pageSize);
	}*/

	/** SELECTION */
	isAllSelected(): boolean {
		const numSelected = this.selection.selected.length;
		const numRows = this.inquiries.length;
		return numSelected === numRows;
	}

	masterToggle() {
		if (this.selection.selected.length === this.inquiries.length) {
			this.selection.clear();
		} else {
			this.inquiries.forEach(row => this.selection.select(row));
		}
	}

	selectedIQDate(value: any) {
		this.inquiryFilters.inquiry_fromDate = value.start.format('YYYY-MM-DD');
		this.inquiryFilters.inquiry_toDate = value.end.format('YYYY-MM-DD');
	}

	/*clearIQDate() {
		this.inquiryFilters.inquiry_date_filter = false;
	}*/

	selectedCheckInDate(value: any) {
		this.inquiryFilters.check_in_date_fromDate = value.start.format('YYYY-MM-DD');
		this.inquiryFilters.check_in_date_toDate = value.end.format('YYYY-MM-DD');
	}

	/*clearCheckInDate() {
		this.inquiryFilters.check_in_date_filter = false;
	}*/

	selectedCheckOutDate(value: any) {
		this.inquiryFilters.check_out_date_fromDate = value.start.format('YYYY-MM-DD');
		this.inquiryFilters.check_out_date_toDate = value.end.format('YYYY-MM-DD');
	}

	/*clearCheckOutDate() {
		this.inquiryFilters.check_out_date_filter = false;
	}*/

	/* Bootstrap Data Filter */
	onUserSelect(item: any = {}) {
		this.inquiryFilters.assign_to_id = [];
		if (this.selectedUserLists.length > 0) {
			this.selectedUserLists.forEach(itm => {
				this.inquiryFilters.assign_to_id.push(itm.id);
			});
		}
	}

	onStatusSelect(item: any = {}) {
		this.inquiryFilters.status_id = [];
		if (this.selectedStatuses.length > 0) {
			this.selectedStatuses.forEach(itm => {
				this.inquiryFilters.status_id.push(itm.id);
			});

			const tempSelected = [];
			this.selectedSubStatusesLists.filter(itm => {
				if (this.inquiryFilters.status_id.includes(itm.status_id)) {
					tempSelected.push(itm);
				}
			});
			this.selectedSubStatusesLists = tempSelected;
		} else {
			this.inquiryStatusList.forEach(itm => {
				this.inquiryFilters.status_id.push(itm.id);
			});
		}
		this.filterSubStatus();
	}

	onSubStatusSelect(item: any = {}) {
		this.inquiryFilters.sub_status_id = [];
		if (this.selectedSubStatusesLists.length > 0) {
			this.selectedSubStatusesLists.forEach(itm => {
				this.inquiryFilters.sub_status_id.push(itm.id);
			});
		} else {
			this.inquirySubStatusList.forEach(itm => {
				this.inquiryFilters.sub_status_id.push(itm.id);
			});
		}
	}

	onPaymentStatusSelect(item: any = {}) {
		this.inquiryFilters.payment_status_id = [];
		if (this.selectedPaymentStatuses.length > 0) {
			this.selectedPaymentStatuses.forEach(itm => {
				this.inquiryFilters.payment_status_id.push(itm.id);
			});

			const tempSelected = [];
			this.selectedPaymentSubStatuses.filter(itm => {
				if (this.inquiryFilters.payment_status_id.includes(itm.status_id)) {
					tempSelected.push(itm);
				}
			});
			this.selectedPaymentSubStatuses = tempSelected;
		}
		this.filterPaymentSubStatus();
	}

	onPaymentSubStatusSelect(item: any = {}) {
		this.inquiryFilters.payment_sub_status_id = [];
		if (this.selectedPaymentSubStatuses.length > 0) {
			this.selectedPaymentSubStatuses.forEach(itm => {
				this.inquiryFilters.payment_sub_status_id.push(itm.id);
			});
		} /*else {
            this.inquiryPaymentSubStatusList.forEach(itm => {
                this.inquiryFilters.payment_sub_status_id.push(itm.id);
            });
        }*/
	}

	filterSubStatus() {
		this.filteredInquirySubStatusList = [];
		this.inquirySubStatusList.filter(itm => {
			if (this.inquiryFilters.status_id.includes(itm.status_id)) {
				this.filteredInquirySubStatusList.push(itm);
			}
		});
	}

	filterPaymentSubStatus() {
		this.filteredInquiryPaymentSubStatusList = [];
		this.inquiryPaymentSubStatusList.filter(itm => {
			if (this.inquiryFilters.payment_status_id.includes(itm.status_id)) {
				this.filteredInquiryPaymentSubStatusList.push(itm);
			}
		});
	}

	onDiscountStatusSelect(item: any = {}) {
		this.inquiryFilters.discount_status = [];
		if (this.selectedDiscountStatuses.length > 0) {
			this.selectedDiscountStatuses.forEach(itm => {
				this.inquiryFilters.discount_status.push(itm.id);
			});
		} else {
			this.inquiryDiscountStatusList.forEach(itm => {
				this.inquiryFilters.discount_status.push(itm.id);
			});
		}
	}

	onDiscountApprovedBySelect(item: any = {}) {
		this.inquiryFilters.discount_approved_by = [];
		if (this.selectedDiscountApprovedBy.length > 0) {
			this.selectedDiscountApprovedBy.forEach(itm => {
				this.inquiryFilters.discount_approved_by.push(itm.id);
			});
		}
	}

	onHotelSelect(item: any = {}) {
		console.log(this.selectedHotelLists);
		this.inquiryFilters.hotel_id = [];
		if (this.selectedHotelLists.length > 0) {
			this.selectedHotelLists.forEach(itm => {
				this.inquiryFilters.hotel_id.push(itm.id);
			});
		}
	}

	onSourceSelectionEvent(item: any = {}) {
		this.inquiryFilters.source_id = [];
		if (this.selectedSourcesLists.length > 0) {
			this.selectedSourcesLists.forEach(itm => {
				this.inquiryFilters.source_id.push(itm.id);
			});
		}
		this.checkCompanySourceSelected();
	}

	onCompanySelectEvent(item: any = {}) {
		this.inquiryFilters.company_id = [];
		if (this.selectedCompanies.length > 0) {
			this.selectedCompanies.forEach(itm => {
				this.inquiryFilters.company_id.push(itm.id);
			});
		}
	}

	checkCompanySourceSelected() {
		const company = this.inquirySourceList.find(item => item.name === 'Company');
		const ota = this.inquirySourceList.find(item => item.name === 'OTA');
		const travelAgent = this.inquirySourceList.find(item => item.name === 'Travel Agent');

		const compInd = this.inquiryFilters.source_id.indexOf(company.id);
		const otaInd = this.inquiryFilters.source_id.indexOf(ota.id);
		const taInd = this.inquiryFilters.source_id.indexOf(travelAgent.id);

		this.searchCompanyTypes = [];
		if (compInd >= 0) {
			this.searchCompanyTypes.push(0);
		}

		if (taInd >= 0) {
			this.searchCompanyTypes.push(1);
		}

		if (otaInd >= 0) {
			this.searchCompanyTypes.push(2);
		}
	}

	onSearchCompany(evt: any) {
		console.log(evt.target.value);
		this.companyLists = [];
		const companySearchParams = {
			pageSize: '10',
			q: evt.target.value,
			type: this.searchCompanyTypes.join(','),
			_sort: 'name'
		};

		this.http.get(environment.API_URL + '/companies/search', {params: companySearchParams})
			.subscribe(response => {
				console.log(response);
				this.companyLists = response;
			}, error => {
			});
	}

	onInquiryTypesListSelect(item: any = {}) {
		this.inquiryFilters.type_id = [];
		if (this.selectedInquiryTypesLists.length > 0) {
			this.selectedInquiryTypesLists.forEach(itm => {
				this.inquiryFilters.type_id.push(itm.id);
			});
		} else {
			this.inquiryTypesList.forEach(itm => {
				this.inquiryFilters.type_id.push(itm.id);
			});
		}
	}

	loadInquiryHistory(id, content) {
		this.inquiryService.getHistory(id)
			.subscribe(response => {
					this.histories = response;
					this.modalService.open(content);
				},
				error => {
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}
}
