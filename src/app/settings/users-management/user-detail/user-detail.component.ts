import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TokenStorage} from '../../../core/auth/token-storage.service';
import {UserService} from '../../../services/user.service';
import {DataSource} from '@oasisdigital/angular-material-search-select';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {FormControl, Validators} from '@angular/forms';
import {RoleService} from '../../../services/role.service';

@Component({
    selector: 'm-user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.scss'],
    providers: [UserService, RoleService]
})
export class UserDetailComponent implements OnInit {

    isSaving: boolean = false;
    isRequirePsw: boolean = true;
    hidePassword: boolean = true;
    designations: any[] = [];
    departments: any[] = [];
    hotels: any[] = [];
    userRoles: any[] = [];
    userRecord: any = {
        status: 1,
        ota_booking_assign: false
    };
    currentPermissionCategory = 'Module';

    genders: any[] = [
        {
            id: 'Male',
            name: 'Male'
        },
        {
            id: 'Female',
            name: 'Female'
        }
    ];
    locationDataSource: DataSource;
    citiesLIst = new FormControl(null, [Validators.required]);

    constructor(public dialogRef: MatDialogRef<UserDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private cdr: ChangeDetectorRef,
                private toastService: ToastrService,
                private modalService: NgbModal,
                private http: HttpClient,
                private  roleService: RoleService,
                private tokenStorage: TokenStorage,
                private userService: UserService) {
        this.isRequirePsw = true;
        if (this.data) {
            this.designations = data.designations;
            this.departments = data.departments;
            this.hotels = data.hotels;
            this.userRoles = data.userRoles;
            this.userRecord.permissions = this.roleService.defaultPermissions();
            if (this.data.currentRecord.id) {
                this.isRequirePsw = false;
                this.userRecord = this.data.currentRecord;
                this.userRecord.permissions = this.roleService.preparePermissions(this.userRecord.permissions);
                this.citiesLIst.setValue(this.userRecord.location_id);
            }
        }

        const apiURL = environment.API_URL + '/search-cities';
        this.locationDataSource = {
            displayValue(value: any): Observable<any | null> {
                console.log('finding display value for', value);
                if (typeof value === 'string') {
                    value = parseInt(value, 10);
                }
                if (typeof value !== 'number') {
                    return of(null);
                }

                return http.get<any>(apiURL + '?id=' + value).pipe(
                    map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))
                );
            },
            search(term: string): Observable<any[]> {
                return http.get<any[]>(apiURL, {
                    params: {
                        pageSize: '10',
                        q: term || '',
                        type: '1',
                        _sort: 'name'
                    }
                }).pipe(
                    map(list => list.map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))));
            }
        };
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    setUsername() {
        const first_name = this.userRecord.first_name ? this.userRecord.first_name : '';
        const last_name = this.userRecord.last_name ? this.userRecord.last_name : '';
        this.userRecord.user_name = (first_name + last_name).replace(/\s/g, '').toLowerCase();
    }

    saveUser() {
        this.isSaving = true;
        this.userRecord.name = this.userRecord.first_name + ' ' + this.userRecord.last_name;
        this.userRecord.location_id = this.citiesLIst.value;
        this.userService.save(this.userRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    dobDateChangeEvent(type: string) {
        this.userRecord[type] = this.userRecord[type].format('YYYY-MM-DD HH:mm:ss');
    }

    changeViewPermissionCategory(val) {
        this.currentPermissionCategory = val;
    }

    disabledRelevantPermissions(permission) {
        permission.view = '';
        permission.add = '';
        permission.update = '';
        permission.delete = '';
    }

    changeUserRole() {
        if (this.userRecord.role_id) {
            const selRole = this.userRoles.find(itm => {
                return itm.id === this.userRecord.role_id;
            });

            if (selRole) {
                if (selRole.permissions) {
                    this.userRecord.permissions = this.roleService.preparePermissions(selRole.permissions);
                }
            }
        }
    }
}
