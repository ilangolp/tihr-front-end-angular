import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {CompanyService} from '../../services/company.service';
import {CommonService} from '../../services/common.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TokenStorage} from '../../core/auth/token-storage.service';
import {InquiryService} from '../inquiry.service';
import {isEmpty} from 'rxjs/operators';

@Component({
    selector: 'm-inq-discount-approve',
    templateUrl: './inq-discount-approve.component.html',
    styleUrls: ['./inq-discount-approve.component.scss'],
    providers: [InquiryService, CompanyService, CommonService]
})
export class InqDiscountApproveComponent implements OnInit {

    isSaving: boolean = false;
    bookingEntry: any = {};
    bookingRecord: any = {};

    discountApprovalValidationRule: any;

    constructor(public dialogRef: MatDialogRef<InqDiscountApproveComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private cdr: ChangeDetectorRef,
                private toastService: ToastrService,
                private commonService: CommonService,
                private modalService: NgbModal,
                private tokenStorage: TokenStorage,
                private  inquiryService: InquiryService) {
        this.discountApprovalValidationRule = this.tokenStorage.getAppConfigByKey('discount_approval_validation_rule');
        if (this.data) {
            this.bookingRecord = data.bookingDetail;
            this.bookingEntry.id = this.bookingRecord.id;
            this.bookingEntry.discount_ratio = this.bookingRecord.discount_ratio;
            this.bookingEntry.discount_amount = this.bookingRecord.discount_amount;
            this.bookingEntry.offer_rate = this.bookingRecord.offer_rate;
        }
    }

    ngOnInit() {
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    approveDiscount() {
        this.isSaving = true;
        this.inquiryService.updateDiscountStatus(this.bookingEntry.id, 2, this.bookingEntry)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Discount Approved', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    this.cdr.detectChanges();
                });
    }

    rejectDiscount() {
        this.isSaving = true;
        this.inquiryService.updateDiscountStatus(this.bookingEntry.id, 3, this.bookingEntry)
            .subscribe(response => {
                    this.isSaving = false;
                    this.toastService.success('Discount Rejected', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    this.cdr.detectChanges();
                });
    }
}
