import {AfterContentChecked, Component, OnInit, ViewChild} from '@angular/core';
import * as moment from 'moment';
import {InquiryService} from '../inquiry.service';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../../services/user.service';
import {HotelService} from '../../services/hotel.service';
import {SettingsService} from '../../services/settings.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RateInventoryService} from '../../services/rateInventory.service';
import {LayoutUtilsService, MessageType} from '../../content/pages/components/apps/e-commerce/_core/utils/layout-utils.service';
import {FormControl, NgForm, Validators} from '@angular/forms';
import {DataSource} from '@oasisdigital/angular-material-search-select';
import {environment} from '../../../environments/environment';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TokenStorage} from '../../core/auth/token-storage.service';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {AddInquiryComponent} from '../add-inquiry/add-inquiry.component';
import {MatDialog} from '@angular/material';
import {NgxPermissionsService} from 'ngx-permissions';
import {PreviewEmailComponent} from './preview-email/preview-email.component';
import {InqDiscountApproveComponent} from '../inq-discount-approve/inq-discount-approve.component';

declare var jquery: any;
declare var $: any;

let apiURL;
let companySearchParams: any = {};

@Component({
	selector: 'm-booking',
	templateUrl: './booking.component.html',
	styleUrls: ['./booking.component.scss'],
	providers: [InquiryService, UserService, HotelService, SettingsService, RateInventoryService]
})
export class BookingComponent implements OnInit, AfterContentChecked {
	@ViewChild('bookingForm') bookingForm: NgForm;
	serverUrl = environment.Server_URL;
	sendingEmail: any = false;
	previewEmail: any = false;
	isLoading: boolean = false;
	revertSalePermission: boolean = false;
	isReleaseCancelPermission: boolean = false;
	isLoadingRate: boolean = false;
	isSaving: boolean = false;
	isChangingStatus: boolean = false;
	checkingAvailability: boolean = false;
	availableStatistic: any = [];
	isInventoryAvailable = true;
	isSettingsLoading: boolean = false;
	isVisibleCompanies: boolean = false;
	isVisibleOTA: boolean = false;
	isVisibleAgents: boolean = false;
	minInqDate: any = moment().subtract(6, 'months');
	maxDate = moment();
	minReleaseDate = moment();
	maxReleaseDate = moment();
	editorConfig: AngularEditorConfig = {
		editable: true,
		spellcheck: true,
		height: '15rem',
		minHeight: '5rem',
		placeholder: 'Enter text here...',
		translate: 'no',
		customClasses: [
			{
				name: 'quote',
				class: 'quote',
			},
			{
				name: 'redText',
				class: 'redText'
			},
			{
				name: 'titleText',
				class: 'titleText',
				tag: 'h1',
			},
		]
	};
	origBooking: any = {};
	originalBooking: any = {};
	newBooking: any = {
		inclusion: '',
		booking_rooms: [],
		extras: [],
		taxes: [],
		discount_amount: 0,
		discount_ratio: 0,
		discount_approval_percentage: 0,
		total_amount: 0,
		gross_amount: 0,
		net_amount: 0,
		room_nights: 1,
		inquiry_date: moment().format('YYYY-MM-DD'),
		check_in_date: moment().format('YYYY-MM-DD'),
		check_out_date: moment().add(1, 'days').format('YYYY-MM-DD'),
		reminders: [],
		isReadOnly: false
	};
	checkInMinDate: any = moment();
	checkOutMinDate: any = moment();
	inquirySourceList: any[] = [];
	inquiryStatusList: any[] = [];
	inquirySubStatusList: any[] = [];
	inquiryTypesList: any[] = [];
	roomTypes: any[] = [];
	mealPlans: any[] = [];
	extras: any[] = [];
	taxes: any[] = [];
	hotelList: any[] = [];
	inquiries: any[] = [];
	histories: any = [];
	travelAgents: any[] = [];
	companies: any[] = [];
	assignToUserList: any[] = [];
	tempSelectedRoom: any = {
		roomsCount: ''
	};
	currentStatus: any = {};
	currentInqType: any = {};
	currentSource: any = {};
	reminderTypes: any[] = [
		{
			name: 'Web',
			type: 'WEB'
		},
		{
			name: 'Web + Email',
			type: 'WEB+EMAIL'
		},
		{
			name: 'Web + SMS',
			type: 'WEB+SMS'
		},
		{
			name: 'Web + SMS + Email',
			type: 'WEB+SMS+EMAIL'
		}
	];

	billToOptions = [
		'Guest',
		'Company'
	];

	ota = new FormControl(null, [Validators.required]);
	otaDataSource: DataSource;

	travel_agent = new FormControl(null, [Validators.required]);
	travelAgentDataSource: DataSource;

	companyList = new FormControl(null, [Validators.required]);
	companyDataSource: DataSource;
	companyListArray: any[] = [];
	loggedInUser: any = {};
	@ViewChild('availabilityChartModal') availabilityChartModal;
	userPermissions: any = {};
	entryModalRef: any;
	waitListedInquiries: any = {};
	@ViewChild('waitListedInquiriesModel') waitListedInquiriesModel;
	// @ViewChild('waitListedInquiriesModel')
	// private modalRef: TemplateRef<any>;

	discountAutoApproveVariation: any;
	discountApprovalValidationRule: any;
	discountApprovedUsers: any[] = [];
	isSavingApprover: any = false;
	discountStatus: any = 2;
	discountApprovedBy: any = null;
	approvalStatus: any = '';
	approvedByName: any = '';
	approvedColour: any = '#fff';
	@ViewChild('discountApprovedByModal') discountApprovedByModal;

	searchTermExtras: any = '';

	currentPaymentStatus: any = {};
	inquiryPaymentStatusList: any[] = [];
	inquiryPaymentSubStatusList: any[] = [];

	// Attachment
	afuConfig: any = {};

	constructor(private inquiryService: InquiryService,
				private settingsService: SettingsService,
				private hotelService: HotelService,
				private http: HttpClient,
				private tokenStorage: TokenStorage,
				private modalService: NgbModal,
				public dialog: MatDialog,
				private permissions: NgxPermissionsService,
				private layoutUtilsService: LayoutUtilsService,
				private rateInventoryService: RateInventoryService,
				private activatedRoute: ActivatedRoute,
				private router: Router,
				private toastService: ToastrService) {

		this.userPermissions = this.permissions.getPermissions();

		this.initBooking();
		this.loggedInUser = this.tokenStorage.getUserDetail();

		apiURL = environment.API_URL + '/companies/';
		companySearchParams = {
			pageSize: '10',
			q: '',
			type: '0',
			_sort: 'name'
		};
		this.initCompanyDataSource();

		this.discountAutoApproveVariation = this.tokenStorage.getAppConfigByKey('discount_auto_approve_variation');
		this.discountApprovalValidationRule = this.tokenStorage.getAppConfigByKey('discount_approval_validation_rule');

		const pastDaysToAdd: any = this.tokenStorage.getAppConfigByKey('allow_add_past_inq_for_days');
		if (pastDaysToAdd) {
			this.minInqDate = moment().subtract(pastDaysToAdd, 'days');
		}
		this.dateChangeEvent();

		if (this.userPermissions['revert-sale']) {
			this.revertSalePermission = true;
		}
	}


	ngAfterContentChecked() {
		this.getFormValidationErrors();
	}


	ngOnInit() {
		this.getBookingSettings();
		this.loadMealPlans();
		this.loadExtras();
		this.loadTaxes();


		// this.activatedRoute
		//     .queryParams
		//     .subscribe(queryParams => {
		//         if (queryParams.iqId) {
		//             this.inquiryService.findInquiry(queryParams.iqId)
		//                 .subscribe(response => {
		//                         this.inquiry = Object.assign({}, response);
		//                         this.newBooking.check_in_date = response.check_in_date;
		//                         this.newBooking.check_out_date = response.check_out_date;
		//                         this.newBooking.hotel_id = response.hotel_id;
		//                         this.dateChangeEvent(null);
		//                     },
		//                     error => {
		//                         this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
		//                     });
		//         }
		//     });
	}

	dateChangeEvent(type: string = '') {
		switch (type) {
			case 'check_in_date':
				this.checkOutMinDate = this.newBooking[type];
				break;

			case 'inquiry_date':
				this.minReleaseDate = this.newBooking[type];
				break;
		}
		if (type) {
			this.newBooking[type] = this.newBooking[type].format('YYYY-MM-DD');
		}
		if (this.newBooking['inquiry_date']) {
			this.minReleaseDate = moment(this.newBooking['inquiry_date'], 'YYYY-MM-DD').startOf('day');
			this.checkInMinDate = moment(this.newBooking['inquiry_date'], 'YYYY-MM-DD').startOf('day');
		}
		if (this.newBooking['check_in_date'] && this.newBooking['check_out_date']) {
			const chIDate = moment(this.newBooking['check_in_date'], 'YYYY-MM-DD').startOf('day');
			this.maxReleaseDate = chIDate;
			this.checkOutMinDate = moment(this.newBooking['check_in_date'], 'YYYY-MM-DD').startOf('day').add(1, 'days');
			const chODate = moment(this.newBooking['check_out_date'], 'YYYY-MM-DD').endOf('day');
			const diff = chODate.diff(chIDate, 'days');
			this.newBooking.room_nights = diff;
			this.calculateBooking();
		}
	}

	getBookingSettings() {
		if (this.assignToUserList.length <= 0 ||
			this.inquirySourceList.length <= 0 ||
			this.inquiryStatusList.length < 1 ||
			this.inquirySubStatusList.length <= 0 ||
			this.hotelList.length <= 0 ||
			this.inquiryTypesList.length <= 0 ||
			this.travelAgents.length <= 0 ||
			this.companies.length <= 0 ||
			this.inquiryPaymentStatusList.length < 1 ||
			this.inquiryPaymentSubStatusList.length <= 0
		) {
			this.isSettingsLoading = true;
			this.inquiryService.getInquirySettingLists()
				.subscribe(response => {
						this.assignToUserList = response.users;
						this.inquirySourceList = response.sources;
						this.inquirySubStatusList = response.subStatuses;
						this.hotelList = response.hotels;
						this.inquiryTypesList = response.types;
						this.travelAgents = response.travel_agents;
						this.companies = response.companies;
						this.inquiryPaymentStatusList = response.inquiryPaymentStatuses;
						this.inquiryPaymentSubStatusList = response.inquiryPaymentSubStatuses;
						this.isSettingsLoading = false;
						this.activatedRoute.params.subscribe(params => {
							console.log(params);
							if (params['id']) {
								this.fetchBookingDetail(params['id']);
							}
						});
						this.inquiryStatusList = response.statuses;
						/*if (!this.userPermissions['release-booking']) {
							this.inquiryStatusList = response.statuses.filter(ob => {
								return (ob.name !== 'Released' || ob.name !== 'Cancelled');
							});
						}*/
					},
					error => {
						this.isSettingsLoading = false;
						this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
					});
		}

	}

	loadMealPlans() {
		// this.settingsService.getMealPlans()
		//     .subscribe(response => {
		//             this.mealPlans = response.items;
		//         },
		//         error => {
		//             this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
		//         });
	}

	loadRoomTypes() {
		this.hotelService.getHotelRoomTypes(this.newBooking.hotel_id)
			.subscribe(response => {
					this.roomTypes = response;
				},
				error => {
					this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
				});
	}

	getFilteredRatePlans(plans: any[]) {
		let ratePlans: any[];
		ratePlans = [];
		if (plans) {
			plans.forEach(pln => { // && this.newBooking.room_nights <= pln.max_nights) {
				pln.booking_types.filter(itm => { // console.log(this.newBooking.type_id + ' & ' + itm.pivot.booking_type_id);
					if (this.newBooking.room_nights >= pln.min_nights && (this.newBooking.type_id === itm.pivot.booking_type_id)) {
						if (pln.is_enabled) {
							ratePlans.push(pln);
						}
					}
				});
			});
		}

		return ratePlans;
	}

	getNoOfRoom() {
		if (this.tempSelectedRoom.room) {
			if (this.tempSelectedRoom.room.roomsCount) {
				return this.getRangeArray(this.tempSelectedRoom.room.roomsCount);
			}
		}
		return [];
	}

	getRangeArray(upto: any, from: number = 1) {
		let rangeArr: any;
		rangeArr = [];
		if (upto >= 0) {
			for (let i = from; i <= upto; i++) {
				rangeArr.push(i);
			}
		}
		return rangeArr;
	}

	getReminderDays(reminder) {
		let diff = 0;
		if (reminder.remind_on === 'release_date') {
			if (this.newBooking['inquiry_date'] && this.newBooking['release_date']) {
				const iqDat = moment(this.newBooking['inquiry_date'], 'YYYY-MM-DD').startOf('day');
				const relDate = moment(this.newBooking['release_date'], 'YYYY-MM-DD').endOf('day');

				diff = relDate.diff(iqDat, 'days');
			}
		} else if (this.newBooking['inquiry_date'] && this.newBooking['check_in_date']) {
			const iqDat = moment(this.newBooking['inquiry_date'], 'YYYY-MM-DD').startOf('day');
			const chIDate = moment(this.newBooking['check_in_date'], 'YYYY-MM-DD').startOf('day');

			diff = chIDate.diff(iqDat, 'days');
		}

		let rangeArr;
		rangeArr = [];
		for (let i = 0; i <= diff; i++) {
			rangeArr.push(i);
		}

		return rangeArr;
	}


	changeInquiryStatus(event) {
		this.isChangingStatus = true;
		if (this.originalBooking.status && this.originalBooking.status.is_sold === true && !this.revertSalePermission) {
			this.newBooking.status_id = this.originalBooking.status_id;
			this.toastService.warning('You don\'t have permission to update status', 'Please contact admin');
			return false;
		}
		this.setCurrentStatus();
		if (this.currentStatus.name === 'Released' || this.currentStatus.name === 'Cancelled') {
			if (!this.userPermissions['release-booking']) {
				this.newBooking.status_id = this.originalBooking.status_id;
				this.setCurrentStatus();
				this.toastService.warning('You don\'t have permission to update status', 'Please contact admin');
			}
		}
		if ((this.currentStatus.is_sold === true || this.currentStatus.is_confirmed === true)) {
			this.checkAvailability();
		}
		setInterval(() => {
			this.isChangingStatus = false;
		}, 2000);
	}

	sourceValueChanged() {
		if (this.companyList.value) {
			const compDetail = this.companyListArray.find(item => item.id === this.companyList.value);
			if (compDetail) {
				if (!this.newBooking.email && compDetail.email) {
					this.newBooking.email = compDetail.email;
				}
				if (!this.newBooking.phone && compDetail.mobile) {
					this.newBooking.phone = compDetail.mobile;
				}
			}
		}
	}

	sourceChange(companyId: any = null) {
		const source = this.inquirySourceList.find(item => item.id === this.newBooking.source_id);
		this.isVisibleCompanies = false;
		this.isVisibleOTA = false;
		this.isVisibleAgents = false;
		if (source) {
			if (source.name === 'Travel Agent') {
				this.isVisibleAgents = true;
				companySearchParams.type = 1;
				this.initCompanyDataSource();
				this.companyList.setValue(companyId);
			} else if (source.name === 'Company') {
				this.isVisibleCompanies = true;
				companySearchParams.type = 0;
				this.initCompanyDataSource();
				this.companyList.setValue(companyId);
			} else if (source.name === 'OTA') {
				this.isVisibleOTA = true;
				companySearchParams.type = 2;
				this.initCompanyDataSource();
				this.companyList.setValue(companyId);
			} else {
				this.newBooking.company_id = null;
			}
		} else {
			this.newBooking.company_id = null;
		}
		this.setCurrentSource();
	}

	setCurrentInqType() {
		this.currentInqType = this.inquiryTypesList.find(item => item.id === this.newBooking.type_id);
	}

	setCurrentStatus() {
		this.currentStatus = this.inquiryStatusList.find(item => item.id === this.newBooking.status_id);

		/* Disable Lost status when Original status is Hold */
		if (this.currentStatus.name === 'Hold' ||
			(this.currentStatus.name === 'Hold' && this.originalBooking.id &&
				(this.originalBooking.status_id === this.currentStatus.id))) {
			const lostStatus = this.inquiryStatusList.find(item => item.name === 'Lost');
			lostStatus.isDisabled = true;
		} else {
			const lostStatus = this.inquiryStatusList.find(item => item.name === 'Lost');
			lostStatus.isDisabled = false;
		}
		console.log(this.currentStatus);
	}

	setCurrentSource() {
		this.currentSource = this.inquirySourceList.find(item => item.id === this.newBooking.source_id);
		if (this.currentSource && this.currentSource.name !== 'Referral') {
			this.newBooking.referral = '';
		}
	}

	filterSubStatus() {
		let result: any;
		result = [];
		this.inquirySubStatusList.forEach(stat => {
			if (stat.status_id === this.newBooking.status_id /*|| stat.status_id === null*/) {
				result.push(stat);
			}
		});

		return result;
	}

	changeInquiryPaymentStatus(event) {
		this.setCurrentPaymentStatus();
	}

	setCurrentPaymentStatus() {
		this.currentPaymentStatus = this.inquiryPaymentStatusList.find(item => item.id === this.newBooking.payment_status_id);
	}

	filterPaymentSubStatus() {
		let result: any;
		result = [];
		this.inquiryPaymentSubStatusList.forEach(stat => {
			if (stat.status_id === this.newBooking.payment_status_id /*|| stat.status_id === null*/) {
				result.push(stat);
			}
		});

		return result;
	}

	addRoomToBooking() {

		const isAdded = this.newBooking.booking_rooms.find(item =>
			(item.room_type_id === this.tempSelectedRoom.room.id &&
				item.meal_plan_id === this.tempSelectedRoom.rate_plan.id &&
				item.rate_plan_id === this.tempSelectedRoom.meal_plan.id));
		if (isAdded) {
			this.layoutUtilsService.showActionNotification('Room already added', MessageType.Update);
			return;
		}
		this.isLoadingRate = true;
		this.rateInventoryService.fetchRateForBooking({
			hotel_id: this.newBooking.hotel_id,
			room_type_id: this.tempSelectedRoom.room.id,
			meal_plan_id: this.tempSelectedRoom.meal_plan.id,
			rate_plan_id: this.tempSelectedRoom.rate_plan.id,
			check_in_date: this.newBooking.check_in_date
		})
			.subscribe(response => {
					let roomObj: any;

					roomObj = {
						room: this.tempSelectedRoom.room,
						room_type_id: this.tempSelectedRoom.room.id,
						rate_plan_id: this.tempSelectedRoom.rate_plan.id,
						rate_plan: this.tempSelectedRoom.rate_plan,
						meal_plan_id: this.tempSelectedRoom.meal_plan.id,
						meal_plan: this.tempSelectedRoom.meal_plan,
						rooms: this.tempSelectedRoom.selectedRooms,
						hotel_id: this.newBooking.hotel_id,
						adult: parseInt(this.tempSelectedRoom.rate_plan.base_adult) * this.tempSelectedRoom.selectedRooms,
						child: parseInt(this.tempSelectedRoom.rate_plan.base_child) * this.tempSelectedRoom.selectedRooms,
						rateInventory: (response) ? response : {},
						roomsCount: this.tempSelectedRoom.room.roomsCount,

						rack_rate: parseFloat(this.tempSelectedRoom.rate_plan.rack_rate),
						extra_child_rate: parseFloat(this.tempSelectedRoom.rate_plan.extra_child_rate),
						org_child_rate: parseFloat(this.tempSelectedRoom.rate_plan.extra_child_rate),
						extra_adult_rate: parseFloat(this.tempSelectedRoom.rate_plan.extra_adult_rate),
						org_adult_rate: parseFloat(this.tempSelectedRoom.rate_plan.extra_adult_rate),

						roomTotalAmount: 0,

						// Rooms Array
						child_array: this.getRangeArray(this.tempSelectedRoom.rate_plan.max_child),
						adult_array: this.getRangeArray(this.tempSelectedRoom.rate_plan.max_adult),
						total_room_array: this.getRangeArray(this.tempSelectedRoom.room.roomsCount),
						isExtraAdults: false,
						isExtraChild: false,

					};

					// if (roomObj.meal_plan && roomObj.meal_plan.pivot) {
					//     roomObj.meal_plan.differential_price = roomObj.meal_plan.pivot.differential_price;
					// }

					console.log(roomObj);

					if (response.id) {
						roomObj.rack_rate = parseFloat(response.rate);
						roomObj.plan_rate = parseFloat(response.rate);
						roomObj.org_rack_rate = parseFloat(response.rate);
						roomObj.extra_child_rate = parseFloat(response.child_rate);
						roomObj.extra_adult_rate = parseFloat(response.adult_rate);
						roomObj.org_child_rate = parseFloat(response.child_rate);
						roomObj.org_adult_rate = parseFloat(response.adult_rate);
					}


					/* Original Rate (For Calculate Discountr Purpose) */

					roomObj.plan_rate = roomObj.rack_rate;

					roomObj.org_adult_rate = roomObj.extra_adult_rate;
					roomObj.org_child_rate = roomObj.extra_child_rate;

					roomObj.plan_base_adult = roomObj.rate_plan.base_adult;
					roomObj.plan_max_adults = roomObj.rate_plan.max_adult;
					roomObj.plan_base_child = roomObj.rate_plan.base_child;
					roomObj.plan_max_child = roomObj.rate_plan.max_child;

					this.newBooking.booking_rooms.push(Object.assign({}, roomObj));
					console.log(this.newBooking.booking_rooms);

					this.tempSelectedRoom = {
						selectedRooms: 1
					};

					this.calculateBooking();
					this.isLoadingRate = false;
				},
				error => {
					this.isLoadingRate = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
				});
	}

	calculateDiscounts(room: any) {
		if (room.discountPercent > 0) {
			if (room.rate_plan.is_calc_by_person) {
				let discountAmt;
				discountAmt = (room.org_adult_rate * room.discountPercent) / 100;
				room.extra_adult_rate = room.org_adult_rate - discountAmt;

				discountAmt = (room.org_child_rate * room.discountPercent) / 100;
				room.extra_child_rate = room.org_child_rate - discountAmt;

			} else {
				let roomChargesAmount;
				roomChargesAmount = (room.plan_rate * room.discountPercent) / 100;
				room.rack_rate = room.plan_rate - roomChargesAmount;
			}
		} else {
			room.rack_rate = room.plan_rate;
			room.extra_adult_rate = room.org_adult_rate;
			room.extra_child_rate = room.org_child_rate;
		}
		this.calculateBooking();
	}

	calculateBooking() {
		let totalAmount: number;
		let extraChargeAmount: number;
		totalAmount = 0;
		let taxAmount = 0;
		extraChargeAmount = 0;
		let roomIndex = 0;
		let discountAmount = 0;
		let maxRoomRate = 0;
		this.newBooking.adults = 0;
		this.newBooking.childs = 0;
		this.newBooking.rooms = 0;
		this.newBooking.offer_rate = 0;

		this.newBooking.booking_rooms.forEach(room => {
			room.isExtraChild = false;
			room.isExtraAdults = false;
			room.isExtraChild = false;
			room.extra_child = 0;
			room.extra_adult = 0;
			room.rack_rate = parseFloat(room.rack_rate);
			room.rooms = parseInt(room.rooms);
			room.discount = 0;
			room.discountPercent = 0;
			room.extra_child_amount = 0;
			room.extra_adult_amount = 0;
			room.extra_child_discount = 0;
			room.extra_adult_discount = 0;
			room.extra_child_discount = 0;
			room.extra_adult_discount = 0;
			if (!room.rack_rate) {
				room.rack_rate = 0;
			}
			if (!room.extra_child_rate) {
				room.extra_child_rate = 0;
			}
			if (!room.extra_adult_rate) {
				room.extra_adult_rate = 0;
			}

			if ((roomIndex === 0 && room.rate_plan && !this.newBooking.inclusion) || room.rate_plan.inclusion === '<div><br></div>') {
				this.newBooking.inclusion = room.rate_plan.inclusion;
			}

			room.child_array = this.getRangeArray(room.rate_plan.max_child * room.rooms, 0);
			room.adult_array = this.getRangeArray(room.rate_plan.max_adult * room.rooms, 0);
			room.total_room_array = this.getRangeArray(room.roomsCount);

			// room.extra_child_rate = room.rate_plan.extra_child_rate;
			// room.extra_adult_rate = room.rate_plan.extra_adult_rate;

			const extraAdultCharges = 0;
			const extraNights = this.newBooking.room_nights - room.rate_plan.min_nights;
			let perNightRate = room.rack_rate / room.rate_plan.min_nights;
			const planUseTimes = Math.floor(this.newBooking.room_nights / room.rate_plan.min_nights);
			const extendNights = this.newBooking.room_nights % room.rate_plan.min_nights;

			if (room.rate_plan.is_calc_by_person) {
				let adultAmt = 0;
				let childAmt = 0;

				adultAmt = ((room.extra_adult_rate / room.rate_plan.min_nights) * room.adult) * this.newBooking.room_nights;
				const actAdultAmt = ((room.org_adult_rate / room.rate_plan.min_nights) * room.adult) * this.newBooking.room_nights;

				const actChildAmt = ((room.org_child_rate / room.rate_plan.min_nights) * room.child) * this.newBooking.room_nights;
				childAmt = ((room.extra_child_rate / room.rate_plan.min_nights) * room.child) * this.newBooking.room_nights;

				room.amount = adultAmt + childAmt;
				room.discount = (actChildAmt + actAdultAmt) - room.amount;
				room.extra_child_discount = 0;
				room.extra_adult_discount = 0;

				perNightRate = (room.amount / room.rooms) / (this.newBooking.room_nights);
			} else {

				// Extra Child/Adult Calculation
				this.calculationChildAmount(room);
				this.calculationAdultAmount(room);

				if (extraNights > 0) {
					if (planUseTimes > 0) {
						room.amount = (room.rack_rate * room.rooms) * planUseTimes;
					}
					if (extendNights > 0) {
						room.amount += (room.rooms) * (extendNights * perNightRate);
					}
					const orgPerNightRate = room.plan_rate / room.rate_plan.min_nights;
					room.discount = ((orgPerNightRate * extendNights) + (room.plan_rate * planUseTimes) * room.rooms)
						- room.amount;

				} else {
					room.amount = (room.rack_rate * room.rooms);
					room.discount = (room.plan_rate * room.rooms) - room.amount;
				}
			}

			discountAmount += room.extra_child_discount;
			discountAmount += room.extra_adult_discount;

			if (maxRoomRate < perNightRate) {
				maxRoomRate = perNightRate;
			}

			room.discountPercent = this.getDiscountRate(room.discount, room.amount);
			discountAmount += room.discount;

			totalAmount += (room.amount + room.extra_adult_amount + room.extra_child_amount);

			this.newBooking.rooms += room.rooms;
			this.newBooking.childs += room.child;
			this.newBooking.adults += room.adult;

			roomIndex++;

		});

		this.newBooking.extras.forEach(chrg => {
			if (!chrg.price) {
				chrg.price = 0;
			}
			chrg.amount = parseFloat(chrg.price) * parseFloat(chrg.qty);
			chrg.discount_amount = (parseFloat(chrg.original_price) * parseFloat(chrg.qty)) - chrg.amount;

			discountAmount += chrg.discount_amount;
			extraChargeAmount += chrg.amount;
		});

		this.newBooking.taxes.forEach(chrg => {
			if (chrg.type === 'Flat') {
				chrg.amount = chrg.value;
				taxAmount += parseFloat(chrg.amount);
			} else if (chrg.type === 'Range') {
				chrg.value = null;
				chrg.ranges.forEach(rng => {
					if (maxRoomRate >= rng.from_amount && maxRoomRate <= rng.to_amount) {
						chrg.value = rng.value;
					}
				});
				// if (!chrg.value) {
				//     this.removeTax(chrg);
				// }
				chrg.amount = (totalAmount * parseFloat(chrg.value)) / 100;
				taxAmount += parseFloat(chrg.amount);
			} else {
				chrg.amount = (totalAmount * parseFloat(chrg.value)) / 100;
				taxAmount += chrg.amount;
			}
		});

		this.newBooking.discount_amount = discountAmount;
		this.newBooking.total_amount = totalAmount;
		this.newBooking.gross_amount = Math.round(totalAmount + extraChargeAmount);
		this.newBooking.net_amount = Math.round(totalAmount + extraChargeAmount + taxAmount);
		this.newBooking.offer_rate = this.newBooking.net_amount;
		console.log(this.newBooking);
	}

	calculationChildAmount(room) {
		const planUseTimes = Math.floor(this.newBooking.room_nights / room.rate_plan.min_nights);
		const extendNights = this.newBooking.room_nights % room.rate_plan.min_nights;

		if (room.child > (room.rate_plan.base_child * room.rooms)) {
			room.extra_child = room.child - (room.rate_plan.base_child * room.rooms);
			room.isExtraChild = true;

			const perNightChildRate = room.rate_plan.base_child / room.rate_plan.min_nights;
			if (planUseTimes > 0) {
				room.extra_child_amount = (room.extra_child_rate * room.extra_child) * planUseTimes;
			}
			if (extendNights > 0) {
				room.extra_child_amount += (room.extra_child * (extendNights * perNightChildRate));
			}

			const orgPerNightChildRate = room.org_child_rate / room.rate_plan.min_nights;
			room.extra_child_discount = ((orgPerNightChildRate * extendNights) +
				(room.org_child_rate * planUseTimes) * room.extra_child) - room.extra_child_amount;

		}
	}

	calculationAdultAmount(room) {
		const planUseTimes = Math.floor(this.newBooking.room_nights / room.rate_plan.min_nights);
		const extendNights = this.newBooking.room_nights % room.rate_plan.min_nights;
		if (room.adult > (room.rate_plan.base_adult * room.rooms)) {
			room.extra_adult = room.adult - (room.rate_plan.base_adult * room.rooms);
			room.isExtraAdults = true;

			const perNightAdultRate = room.extra_adult_rate / room.rate_plan.min_nights;
			if (planUseTimes > 0) {
				room.extra_adult_amount = (room.extra_adult_rate * room.extra_adult) * planUseTimes;
			}
			if (extendNights > 0) {
				room.extra_adult_amount += (room.extra_adult * (extendNights * perNightAdultRate));
			}

			const orgPerNightAdultRate = room.org_adult_rate / room.rate_plan.min_nights;
			room.extra_adult_discount = ((orgPerNightAdultRate * extendNights) +
				(room.org_adult_rate * planUseTimes) * room.extra_adult) - room.extra_adult_amount;
		}
	}

	removeRoomFromBooking(roomData) {
		const dialogRef = this.layoutUtilsService.deleteElement();
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			const ind = this.newBooking.booking_rooms.indexOf(roomData);
			if (ind !== false) {
				this.newBooking.booking_rooms.splice(ind, 1);
			}
			this.calculateBooking();
		});
	}

	loadExtras() {
		this.settingsService.getExtras()
			.subscribe(response => {
					this.extras = response.items;
				},
				error => {
					this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
				});
	}


	loadTaxes() {
		this.settingsService.getTaxes({apply_from: moment().format('YYYY-MM-DD'), is_enabled: 1})
			.subscribe(response => {
					this.taxes = response.items;

					this.taxes.forEach(tx => {
						if (tx.is_auto_add) {
							this.addTaxBooking(tx);
						}
					});
				},
				error => {
					this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
				});
	}

	addExtraToBooking(record) {
		const isExistCharge = this.newBooking.extras.find(item => item.extra_id === record.id);
		if (!isExistCharge) {
			let newItem: any;
			newItem = Object.assign({
				original_price: record.price,
				qty: 1,
				extra_id: record.id,
				id: ''
			}, record);
			newItem.id = '';
			this.newBooking.extras.push(newItem);
			this.calculateBooking();
		}
	}

	removeExtraCharge(charge) {
		const isExistCharge = this.newBooking.extras.find(item => item.extra_id === charge.extra_id);
		if (isExistCharge) {
			this.newBooking.extras.splice(this.newBooking.extras.indexOf(isExistCharge), 1);
			this.calculateBooking();
		}
	}


	openModel(modal) {
		this.modalService.open(modal, {size: 'lg', centered: true});
	}

	addTaxBooking(record) {
		const isExistCharge = this.newBooking.taxes.find(item => item.tax_id === record.id);
		if (!isExistCharge) {
			let newItem: any;
			newItem = Object.assign({amount: 0, tax_id: record.id, id: ''}, record);
			newItem.id = '';
			newItem.tax_id = record.id;
			this.newBooking.taxes.push(newItem);
			this.calculateBooking();
		}
	}

	removeTax(charge) {
		const isExistCharge = this.newBooking.taxes.find(item => item.tax_id === charge.tax_id);
		if (isExistCharge) {
			this.newBooking.taxes.splice(this.newBooking.taxes.indexOf(isExistCharge), 1);
			this.calculateBooking();
		}
	}

	addReminderToBooking() {
		this.newBooking.reminders.push({
			days: 1,
			type: 'WEB',
			remind_on: 'check_in_date'
		});
	}

	removeReminder(reminder) {
		const ind = this.newBooking.reminders.indexOf(reminder);
		if (ind !== false) {
			this.newBooking.reminders.splice(ind, 1);
		}
	}


	getAvailableTaxes() {
		let taxes: any[];
		taxes = [];

		let maxRoomRate = 0;
		this.newBooking.booking_rooms.forEach(room => {
			const perNightRate = room.rack_rate / room.rate_plan.min_nights;
			if (maxRoomRate < perNightRate) {
				maxRoomRate = perNightRate;
			}
		});
		this.taxes.forEach(tx => {
			if (tx.type === 'Range') {
				let matchRule;
				matchRule = false;
				tx.ranges.forEach(rng => {
					if (maxRoomRate >= rng.from_amount && maxRoomRate <= rng.to_amount) {
						tx.value = rng.value;
						matchRule = true;
					}
				});
				if (matchRule === true) {
					taxes.push(tx);
				}
			} else {
				taxes.push(tx);
			}
		});

		return taxes;
	}

	getDiscountRate(discAmt, totalAmt, isTotalAfterDisc: boolean = true) {
		let discPercent: number;
		if (isTotalAfterDisc) {
			const total: number = (parseFloat(totalAmt) + parseFloat(discAmt));
			discPercent = (parseFloat(discAmt) * 100) / total;
		} else {
			discPercent = (parseFloat(discAmt) * 100) / parseFloat(totalAmt);
		}
		return discPercent.toFixed(2);
	}

	sendDetailMail(id) {
		this.sendingEmail = true;
		this.inquiryService.sendDetailMail(id, {type: 'updated_booking_detail'})
			.subscribe(response => {
					this.sendingEmail = false;
					this.toastService.success('Mail Send Success', 'Success');
				},
				error => {
					this.sendingEmail = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}

	previewDetailMail(id) {
		this.previewEmail = true;
		this.inquiryService.previewDetailMail(id, {type: 'updated_booking_detail'})
			.subscribe(response => {
					this.previewEmail = false;
					this.openPreviewMailDetailForm(response);
					/*setTimeout(() => {
						this.initBooking();
						this.router.navigate(['/booking']);
					}, 2500);*/
				},
				error => {
					this.previewEmail = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}

	openPreviewMailDetailForm(detail: any = {}) {
		const dialogRef = this.dialog.open(PreviewEmailComponent, {
			width: '900px',
			data: {
				record: detail,
			}
		});
		dialogRef.afterClosed().subscribe(result => {
			if (result && result === 'sendEmail') {
				this.sendDetailMail(detail.id);
			}
			this.router.navigate(['/booking']);
		});
	}

	openPendingApprovalForm(record) {
		const dialogRef = this.dialog.open(InqDiscountApproveComponent, {
			width: '400px',
			data: {
				bookingDetail: record
			}
		});

		dialogRef.afterClosed().subscribe(result => {
			record.selected = false;
			if (result) { // console.log('Discount Status Updated!');
				this.discountStatus = result.discount_status;
				this.setDiscountApprovalStatusColor();
				this.router.navigate(['/']);
			}
		});
	}

	getOrigBookingDiscRateForApproval() {
		let discount = 0;
		if (this.origBooking && (Object.keys(this.origBooking).length === 0)) {
			return discount;
		}
		if (this.discountApprovalValidationRule === 'by_each_item') {
			let maxDiscRooms = 0;
			let maxDiscExtras = 0;

			this.origBooking.booking_rooms.forEach(room => {
				const roomDisc = parseFloat(this.getDiscountRate(room.discount, room.amount));
				if (roomDisc > maxDiscRooms) {
					maxDiscRooms = roomDisc;
				} // console.log(roomDisc);
				const extraChildDisc = parseFloat(this.getDiscountRate(room.extra_child_discount, room.extra_child_amount));
				if (extraChildDisc > maxDiscRooms) {
					maxDiscRooms = extraChildDisc;
				} // console.log(extraChildDisc);
				const extraAdultDisc = parseFloat(this.getDiscountRate(room.extra_adult_discount, room.extra_adult_amount));
				if (extraAdultDisc > maxDiscRooms) {
					maxDiscRooms = extraAdultDisc;
				} // console.log(extraAdultDisc);
			});

			this.origBooking.extras.forEach(chrg => {
				const extraDisc = parseFloat(this.getDiscountRate(chrg.discount_amount, chrg.amount));
				if (extraDisc > maxDiscExtras) {
					maxDiscExtras = extraDisc;
				} // console.log(extraDisc);
			});

			discount = maxDiscRooms > maxDiscExtras ? maxDiscRooms : maxDiscExtras;
		} else {
			discount = parseFloat(this.getDiscountRate(this.origBooking.discount_amount, this.origBooking.gross_amount));
		}
		return discount;
	}

	getNewBookingDiscRateForApproval() {
		let discount = 0;
		if (this.discountApprovalValidationRule === 'by_each_item') {
			let maxDiscRooms = 0;
			let maxDiscExtras = 0;

			this.newBooking.booking_rooms.forEach(room => {
				const roomDisc = parseFloat(this.getDiscountRate(room.discount, room.amount));
				if (roomDisc > maxDiscRooms) {
					maxDiscRooms = roomDisc;
				} // console.log(roomDisc);
				const extraChildDisc = parseFloat(this.getDiscountRate(room.extra_child_discount, room.extra_child_amount));
				if (extraChildDisc > maxDiscRooms) {
					maxDiscRooms = extraChildDisc;
				} // console.log(extraChildDisc);
				const extraAdultDisc = parseFloat(this.getDiscountRate(room.extra_adult_discount, room.extra_adult_amount));
				if (extraAdultDisc > maxDiscRooms) {
					maxDiscRooms = extraAdultDisc;
				} // console.log(extraAdultDisc);
			});

			this.newBooking.extras.forEach(chrg => {
				const extraDisc = parseFloat(this.getDiscountRate(chrg.discount_amount, chrg.amount));
				if (extraDisc > maxDiscExtras) {
					maxDiscExtras = extraDisc;
				} // console.log(extraDisc);
			});

			discount = maxDiscRooms > maxDiscExtras ? maxDiscRooms : maxDiscExtras;
		} else {
			discount = parseFloat(this.getDiscountRate(this.newBooking.discount_amount, this.newBooking.gross_amount));
		}
		return discount;
	}

	openDiscountApproverModal() {
		let reqParams;
		this.isSaving = true;
		this.isSavingApprover = true;
		this.newBooking.remark = '';
		const discount = this.getNewBookingDiscRateForApproval();
		if (discount > 0) {

			reqParams = {
				discount: discount,
				hotel_id: this.newBooking.hotel_id,
				booking_type_id: this.newBooking.type_id,
				booking_status_id: this.newBooking.status_id,
				assign_to_id: this.newBooking.assign_to_id,
				source_id: this.newBooking.source_id,
			};

			this.inquiryService.getDiscountUsers(reqParams)
				.subscribe(response => {
						this.isSaving = false;
						const user = response.users.filter(ob => ob['id'] === this.newBooking.assign_to_id);

						if (user.length < 1 && response.rules_matched) { // console.log(response.users);
							this.discountApprovedUsers = response.users;
							this.modalService.open(this.discountApprovedByModal, {size: 'lg', centered: true}).result.then((result) => {
								this.discountStatus = 1;
								this.newBooking.discount_status = 1;
								this.newBooking.discount_approved_by = this.discountApprovedBy;
								this.newBooking.discount_ratio = parseFloat(
									this.getDiscountRate(this.newBooking.discount_amount, this.newBooking.gross_amount)
								);
								this.newBooking.discount_approval_percentage = discount;
								this.saveBooking();
							}, (reason) => { // console.log(reason);
								this.discountApprovedBy = this.originalBooking.discount_approved_by;
							});
						} /*else { this.saveBooking(); }*/
					},
					error => {
						this.isSaving = false;
						this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
					});
		} /*else { this.saveBooking(); }*/
	}

	validateAndSaveBooking(sendMailAfterSave: boolean = false) {
		let reqParams;
		this.isSaving = true;
		this.newBooking.remark = '';
		const origDisc = this.getOrigBookingDiscRateForApproval();
		const discount = this.getNewBookingDiscRateForApproval();
		if (discount > 0) {

			reqParams = {
				discount: discount,
				hotel_id: this.newBooking.hotel_id,
				booking_type_id: this.newBooking.type_id,
				booking_status_id: this.newBooking.status_id,
				assign_to_id: this.newBooking.assign_to_id,
				source_id: this.newBooking.source_id,
			};

			this.inquiryService.getDiscountUsers(reqParams)
				.subscribe(response => {
						this.isSaving = false;
						this.newBooking.discount_ratio = parseFloat(
							this.getDiscountRate(this.newBooking.discount_amount, this.newBooking.gross_amount)
						);
						this.newBooking.discount_approval_percentage = discount;
						const user = response.users.filter(ob => ob['id'] === this.newBooking.assign_to_id);

						if (this.newBooking.discount_status === 1 && this.newBooking.discount_approved_by && this.newBooking.id) {
							const discountApprovePerson = response.users.find(ob => ob['id'] === this.newBooking.discount_approved_by);
							if (discountApprovePerson) {
								this.newBooking.discount_status = this.discountStatus;
								this.newBooking.discount_approved_by = this.discountApprovedBy;
								this.saveBooking(sendMailAfterSave);
								return;
							} /*else {
                                this.discountStatus = 1;
                                this.discountApprovedBy = null;
                            }*/
						}

						if (user.length < 1 && response.rules_matched) {
							const discWithVariation = origDisc + +this.discountAutoApproveVariation;
							if (this.newBooking.discount_approved_by && response.users.length > 0 &&
								discount <= discWithVariation && this.newBooking.discount_status === 2 &&
								response.users.find(ob => ob['id'] === this.newBooking.discount_approved_by)
							) {
								this.saveBooking(sendMailAfterSave);
							} else {
								if (this.discountApprovedUsers.length <= 0) {
									this.discountApprovedBy = null;
								}
								this.discountApprovedUsers = response.users;
								this.modalService.open(this.discountApprovedByModal, {size: 'lg', centered: true}).result.then((result) => {
									if (this.newBooking.discount_status !== 3 || (
										this.newBooking.discount_status === 3 && this.newBooking.resend_approval)) {
										this.discountStatus = 1;
										this.newBooking.discount_status = 1;
										this.newBooking.discount_approved_by = this.discountApprovedBy;
									}
									this.saveBooking(sendMailAfterSave);
								}, (reason) => {
									console.log(reason);
								});
							}
						} else {
							this.saveBooking(sendMailAfterSave);
						}
					},
					error => {
						this.isSaving = false;
						this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
					});
		} else {
			this.saveBooking(sendMailAfterSave);
		}
	}

	saveBooking(sendMailAfterSave: boolean = false) {
		this.isSaving = true;
		this.newBooking.company_id = null;
		if (this.isVisibleCompanies || this.isVisibleOTA || this.isVisibleAgents) {
			this.newBooking.company_id = this.companyList.value;
		}

		this.inquiryService.saveBooking(this.newBooking)
			.subscribe(response => {
					this.isSaving = false;
					this.toastService.success('Saved Successfully', 'Done');
					if (sendMailAfterSave) {
						this.previewDetailMail(this.newBooking.id);
					}
					if (this.currentStatus.is_lost === true) {
						this.checkWaitListedAvailability();
					}
					if (this.bookingForm.valid) {
						this.initBooking();
					} else {
						this.isSavingApprover = false;
						this.assignToUserList.forEach(itm => {
							// tslint:disable-next-line:radix
							if (itm.id === parseInt(this.discountApprovedBy)) {
								this.approvedByName = itm.name;
							}
						});

						this.setDiscountApprovalStatusColor();
					}
					// this.router.navigate(['/booking']);
				},
				error => {
					this.isSaving = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
				});
	}

	checkWaitListedAvailability() {
		let reqParams;

		let waitListedStatusIds;
		waitListedStatusIds = [];
		this.inquiryStatusList.forEach(itm => {
			if (itm.is_waitlisted === true) {
				waitListedStatusIds.push(itm.id);
			}
		});

		reqParams = {
			id: (this.newBooking.id) ? this.newBooking.id : '',
			hotel_id: this.newBooking.hotel_id,
			check_in_date_fromDate: this.newBooking.check_in_date,
			check_in_date_toDate: this.newBooking.check_out_date,
			check_out_date_fromDate: this.newBooking.check_in_date,
			check_out_date_toDate: this.newBooking.check_out_date,
			room_types: [],
			status_id: waitListedStatusIds,
		};
		this.inquiryService.showWaitListedInquiries(reqParams);
	}

	fetchBookingDetail(id) {
		this.isLoading = true;
		this.inquiryService.findBooking(id)
			.subscribe(response => {
					console.log(response);
					this.newBooking = response;
					this.setCurrentStatus();
					this.setCurrentSource();
					this.setCurrentInqType();
					this.sourceChange(this.newBooking.company_id);
					this.loadRoomTypes();
					this.originalBooking = Object.assign({}, this.newBooking);
					this.origBooking = JSON.parse(JSON.stringify(this.newBooking));
					this.originalBooking.status = this.currentStatus;
					this.checkInMinDate = moment(this.newBooking['check_in_date'], 'YYYY-MM-DD');
					this.minInqDate = moment(this.newBooking['inquiry_date'], 'YYYY-MM-DD');
					this.checkOutMinDate = this.checkInMinDate;
					this.isLoading = false;
					this.dateChangeEvent();
					this.calculateBooking();
					this.originalBooking.gross_amount = this.newBooking.gross_amount;
					this.origBooking.gross_amount = this.newBooking.gross_amount;
					this.discountStatus = this.newBooking.discount_status;
					this.discountApprovedBy = this.newBooking.discount_approved_by;

					this.assignToUserList.forEach(itm => {
						if (itm.id === this.discountApprovedBy) {
							this.approvedByName = itm.name;
						}
					});

					this.setDiscountApprovalStatusColor();

					if (this.userPermissions['inquiries-update-responsible']) {
						if (this.newBooking.assign_to_id !== this.loggedInUser.id) {
							this.newBooking.isReadOnly = true;
							this.travel_agent.disable();
							this.companyList.disable();
						}
					} else if (!this.userPermissions['inquiries-update']) {
						this.newBooking.isReadOnly = true;
						this.travel_agent.disable();
						this.companyList.disable();
					}

					this.initAttachmentConfig();
				},
				error => {
					this.isLoading = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
				});
	}

	setDiscountApprovalStatusColor() {
		const arrDiscountStatus = {1: 'Pending', 2: 'Approved', 3: 'Rejected'};
		this.approvalStatus = arrDiscountStatus[this.discountStatus];

		switch (this.discountStatus) {
			case 1:
				this.approvedColour = '#F4B400';
				break;
			case 2:
				this.approvedColour = '#37A282';
				break;
			case 3:
				this.approvedColour = '#FF729F';
				break;
		}

		this.inquiryStatusList.forEach(itm => {
			if (itm.name === 'Hold' && arrDiscountStatus[this.discountStatus] === 'Pending') {
				this.approvedColour = itm.color_code;
			} else if (itm.name === 'Confirm' === true && arrDiscountStatus[this.discountStatus] === 'Approved') {
				this.approvedColour = itm.color_code;
			} else if (itm.name === 'Lost' && arrDiscountStatus[this.discountStatus] === 'Rejected') {
				this.approvedColour = itm.color_code;
			}
		});

	}

	initBooking() {
		this.newBooking = {
			inclusion: '',
			booking_rooms: [],
			extras: [],
			taxes: [],
			discount_amount: 0,
			discount_ratio: 0,
			discount_approval_percentage: 0,
			total_amount: 0,
			net_amount: 0,
			room_nights: 1,
			inquiry_date: moment().format('YYYY-MM-DD'),
			check_in_date: moment().format('YYYY-MM-DD'),
			check_out_date: moment().add(1, 'days').format('YYYY-MM-DD'),
			reminders: [],
			discount_status: 2,
			discount_approved_by: null
		};
		this.discountStatus = 2;
		this.discountApprovedBy = null;
		this.approvalStatus = '';
		this.approvedByName = '';
		this.approvedColour = '#fff';
	}

	getFormValidationErrors() {
		const controls = this.bookingForm.controls;
		if (this.bookingForm.invalid) {
			Object.keys(controls).forEach(controlName => {
				controls[controlName].markAsTouched();
			});
			return;
		}
	}

	openInquiryForm(currentRecord: any = {}, params: any = {}) {
		this.newBooking.status = this.currentStatus;
		const dialogRef = this.dialog.open(AddInquiryComponent, {
			data: {
				statuses: this.inquiryStatusList,
				sources: this.inquirySourceList,
				assignToList: this.assignToUserList,
				subStatuses: this.inquirySubStatusList,
				types: this.inquiryTypesList,
				hotels: this.hotelList,
				travel_agents: this.travelAgents,
				companies: this.companies,
				paymentStatuses: this.inquiryPaymentStatusList,
				paymentSubStatuses: this.inquiryPaymentSubStatusList,
				currentRecord: this.newBooking,
				originalBooking: this.originalBooking,
				params: params
			}
		});
	}

	loadInquiryHistory() {
		this.inquiryService.getHistory(this.newBooking.id)
			.subscribe(response => {
					this.histories = response;
				},
				error => {
					this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
				});
	}

	openHistory(content) {
		this.loadInquiryHistory();
		this.modalService.open(content).result.then((result) => {
		}, (reason) => {
		});
	}

	checkAvailability(isOpenInventoryWindow: boolean = false) {
		let reqParams;
		this.isInventoryAvailable = true;

		const chInDate = moment(this.newBooking.check_in_date, 'YYYY-MM-DD').startOf('day');
		const chOutDate = moment(this.newBooking.check_out_date, 'YYYY-MM-DD').endOf('day');

		if (chOutDate <= chInDate) {
			this.toastService.error('Check out date should be greater then Check in date');
			return;
		}

		reqParams = {
			id: (this.newBooking.id && !isOpenInventoryWindow) ? this.newBooking.id : '',
			hotel_id: this.newBooking.hotel_id,
			check_in_date: this.newBooking.check_in_date,
			check_out_date: this.newBooking.check_out_date,
			room_types: []
		};

		let diff: number = 0;
		if (isOpenInventoryWindow) {
			diff = chInDate.diff(moment().startOf('day'), 'days');
			if (diff >= 1) {
				reqParams.check_in_date = chInDate.subtract((diff === 1) ? 1 : 2, 'days').format('YYYY-MM-DD');
			}
			reqParams.check_out_date = chOutDate.add(2, 'days').format('YYYY-MM-DD');
		}

		this.newBooking.booking_rooms.forEach(room => {
			const existRoomType = reqParams.room_types.find(item =>
				item.room_type_id === room.room_type_id);
			if (existRoomType) {
				existRoomType.rooms += room.rooms;
			} else {
				reqParams.room_types.push({
					room_type_id: room.room_type_id,
					rooms: room.rooms
				});
			}
		});

		this.availableStatistic = [];
		this.checkingAvailability = true;
		this.inquiryService.checkBookingAvailability(reqParams)
			.subscribe(response => {
					this.checkingAvailability = false;
					if (response) {
						let isAvalabilityOk;
						isAvalabilityOk = true;
						let invDay = 0;
						response.forEach(roomType => {
							if (this.newBooking.booking_rooms.length > 0) {
								const bookRoom = this.newBooking.booking_rooms.find(item =>
									item.room_type_id === roomType.room_type_id);
								if (bookRoom) {
									if (isOpenInventoryWindow) {
										roomType['statistic'][0]['status'] = true;
										if (diff >= 1) {
											roomType['statistic'][1]['status'] = true;
										}
										roomType['statistic'][roomType['statistic'].length - 1]['status'] = true;
										roomType['statistic'][roomType['statistic'].length - 2]['status'] = true;
									}
									this.availableStatistic.push(roomType);
									if (!roomType.status) {
										isAvalabilityOk = false;
										this.isInventoryAvailable = false;
									}


								}
							} else {
								this.availableStatistic = response;
							}
							invDay++;
						});
						if (!isAvalabilityOk || isOpenInventoryWindow) {
							this.modalService.open(this.availabilityChartModal, {size: 'lg', centered: true});
						}
						if (!isAvalabilityOk && !isOpenInventoryWindow) {
							this.newBooking.status_id = '';
						}
					}
				},
				error => {
					this.checkingAvailability = false;
					this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
				});
	}

	initCompanyDataSource() {
		const http = this.http;
		let me: any;
		me = this;
		this.companyDataSource = {
			displayValue(value: any): Observable<any | null> {
				console.log('finding display value for', value);
				if (typeof value === 'string') {
					value = parseInt(value, 10);
				}
				if (typeof value !== 'number') {
					return of(null);
				}

				return http.get<any>(apiURL + 'find/' + value).pipe(
					map(e => ({
						value: e.id,
						display: `${e.name}`,
						details: {}
					}))
				);
			},
			search(term: string): Observable<any[]> {
				companySearchParams.q = term || '';
				return http.get<any[]>(apiURL + 'search', {
					params: companySearchParams
				}).pipe(
					map(list => {
						me.companyListArray = list;
						return list.map(e => ({
							value: e.id,
							display: `${e.name}`,
							details: {}
						}));
					}));
			}
		};
	}

	initAttachmentConfig() {
		const accessToken = localStorage.getItem('accessToken');
		this.afuConfig = {
			multiple: false,
			formatsAllowed: '.jpg,.png,.pdf,.jpeg',
			maxSize: '2',
			uploadAPI: {
				url: this.inquiryService.getAttachmentInvoiceUrl(this.newBooking.id),
				headers: {
					'Authorization': 'Bearer ' + accessToken
				}
			},
			theme: 'attachPin',
			hideProgressBar: true,
			hideResetBtn: true,
			hideSelectBtn: true,
			attachPinText: ' '
		};
	}

	uploadInvoice(response) {
		if (response.status === 200) {
			const invoiceResp = JSON.parse(response.response);
			this.newBooking.invoice_url = invoiceResp.invoice_url;
		}
	}

	removeBookingInvoice() {
		const dialogRef = this.layoutUtilsService.deleteElement('Delete Confirm', 'Are you sure to delete this ?');
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}
			this.inquiryService.deleteBookingInvoice(this.newBooking.id)
				.subscribe(response => {
						this.toastService.success('Deleted Successfully', 'Done');
						this.newBooking.invoice_url = response.invoice_url;
					},
					error => {
						this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
					});
		});
	}

	getFileName(fullPath) {
		return fullPath.replace(/^.*[\\\/]/, '');
	}

	isCheckedOut() {
		const chOut = moment(this.newBooking['check_out_date'], 'YYYY-MM-DD');
		const today = moment();
		if (chOut <= today && this.currentStatus.is_sold) {
			return true;
		}
		return false;
	}
}
