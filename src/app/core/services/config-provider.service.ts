import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {TokenStorage} from '../auth/token-storage.service';
import {NgxPermissionsService} from 'ngx-permissions';

@Injectable()
export class ConfigProvider {

    private configs: any = null;
    API_URL = environment.API_URL;

    constructor(private tokenStorage: TokenStorage,
                private permissions: NgxPermissionsService,
                private http: HttpClient) {
    }

    public getAppConfig(): any {
        return this.configs;
    }

    load() {
        return new Promise((resolve, reject) => {
            try {
                let loggedInUser: any;
                loggedInUser = localStorage.getItem('user');
                try {
                    loggedInUser = (JSON.parse(loggedInUser));
                    if (loggedInUser.permissions) {
                        this.permissions.loadPermissions(loggedInUser.permissions,
                            (permissionName, permissionStore) => !!permissionStore[permissionName]);
                    }
                } catch (e) {
                }
                this.http
                    .get(this.API_URL + '/app-settings?uid=' + ((loggedInUser) ? loggedInUser.id : ''))
                    .subscribe(response => {
                            this.configs = response;
                            this.tokenStorage.setAppConfig(this.configs);
                            let user: any;
                            if (this.configs.user) {
                                user = this.configs.user;
                                this.tokenStorage.setUserAndPermissions(user);
                                try {
                                    this.permissions.loadPermissions(user['permissions'],
                                        (permissionName, permissionStore) => !!permissionStore[permissionName]);
                                } catch (error) {
                                    // await this.handleError(error);
                                }
                            }
                            resolve(true);
                        },
                        error => {
                            this.tokenStorage.clear();
                            resolve(true);
                        });
            } catch (error) {
                resolve(true);
            }
        });
    }
}
