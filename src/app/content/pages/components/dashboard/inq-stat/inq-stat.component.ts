import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {DashboardService} from '../../../../../services/dashboard.service';

@Component({
    selector: 'm-inq-stat',
    templateUrl: './inq-stat.component.html',
    styleUrls: ['./inq-stat.component.scss'],
    providers: [DashboardService]
})
export class InqStatComponent implements OnChanges, OnInit {

    @Input() fromDate: any;
    @Input() toDate: any;
    isLoadingInqStat: boolean = false;
    inqStatistics: any = [];
    chartSettings: any = {fromDate: '', toDate: ''};

    constructor(private toastService: ToastrService,
                private dashboardService: DashboardService) {
    }


    ngOnInit() {
        // this.loadDashboardStatistic();
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log(changes);
        let dateChanged = false;
        if (changes.fromDate) {
            this.chartSettings.fromDate = changes.fromDate.currentValue;
            dateChanged = true;
        }

        if (changes.toDate) {
            this.chartSettings.toDate = changes.toDate.currentValue;
            dateChanged = true;
        }

        if (dateChanged) {
            this.loadDashboardStatistic();
        }
    }

    loadDashboardStatistic() {
        this.isLoadingInqStat = true;
        this.dashboardService.inquiryStat(this.chartSettings)
            .subscribe(response => {
                    this.isLoadingInqStat = false;
                    this.inqStatistics = response;
                    console.log(this.inqStatistics);
                },
                error => {
                    this.isLoadingInqStat = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }
}
