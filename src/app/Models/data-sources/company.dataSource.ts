import {finalize, tap} from 'rxjs/operators';
import {BaseDataSource} from '../../content/pages/components/apps/e-commerce/_core/models/data-sources/_base.datasource';
import {QueryParamsModel} from '../../content/pages/components/apps/e-commerce/_core/models/query-models/query-params.model';
import {CompanyService} from '../../services/company.service';

export class CompanyDataSource extends BaseDataSource {

    constructor(private companyService: CompanyService) {
        super();
    }

    loadCompanies(queryParams: QueryParamsModel) {
        this.loadingSubject.next(true);
        this.companyService.getCompanies(queryParams).pipe(
            tap(res => {
                const result = res;
                this.entitySubject.next(result.items);
                this.paginatorTotalSubject.next(result.totalCount);
            }),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe();
    }
}
