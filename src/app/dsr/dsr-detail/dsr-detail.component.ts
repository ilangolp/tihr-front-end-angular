import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TokenStorage} from '../../core/auth/token-storage.service';
import {FormControl, Validators} from '@angular/forms';
import {DataSource} from '@oasisdigital/angular-material-search-select';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {SpinnerButtonOptions} from '../../content/partials/content/general/spinner-button/button-options.interface';
import {DsrService} from '../../services/dsr.service';
import {UserService} from '../../services/user.service';
import * as moment from 'moment';
import {CommonService} from '../../services/common.service';
import {CompanyService} from '../../services/company.service';
import {NgxPermissionsService} from 'ngx-permissions';

@Component({
    selector: 'm-dsr-detail',
    templateUrl: './dsr-detail.component.html',
    styleUrls: ['./dsr-detail.component.scss'],
    providers: [DsrService, UserService, CommonService, CompanyService]
})
export class DsrDetailComponent implements OnInit {
    isSaving: boolean = false;
    assignToList: any = [];
    histories: any = [];
    visitMaxDate: any = moment().format('YYYY-MM-DD HH:mm:ss');
    countryId: number = 63; // India
    states: any[] = [];
    cities: any[] = [];
    dsrRecord: any = {
        type: 0,
        state_id: 12
    };
    spinner: SpinnerButtonOptions = {
        active: false,
        spinnerSize: 18,
        raised: true,
        buttonColor: 'primary',
        spinnerColor: 'accent',
        fullWidth: false
    };
    types: any[] = [
        {
            id: 0,
            name: 'Company'
        },
        {
            id: 1,
            name: 'Travel Agent'
        }
    ];

    companyList = new FormControl(null, [Validators.required]);
    companyDataSource: DataSource;
    travelAgentDataSource: DataSource;
    loggedInUser: any = {};
    userPermissions: any = {};

    constructor(private dialogRef: MatDialogRef<DsrDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private cdr: ChangeDetectorRef,
                private toastService: ToastrService,
                private modalService: NgbModal,
                private userService: UserService,
                private permissions: NgxPermissionsService,
                private companyService: CompanyService,
                private tokenStorage: TokenStorage,
                private http: HttpClient,
                private commonService: CommonService,
                private dsrService: DsrService) {

        this.loggedInUser = this.tokenStorage.getUserDetail();
        this.userPermissions = this.permissions.getPermissions();

        if (this.data) {
            if (this.data.setting) {
                //
            }
            if (this.data.currentRecord.id) {
                this.dsrRecord = this.data.currentRecord;
                this.dsrRecord.type = parseInt(this.dsrRecord.type);
                this.companyList.setValue(this.dsrRecord.company_id);
                this.loadDSRHistory();

                if (this.userPermissions['dsr-update-responsible']) {
                    if (this.dsrRecord.assign_to_id !== this.loggedInUser.id) {
                        this.dsrRecord.isReadOnly = true;
                        this.companyList.disable();
                    }
                } else if (!this.userPermissions['dsr-update']) {
                    this.dsrRecord.isReadOnly = true;
                    this.companyList.disable();
                }

            } else {
                this.dsrRecord = {
                    assign_to_id: this.loggedInUser.id,
                    type: 0,
                    state_id: 12,
                    visited_date: moment().format('YYYY-MM-DD'),
                    visited_time: moment().format('LT')
                };
            }
        }

        const apiURL = environment.API_URL + '/companies/';
        this.companyDataSource = {
            displayValue(value: any): Observable<any | null> {
                console.log('finding display value for', value);
                if (typeof value === 'string') {
                    value = parseInt(value, 10);
                }
                if (typeof value !== 'number') {
                    return of(null);
                }

                return http.get<any>(apiURL + 'find/' + value).pipe(
                    map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))
                );
            },
            search(term: string): Observable<any[]> {
                return http.get<any[]>(apiURL + 'search', {
                    params: {
                        pageSize: '10',
                        q: term || '',
                        type: '0',
                        _sort: 'name'
                    }
                }).pipe(
                    map(list => list.map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))));
            }
        };

        this.travelAgentDataSource = {
            displayValue(value: any): Observable<any | null> {
                console.log('finding display value for', value);
                if (typeof value === 'string') {
                    value = parseInt(value, 10);
                }
                if (typeof value !== 'number') {
                    return of(null);
                }

                return http.get<any>(apiURL + 'find/' + value).pipe(
                    map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))
                );
            },
            search(term: string): Observable<any[]> {
                return http.get<any[]>(apiURL + 'search', {
                    params: {
                        pageSize: '10',
                        q: term || '',
                        type: '1',
                        _sort: 'name'
                    }
                }).pipe(
                    map(list => list.map(e => ({
                        value: e.id,
                        display: `${e.name}`,
                        details: {}
                    }))));
            }
        };
    }

    ngOnInit() {
        this.loadAssignTioUsers();
        this.loadStates();
    }

    companyTypeChange() {
        this.companyList.setValue(null);
    }

    closeDialog(result: any = '') {
        this.dialogRef.close(result);
    }

    save() {
        this.spinner.active = true;
        this.isSaving = true;
        this.dsrRecord.company_id = this.companyList.value;
        if (!this.dsrRecord.assign_to_id) {
            this.dsrRecord.assign_to_id = this.loggedInUser.id;
        }
        this.dsrService.save(this.dsrRecord)
            .subscribe(response => {
                    this.isSaving = false;
                    this.spinner.active = false;
                    this.cdr.detectChanges();
                    this.toastService.success('Saved Successfully', 'Success');
                    this.closeDialog(response);
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                    this.spinner.active = false;
                    this.cdr.detectChanges();
                });
    }

    dateChangeEvent(type: string) {
        this.dsrRecord[type] = this.dsrRecord[type].format('YYYY-MM-DD');
    }

    loadAssignTioUsers() {
        this.userService.getUserList()
            .subscribe(response => {
                    this.assignToList = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }


    loadDSRHistory() {
        this.dsrService.getHistory(this.dsrRecord.id)
            .subscribe(response => {
                    this.histories = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'something went wrong');
                });
    }

    openHistory(content) {
        this.modalService.open(content).result.then((result) => {
            // this.closeResult = `Closed with: ${result}`;
        });
    }

    loadStates() {
        this.commonService.getStates(this.countryId)
            .subscribe(response => {
                    this.states = response;
                    if (this.dsrRecord.state_id) {
                        this.loadCities();
                    }
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    loadCities() {
        this.commonService.getCities(this.dsrRecord.state_id)
            .subscribe(response => {
                    this.cities = response;
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    changeCompany($event) {
        if (this.dsrRecord.company_id !== this.companyList.value) {
            this.dsrRecord.company_id = this.companyList.value;
            if (this.dsrRecord.company_id) {
                this.companyService.getCompanyDetail(this.dsrRecord.company_id)
                    .subscribe(response => {
                            this.dsrRecord.person_name = response.person_name;
                            this.dsrRecord.designation = response.designation;
                            this.dsrRecord.mobile = response.mobile;
                            this.dsrRecord.email = response.email;
                        },
                        error => {
                            this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                        });
            }
        }
    }

}
