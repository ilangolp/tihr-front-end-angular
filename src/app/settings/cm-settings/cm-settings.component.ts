import {Component, OnInit} from '@angular/core';
import {HotelService} from '../../services/hotel.service';
import {ToastrService} from 'ngx-toastr';
import {CMMappingService} from '../../services/cmMapping.service';

@Component({
    selector: 'm-cm-settings',
    templateUrl: './cm-settings.component.html',
    styleUrls: ['./cm-settings.component.scss'],
    providers: [HotelService, CMMappingService]
})
export class CMSettingsComponent implements OnInit {
    hotels: any[] = [];
    isSaving: boolean = false;
    isLoading: boolean = false;
    currentHotel: any = {};
    hotelMappings: any[] = [];

    constructor(private toastService: ToastrService,
                private cmMappingService: CMMappingService,
                private hotelService: HotelService) {
    }

    ngOnInit() {
        this.getHotels();
    }

    getHotels() {
        this.hotelService.getHotelList()
            .subscribe(response => {
                    this.hotels = response;
                    if (this.hotels.length > 0) {
                        this.currentHotel = this.hotels[0];
                        this.loadMappingForHotel();
                    }
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    loadMappingForHotel() {
        this.isLoading = true;
        this.cmMappingService.getHotelMappings(this.currentHotel.id)
            .subscribe(response => {
                    this.hotelMappings = response;
                    this.isLoading = false;
                },
                error => {
                    this.isLoading = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

    saveHotelMappings() {
        this.isSaving = true;
        this.cmMappingService.saveHotelMappings(this.currentHotel.id, this.hotelMappings)
            .subscribe(response => {
                    this.hotelMappings = response;
                    this.isSaving = false;
                },
                error => {
                    this.isSaving = false;
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }
}
