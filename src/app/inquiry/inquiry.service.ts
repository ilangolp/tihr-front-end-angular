import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {MatDialog} from '@angular/material';
import {WaitlistedInquiriesComponent} from './waitlisted-inquiries/waitlisted-inquiries.component';

@Injectable()
export class InquiryService {

    API_URL = environment.API_URL;

    constructor(private http: HttpClient, private toastService: ToastrService, public dialog: MatDialog) {
        this.API_URL = environment.API_URL;
    }

    getInquirySourceList(): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/source-list');
    }

    getInquiryStatusList(): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/status-list');
    }

    getInquirySubStatusList(): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/sub-status-list');
    }

    getInquiryTypeList(): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/type-list');
    }

    getHotels(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/hotels', {params: params});
    }

    getInquirySettingLists(): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/setting-list');
    }

    getDiscountUsers(params: any): Observable<any> {
        return this.http.post(this.API_URL + '/settings/discount-users', params);
    }

    createInquiry(data: any): Observable<any> {
        return this.http.post(this.API_URL + '/inquiries', data);
    }

    saveBooking(data: any): Observable<any> {
        return this.http.post(this.API_URL + '/inquiries/booking', data);
    }

    checkBookingAvailability(params: any): Observable<any> {
        return this.http.post(this.API_URL + '/settings/rate-inventory/check-availability', params);
    }

    findBooking(id): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/booking/' + id);
    }

    findInquiry(id): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/find/' + id);
    }

    getInquiries(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries', {params: params});
    }

    getInquiriesList(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/inquiries/list', params);
    }

    getHistory(id): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/history/' + id);
    }

    getInquiryPayment(inquiry_id): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/' + inquiry_id + '/payment');
    }

    deleteInquiryPayment(paymentId): Observable<any> {
        return this.http.delete(this.API_URL + '/inquiries/payment/' + paymentId);
    }

    sendPaymentRequest(paymentId): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/payment/' + paymentId + '/payment-request');
    }

    saveInquiryPayment(data: any): Observable<any> {
        return this.http.post(this.API_URL + '/inquiries/payment', data);
    }

    confirmPayment(id: any, data: any): Observable<any> {
        return this.http.post(this.API_URL + '/inquiries/' + id + '/payment-confirm/' + data.id, data);
    }

    getAllHotelBanks(): Observable<any> {
        return this.http.get(this.API_URL + '/banks-list');
    }

    updateDiscountStatus(id: any, discount_status: any, data: any): Observable<any> {
        return this.http.post(this.API_URL + '/inquiries/' + id + '/discount-status/' + discount_status, data);
    }

    rejectPayment(id: any, data: any): Observable<any> {
        return this.http.post(this.API_URL + '/inquiries/' + id + '/payment-reject/' + data.id, data);
    }

    getInquiryNotes(inquiry_id): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/' + inquiry_id + '/notes');
    }

    saveInquiryNote(data: any): Observable<any> {
        return this.http.post(this.API_URL + '/inquiries/notes', data);
    }

    deleteInquiryNote(inquiry_id, id): Observable<any> {
        return this.http.delete(this.API_URL + '/inquiries/' + inquiry_id + '/notes/' + id);
    }

    getAttachmentUploadUrl(id: any = '') {
        return this.API_URL + '/inquiries/attach?payment_id=' + id;
    }

    getAttachmentInvoiceUrl(id: any = '') {
        return this.API_URL + '/inquiries/attach-invoice/' + id;
    }

	deleteBookingInvoice(inquiry_id): Observable<any> {
		return this.http.get(this.API_URL + '/inquiries/remove/' + inquiry_id + '/booking-invoice');
	}

    deletePaymentAttachment(inquiry_id, id): Observable<any> {
        return this.http.delete(this.API_URL + '/inquiries/' + inquiry_id + '/payment-attach/' + id);
    }

    sendDetailMail(inquiry_id, params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/' + inquiry_id + '/send-email', {params: params});
    }

    previewDetailMail(inquiry_id, params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/inquiries/' + inquiry_id + '/preview-email', {params: params});
    }

    getUpcomingWIPInquiries(data: any): Observable<any> {
        return this.http.post(this.API_URL + '/inquiries/upcoming-inquiry-actions', data);
    }

    getUserInquiryReminders() {
        return this.http.get(this.API_URL + '/inquiries/inquiry-reminders');
    }

    updateInquiryReminderStatus(reminderId, status) {
        return this.http.get(this.API_URL + '/inquiries/update-inquiry-reminders/' + reminderId + '?status=' + status);
    }


    //Leads
    getLeadList(params: any = {}): Observable<any> {
        return this.http.get(this.API_URL + '/leads', {params: params});
    }

    saveLead(params: any = {}): Observable<any> {
        return this.http.post(this.API_URL + '/leads', params);
    }

    deleteLead(id): Observable<any> {
        return this.http.get(this.API_URL + '/leads/' + id + '/delete');
    }

    showWaitListedInquiries(reqParams) {
        this.getInquiriesList(reqParams)
            .subscribe(response => {
                    if (response.totalCount > 0) {
                        this.dialog.open(WaitlistedInquiriesComponent, {
                            data: {
                                inquiries: response.items
                            }
                        });
                    }
                },
                error => {
                    this.toastService.error((error.error.message) ? error.error.message : '', 'Failed');
                });
    }

	getInvoices(inquiry_id): Observable<any> {
		return this.http.get(this.API_URL + '/inquiries/' + inquiry_id + '/invoices');
	}
}
